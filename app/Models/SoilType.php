<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoilType extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'soil_types';
    public $timestamps = false;
    protected $fillable = [
			'soil_type',
			'status',
		];
}
