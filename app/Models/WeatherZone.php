<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherZone extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'weather_zones';
    public $timestamps = false;
    protected $fillable = [
			'zone_name',
			'status',
		];
}
