<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMagicCircle extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'user_magic_circles';
    public $timestamps = false;
    protected $fillable = [
			'user_id',
			'mc_id',
			'status',
		];
}
