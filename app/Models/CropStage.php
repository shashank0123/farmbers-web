<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CropStage extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'crop_stages';
    public $timestamps = false;
    protected $fillable = [
			'crop_id',
			'stage_name',
			'stage_flexibility',
			'stage_duration',
			'position',
			'status',
			'stage_desc',
		];
}
