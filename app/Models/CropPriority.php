<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CropPriority extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'crop_priorities';
    public $timestamps = false;
    protected $fillable = [
			'crop_id',
			'latitude',
			'longitude',
			'pincode',
			'priority',
			'status',
		];
}
