<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmUser extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'om_users';
    public $timestamps = false;
    protected $fillable = [
			'name',
			'mobile_no',
			'email',
			'google_id',
			'facebook_id',
			'password',
			'mobile_verified_date',
			'email_verified_date',
			'remember_token',
			'status',
		];
}
