<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'locations';
    public $timestamps = false;
    protected $fillable = [
			'location_name',
			'location_code',
			'location_pincode',
			'latitude',
			'longitude',
			'weather_zone',
			'soil_type',
			'irrigation_level',
			'cultivation_period',
			'status',
		];
}
