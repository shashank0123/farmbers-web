<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'buyers';
    public $timestamps = false;
    protected $fillable = [
			'buyer_name',
			'buyer_type',
			'address',
			'pincode',
			'latitude',
			'longitude',
			'status',
		];
}
