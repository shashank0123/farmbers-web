<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherDelay extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'weather_delays';
    public $timestamps = false;
    protected $fillable = [
			'pincode',
			'latitude',
			'longitude',
			'crop_id',
			'stage_id',
			'delay_reason',
			'delay',
			'message',
			'status',
		];
}
