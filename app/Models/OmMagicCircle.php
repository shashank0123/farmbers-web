<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmMagicCircle extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'om_magic_circles';
    public $timestamps = false;
    protected $fillable = [
			'mc_name',
			'pincode',
			'radius',
			'status',
		];
}
