<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crop extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'crops';
    public $timestamps = false;
    protected $fillable = [
			'crop_name',
			'crop_duration',
			'crop_image',
			'featured',
			'position',
			'status',
			'description',
		];
}
