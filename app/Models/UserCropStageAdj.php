<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCropStageAdj extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'user_crop_stage_adjs';
    public $timestamps = false;
    protected $fillable = [
			'user_crop_stage_id',
			'adj_day',
			'status',
		];
}
