<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherImpact extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'weather_impacts';
    public $timestamps = false;
    protected $fillable = [
			'delay_id',
			'log_date',
			'delay_reason',
			'delay',
			'message',
		];
}
