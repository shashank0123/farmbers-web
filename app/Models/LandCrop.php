<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LandCrop extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'land_crops';
    public $timestamps = false;
    protected $fillable = [
			'land_id',
			'crop_id',
			'log_date',
			'status',
		];
}
