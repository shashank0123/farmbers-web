<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuyerCropParam extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'buyer_crop_params';
    public $timestamps = false;
    protected $fillable = [
			'buyer_id',
			'crop_name',
			'log_date',
			'param_name',
			'param_value',
			'param_unit',
		];
}
