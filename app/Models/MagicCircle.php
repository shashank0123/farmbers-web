<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MagicCircle extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'magic_circles';
    public $timestamps = false;
    protected $fillable = [
			'mc_name',
			'pincode',
			'radius',
			'status',
			'latitude',
			'longitude',
		];
}
