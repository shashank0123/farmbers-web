<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CropLocation extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'crop_locations';
    public $timestamps = false;
    protected $fillable = [
			'crop_id',
			'location_id',
			'from_day',
			'to_day',
			'status',
		];
}
