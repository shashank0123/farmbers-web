<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Land extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'lands';
    public $timestamps = false;
    protected $fillable = [
			'land_name',
			'latitude',
			'longitude',
			'address',
			'status',
		];
}
