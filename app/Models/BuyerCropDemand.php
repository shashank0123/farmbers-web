<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuyerCropDemand extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'buyer_crop_demands';
    public $timestamps = false;
    protected $fillable = [
			'buyer_id',
			'crop_id',
			'pincode',
			'demand',
			'status',
		];
}
