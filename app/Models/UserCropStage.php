<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCropStage extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'user_crop_stages';
    public $timestamps = false;
    protected $fillable = [
			'user_id',
			'land_crop_id',
			'log_date',
			'stage_flexibility',
			'status',
		];
}
