<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteSettingController extends Controller
{
    public function getSettingPage(Request $request)
    {
    	return view('admin.website.settings');
    }
}
