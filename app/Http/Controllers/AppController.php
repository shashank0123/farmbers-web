<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use StdClass;
use App\Models\Land;
use App\Models\MagicCircle;
use App\Models\UserLand;
use App\Models\UserCrop;
use App\Models\CropStage;
use App\Models\Crop;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class AppController extends Controller
{
    public function allLanguage(Request $request)
    {
    	$languages = [];
    	$language = new StdClass;
    	$language->id = 1;
    	$language->language = 'English';
    	$language->code = 'en';
    	array_push($languages, $language);
    	$language = new StdClass;
    	$language->id = 1;
    	$language->language = 'Hindi';
    	$language->code = 'hi';

    	array_push($languages, $language);

    	return response()->json(['status' => 'success', 'data' => $languages]);
    }

    public function getDashboard(Request $request)
    {

        $lands = UserLand::leftJoin('lands', 'user_lands.land_id' , 'lands.id')->leftJoin('user_crops', 'user_crops.user_land_id', 'lands.id')->where('user_lands.user_id', Auth::user()->id)->get();
        foreach ($lands as $key => $value){
            if (isset($value->user_land_id) && $value->user_land_id != ''){
                $lands[$key]->color = 'yellow';
            }
            else
            $lands[$key]->color = 'green';
        }


        return response()->json(['status' => 'success', 'data' => $lands]);
    }

    public function buyerDashboard(Request $request)
    {

        $buyer_types = [];

        $buyer_type = new StdClass;
        $buyer_type->id = 1;
        $buyer_type->buyer_type = 'Mandi';
        $buyer_type->text = 'Lorem ipsum';
        $buyer_type->image = 'http://farmbers.smartstalk.com/'.'/uploads/buyers/mandi.jpg';
        array_push($buyer_types, $buyer_type);

        $buyer_type = new StdClass;
        $buyer_type->id = 1;
        $buyer_type->buyer_type = 'Home Kitchen';
        $buyer_type->text = 'Lorem ipsum';
        $buyer_type->image = 'http://farmbers.smartstalk.com/'.'/uploads/buyers/home.jpg';
        array_push($buyer_types, $buyer_type);

        $buyer_type = new StdClass;
        $buyer_type->id = 1;
        $buyer_type->buyer_type = 'Restaurent Kitchen';
        $buyer_type->text = 'Lorem ipsum';
        $buyer_type->image = 'http://farmbers.smartstalk.com/'.'/uploads/buyers/hotel.jpg';
        array_push($buyer_types, $buyer_type);

        $buyer_type = new StdClass;
        $buyer_type->id = 1;
        $buyer_type->buyer_type = 'Veg Shop';
        $buyer_type->text = 'Lorem ipsum';
        $buyer_type->image = 'http://farmbers.smartstalk.com/'.'/uploads/buyers/shop.jpg';
        array_push($buyer_types, $buyer_type);

        return response()->json(['status' => 'success', 'data' => $buyer_types]);
    }

    public function bestCrop(Request $request)
    {
        $crops = Crop::limit(3)->get();
        foreach ($crops as $key => $value) {
            $crops[$key]->crop_image = 'http://farmbers.smartstalk.com/'.$value->crop_image;
        }




        return response()->json(['status' => 'success', 'data' => $crops]);
    }


    public function weatherData(Request $request)
    {
        $pincode = $request->pincode;
        if (strpos($pincode, '1100')){
            $city = 'delhi';
        }
        if (strpos($pincode, '201')){
            $city = 'noida';
        }
        if (strpos($pincode, '8420')){
            $city = 'muzaffarpur';
        }
        else
            $city = 'gurugram';
        $key = 'a5d27591069d119284c440dae2919a9b' ;

        $url = "http://api.openweathermap.org/data/2.5/forecast?q=$city&appid=$key";
        $c = curl_init();
        curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($c,CURLOPT_HTTPGET ,1);

        curl_setopt($c, CURLOPT_URL, $url);
        $contents = curl_exec($c);
        if (curl_errno($c)) {
            echo 'Curl error: ' . curl_error($c);
        }else{
            curl_close($c);
        }
        $response = new StdClass;
        $response->temperature = 18;
        $response->real_feel = 18;
        $response->humidity = 18;
        $response->wind = 18;
        $response->rain = 18;
        $response->weather_type = "Haze";


        // 3 days data
        $threedays = array();
        $day1 = new StdClass;
        $day1->title = "today";
        $day1->weather_type = "Haze";
        $day1->temperature = "19";
        array_push($threedays, $day1);
        $day1 = new StdClass;
        $day1->title = "tomorrow";
        $day1->weather_type = "Haze";
        $day1->temperature = "19";
        array_push($threedays, $day1);
        $day1 = new StdClass;
        $day1->title = "Friday";
        $day1->weather_type = "Haze";
        $day1->temperature = "19";
        array_push($threedays, $day1);
        $response->threedays = $threedays;


        // 5 days data
        // 3 days data
        $fivedays = array();
        $day1 = new StdClass;
        $day1->date = "08/04";
        $day1->weather_type = "Haze";
        $day1->temperature = "19";
        $day1->wind = 18;
        array_push($fivedays, $day1);
        $day1 = new StdClass;
        $day1->date = "09/04";
        $day1->weather_type = "Haze";
        $day1->temperature = "19";
        $day1->wind = 18;
        array_push($fivedays, $day1);
        $day1 = new StdClass;
        $day1->date = "10/04";
        $day1->weather_type = "Haze";
        $day1->temperature = "19";
        $day1->wind = 18;
        array_push($fivedays, $day1);
        $day1 = new StdClass;
        $day1->date = "11/04";
        $day1->weather_type = "Haze";
        $day1->temperature = "19";
        $day1->wind = 18;
        array_push($fivedays, $day1);
        $day1 = new StdClass;
        $day1->date = "12/04";
        $day1->weather_type = "Haze";
        $day1->temperature = "19";
        $day1->wind = 18;
        array_push($fivedays, $day1);
        $response->fivedays = $fivedays;

        if ($contents){
            $contents = json_decode($contents);
            $data = new StdClass;

            if ($contents->list){
                $data = $contents->list;
                foreach ($contents->list as $key => $value) {
                    $newdate = gmdate("Y-m-d H:i:s\Z", $value->dt);
                    $time = strtotime($newdate);
                    $startTime = date("Y-m-d H:i:s", strtotime('+330 minutes', $time));
                    $data = $startTime;
                    $temperature = round($value->main->temp-273.15,1);
                    $real_feel = round($value->main->feels_like-273.15,1);
                    $humidity = $value->main->humidity;
                    $weather_type = $value->weather[0]->main;
                    $response->temperature = $temperature;
                    $response->real_feel = $real_feel;
                    $response->humidity = $humidity;
                    $response->weather_type = $weather_type;
                    break;
                }
            }



        }


            return response()->json($response);

    }

    public function soilData(Request $request)
    {
        $land_id = $request->land_id;
        $land = Land::where('id', $land_id)->first();
        if ($land){
            $latitude = $land->latitude;
            $longitude = $land->longitude;
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => "https://api.ambeedata.com/soil/latest/by-lat-lng?lat=$latitude&lng=$longitude",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                    "Content-type: application/json",
                    "x-api-key: Zpa6EwL0uZ5Ia2nxZjxcc3kqstxaVvkp912vkRCn"
                ],
            ]);
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                echo $response;
            }
        }
    }

    public function saveHelp(Request $request)
    {
        $response = new StdClass;
        $response->status  = 'success';
        $response->data = 'Data saved successfully';
        return response()->json($response);
    }

    public function saveNotification(Request $request)
    {
        $response = new StdClass;
        $response->status  = 'success';
        $response->data = 'Data saved successfully';
        return response()->json($response);
    }

    public function getMagicCircle(Request $request)
    {
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $languages = [];
        $languages  = MagicCircle::get();


        return response()->json(['status' => 'success', 'data' => $languages]);
    }

    public function getUserCrop(Request $request)
    {

        $user_id = Auth::user()->id;
        $languages  = UserCrop::leftJoin('crops', 'crops.id', 'user_crops.crop_id')->leftJoin('lands', 'user_crops.user_land_id', 'lands.id')->where('user_crops.user_id', $user_id)->get();
        foreach ($languages as $key => $value) {
            $languages[$key]->crop_image = 'http://farmbers.smartstalk.com'.$value->crop_image;
            $languages[$key]->village = 'dummy village';
        }


        return response()->json(['status' => 'success', 'data' => $languages]);
    }

    public function saveFarm(Request $request)
    {
        $land_name = $request->farm_name;
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $address = $request->address;

        $checkland = Land::where('latitude', $latitude)->where('longitude', $longitude)->first();
        if ($checkland){
            return response()->json(['status' => 'failed', 'data' => 'land already exists']);
        }
        $land = new Land;
        $land->land_name    = $land_name;
        $land->latitude = $latitude;
        $land->longitude    = $longitude;
        $land->address  = $address;
        $land->save();

        $checkland = UserLand::where('user_id', Auth::user()->id)->where('land_id', $land->id)->first();
        if ($checkland){
            return response()->json(['status' => 'failed', 'data' => 'land already exists']);
        }

        $userland = new UserLand;
        $userland->user_id = Auth::user()->id;
        $userland->land_id = $land->id;
        $userland->status = 1;
        $userland->save();

        return response()->json(['status' => 'success', 'data' => $userland]);
    }

    public function userCrop(Request $request)
    {
        $crop_id = $request->crop_id;
        $user_land_id = $request->land_id;
        $user_id = Auth::user()->id;

        $checkland = UserCrop::where('user_land_id', $user_land_id)->where('user_id', $user_id)->first();
        if ($checkland){
            return response()->json(['status' => 'failed', 'data' => 'crop already exists']);
        }
        $land = new UserCrop;
        $land->crop_id    = $crop_id;
        $land->user_land_id = $user_land_id;
        $land->user_id    = $user_id;
        $land->status  = 'Active';
        $land->save();



        return response()->json(['status' => 'success', 'data' => $land]);
    }

    public function userCropStage(Request $request)
    {
        $crop_id = $request->crop_id;

        $checkland = CropStage::where('crop_id', $crop_id)->get();
        $current_date = Carbon::now();

        foreach ($checkland as $key => $value) {
            $checkland[$key]->month = $current_date->isoFormat('MMM');
            $checkland[$key]->dateRange = $current_date->isoFormat('DD');
            $current_date = $current_date->addDays($value->stage_duration);
            $checkland[$key]->dateRange .= '-'.$current_date->isoFormat('DD');


            if ($key == 0)
                $checkland[$key]->completed = '1';
            else
                $checkland[$key]->completed = '0';

            if ($checkland[$key]->completed == '1'){
                $checkland[$key]->warning_desc = 'Buy good seeds';
            }

        }
        if ($checkland){
            return response()->json(['status' => 'success', 'data' => $checkland]);
        }
        return response()->json(['status' => 'failed', 'data' => 'crop does not exists']);



    }
    public function locationName(Request $request)
    {
    	$language = new StdClass;
    	$language->id = 1;
    	$language->location = 'Delhi';
    	$language->pincode = '110059';
    	$languages = [];
    	array_push($languages, $language);

    	return response()->json(['status' => 'success', 'data' => $languages]);
    }

    public function userLanguage(Request $request)
    {
    	$language = new StdClass;
    	$language->id = 1;
    	$language->language = 'English';
    	$language->code = 'en';
    	$languages = [];
    	array_push($languages, $language);

    	return response()->json(['status' => 'success', 'data' => 'successfully added']);
    }
}
