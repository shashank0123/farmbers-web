<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\AppNotification;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function getProfile(Request $request)
    {

    	$profile = Profile::where('user_id', Auth::user()->id)->first();
    	if (!$profile){
    		$profile = new Profile;
    		$profile->user_id = Auth::user()->id;
    		$profile->save();
    	}
    	$profile->name = Auth::user()->name;
        $profile->mobile = Auth::user()->mobile;
        if (isset($profile->position) && $profile->position == 'null'){
            $profile->position = '';
        }
        if (isset($profile->profession) && $profile->profession == 'null'){
            $profile->profession = '';
        }
        if (isset($profile->address) && $profile->address == 'null'){
            $profile->address = '';
        }
        if (isset($profile->locality) && $profile->locality == 'null'){
            $profile->locality = '';
        }
        if (isset($profile->city) && $profile->city == 'null'){
            $profile->city = '';
        }
        if (isset($profile->state) && $profile->state == 'null'){
            $profile->state = '';
        }
        if (isset($profile->pincode) && $profile->pincode == 'null'){
            $profile->pincode = '';
        }
        if (isset($profile->bio) && $profile->bio == 'null'){
            $profile->bio = '';
        }
        if (isset($profile->avatar) && $profile->avatar == 'null'){
            $profile->avatar = '';
        }

    	foreach ($profile  as $key => $value) {
            # code...
        }
        return response()->json(["status" => "success", "data" => $profile], 200);
    }

    public function saveProfile(Request $request)
    {
    	$profile = Profile::where('user_id', Auth::user()->id)->first();
    	$profile->position		= $request->position;
    	$profile->profession		= $request->profession;
    	$profile->address		= $request->address;
    	$profile->locality		= $request->locality;
    	$profile->city		= $request->city;
    	$profile->state		= $request->state;
    	$profile->pincode		= $request->pincode;
    	$profile->bio		= $request->bio;
    	if ($request->avatarSource != '' && $request->avatarSource != null){
    		// $image = base64_decode($request->avatarSource);
    		// $image = str_replace('data:image/png;base64,', '', $image);
    		$image = $request->avatarSource;
		    // $image = str_replace(' ', '+', $image);
    		// $image = explode('base64,', $image)[1];
    		$image = base64_decode($image);
    		$filename = time().'.png';
    		file_put_contents(public_path('uploads/user_image/'.$filename), $image);
            $profile->avatar = url('uploads/user_image/'.$filename);
    	}
        
    	$profile->update();
    	$profile->name = Auth::user()->name;
    	return response()->json(["status" => "success", "data" => $profile], 200);
    }

    public function allNotification(Request $request)
    {
        // where('user_id', Auth::user()->id)->
    	$profile = AppNotification::all();
    	
    	return response()->json(["status" => "success", "data" => $profile], 200);
    }
}
