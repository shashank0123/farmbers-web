<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\WeatherDelay;
//add models here

class WeatherDelayController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $weather_delays = DB::table('weather_delays')->where('id', '%'.$queryw.'%')
						->orWhere('pincode', 'like', '%'.$queryw.'%')
						->orWhere('latitude', 'like', '%'.$queryw.'%')
						->orWhere('longitude', 'like', '%'.$queryw.'%')
						->orWhere('crop_id', 'like', '%'.$queryw.'%')
						->orWhere('stage_id', 'like', '%'.$queryw.'%')
						->orWhere('delay_reason', 'like', '%'.$queryw.'%')
						->orWhere('delay', 'like', '%'.$queryw.'%')
						->orWhere('message', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $weather_delays = DB::table('weather_delays')->where('id', $queryw)->first();
            if ($weather_delays){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $weather_delays], 200);
            }

        }
        else
        $weather_delays = WeatherDelay::get();

        if (count($weather_delays) > 0){
            return response()->json(['status' => 'success', 'count' => count($weather_delays), 'data' => $weather_delays], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($weather_delays), 'data' => array() ,'message' => 'Failed! no weather_delays found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['pincode'] = $data['pincode'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['stage_id'] = $data['stage_id'];
		$saveData['delay_reason'] = $data['delay_reason'];
		$saveData['delay'] = $data['delay'];
		$saveData['message'] = $data['message'];
		$saveData['status'] = $data['status'];

        $weather_delays = WeatherDelay::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $weather_delays], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['pincode'] = $data['pincode'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['stage_id'] = $data['stage_id'];
		$saveData['delay_reason'] = $data['delay_reason'];
		$saveData['delay'] = $data['delay'];
		$saveData['message'] = $data['message'];
		$saveData['status'] = $data['status'];

        $row = WeatherDelay::where('id', $id)->first();
        if ($row){
            $WeatherDelay = WeatherDelay::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $WeatherDelay], 200);
    }

    public function delete(Request $request)
    {
        $delete = WeatherDelay::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
