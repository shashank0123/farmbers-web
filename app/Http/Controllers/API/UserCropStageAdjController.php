<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\UserCropStageAdj;
//add models here

class UserCropStageAdjController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $user_crop_stage_adjs = DB::table('user_crop_stage_adjs')->where('id', '%'.$queryw.'%')
						->orWhere('user_crop_stage_id', 'like', '%'.$queryw.'%')
						->orWhere('adj_day', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $user_crop_stage_adjs = DB::table('user_crop_stage_adjs')->where('id', $queryw)->first();
            if ($user_crop_stage_adjs){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $user_crop_stage_adjs], 200);
            }

        }
        else
        $user_crop_stage_adjs = UserCropStageAdj::get();

        if (count($user_crop_stage_adjs) > 0){
            return response()->json(['status' => 'success', 'count' => count($user_crop_stage_adjs), 'data' => $user_crop_stage_adjs], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($user_crop_stage_adjs), 'data' => array() ,'message' => 'Failed! no user_crop_stage_adjs found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_crop_stage_id'] = $data['user_crop_stage_id'];
		$saveData['adj_day'] = $data['adj_day'];
		$saveData['status'] = $data['status'];

        $user_crop_stage_adjs = UserCropStageAdj::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $user_crop_stage_adjs], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_crop_stage_id'] = $data['user_crop_stage_id'];
		$saveData['adj_day'] = $data['adj_day'];
		$saveData['status'] = $data['status'];

        $row = UserCropStageAdj::where('id', $id)->first();
        if ($row){
            $UserCropStageAdj = UserCropStageAdj::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $UserCropStageAdj], 200);
    }

    public function delete(Request $request)
    {
        $delete = UserCropStageAdj::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
