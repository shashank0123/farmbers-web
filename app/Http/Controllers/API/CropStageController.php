<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CropStage;
//add models here

class CropStageController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $crop_stages = DB::table('crop_stages')->where('id', '%'.$queryw.'%')
						->orWhere('crop_id', 'like', '%'.$queryw.'%')
						->orWhere('stage_name', 'like', '%'.$queryw.'%')
						->orWhere('stage_flexibility', 'like', '%'.$queryw.'%')
						->orWhere('stage_duration', 'like', '%'.$queryw.'%')
						->orWhere('position', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')
						->orWhere('stage_desc', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $crop_stages = DB::table('crop_stages')->where('id', $queryw)->first();
            if ($crop_stages){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $crop_stages], 200);
            }

        }
        else
        $crop_stages = CropStage::get();

        if (count($crop_stages) > 0){
            return response()->json(['status' => 'success', 'count' => count($crop_stages), 'data' => $crop_stages], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($crop_stages), 'data' => array() ,'message' => 'Failed! no crop_stages found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['stage_name'] = $data['stage_name'];
		$saveData['stage_flexibility'] = $data['stage_flexibility'];
		$saveData['stage_duration'] = $data['stage_duration'];
		$saveData['position'] = $data['position'];
		$saveData['status'] = $data['status'];
		$saveData['stage_desc'] = $data['stage_desc'];

        $crop_stages = CropStage::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $crop_stages], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['stage_name'] = $data['stage_name'];
		$saveData['stage_flexibility'] = $data['stage_flexibility'];
		$saveData['stage_duration'] = $data['stage_duration'];
		$saveData['position'] = $data['position'];
		$saveData['status'] = $data['status'];
		$saveData['stage_desc'] = $data['stage_desc'];

        $row = CropStage::where('id', $id)->first();
        if ($row){
            $CropStage = CropStage::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $CropStage], 200);
    }

    public function delete(Request $request)
    {
        $delete = CropStage::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
