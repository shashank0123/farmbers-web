<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Crop;
//add models here

class CropController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $crops = DB::table('crops')->where('id', '%'.$queryw.'%')
						->orWhere('crop_name', 'like', '%'.$queryw.'%')
						->orWhere('crop_duration', 'like', '%'.$queryw.'%')
						->orWhere('crop_image', 'like', '%'.$queryw.'%')
						->orWhere('featured', 'like', '%'.$queryw.'%')
						->orWhere('position', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')
						->orWhere('description', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $crops = DB::table('crops')->where('id', $queryw)->first();
            if ($crops){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $crops], 200);
            }

        }
        else
        $crops = Crop::get();

        if (count($crops) > 0){
            return response()->json(['status' => 'success', 'count' => count($crops), 'data' => $crops], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($crops), 'data' => array() ,'message' => 'Failed! no crops found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['crop_name'] = $data['crop_name'];
		$saveData['crop_duration'] = $data['crop_duration'];
if (request()->hasFile('crop_image')) {
               $path = request()->file('crop_image')->store(
                   'file', 'public'
               );

               $data['crop_image'] = \Storage::disk('public')->url($path);
               $data['crop_image'] = '/storage'.explode('storage', $data['crop_image'])[1];
                $saveData['crop_image'] = $data['crop_image'];


            }		$saveData['featured'] = $data['featured'];
		$saveData['position'] = $data['position'];
		$saveData['status'] = $data['status'];
		$saveData['description'] = $data['description'];

        $crops = Crop::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $crops], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['crop_name'] = $data['crop_name'];
		$saveData['crop_duration'] = $data['crop_duration'];
if (request()->hasFile('crop_image')) {
               $path = request()->file('crop_image')->store(
                   'file', 'public'
               );

               $data['crop_image'] = \Storage::disk('public')->url($path);
               $data['crop_image'] = '/storage'.explode('storage', $data['crop_image'])[1];
                $saveData['crop_image'] = $data['crop_image'];


            }		$saveData['featured'] = $data['featured'];
		$saveData['position'] = $data['position'];
		$saveData['status'] = $data['status'];
		$saveData['description'] = $data['description'];

        $row = Crop::where('id', $id)->first();
        if ($row){
            $Crop = Crop::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $Crop], 200);
    }

    public function delete(Request $request)
    {
        $delete = Crop::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
