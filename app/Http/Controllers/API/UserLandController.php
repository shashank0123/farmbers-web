<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\UserLand;
//add models here

class UserLandController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $user_lands = DB::table('user_lands')->where('id', '%'.$queryw.'%')
						->orWhere('user_id', 'like', '%'.$queryw.'%')
						->orWhere('land_id', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $user_lands = DB::table('user_lands')->where('id', $queryw)->first();
            if ($user_lands){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $user_lands], 200);
            }

        }
        else
        $user_lands = UserLand::get();

        if (count($user_lands) > 0){
            return response()->json(['status' => 'success', 'count' => count($user_lands), 'data' => $user_lands], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($user_lands), 'data' => array() ,'message' => 'Failed! no user_lands found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['land_id'] = $data['land_id'];
		$saveData['status'] = $data['status'];

        $user_lands = UserLand::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $user_lands], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['land_id'] = $data['land_id'];
		$saveData['status'] = $data['status'];

        $row = UserLand::where('id', $id)->first();
        if ($row){
            $UserLand = UserLand::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $UserLand], 200);
    }

    public function delete(Request $request)
    {
        $delete = UserLand::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
