<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CropLocation;
//add models here

class CropLocationController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $crop_locations = DB::table('crop_locations')->where('id', '%'.$queryw.'%')
						->orWhere('crop_id', 'like', '%'.$queryw.'%')
						->orWhere('location_id', 'like', '%'.$queryw.'%')
						->orWhere('from_day', 'like', '%'.$queryw.'%')
						->orWhere('to_day', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $crop_locations = DB::table('crop_locations')->where('id', $queryw)->first();
            if ($crop_locations){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $crop_locations], 200);
            }

        }
        else
        $crop_locations = CropLocation::get();

        if (count($crop_locations) > 0){
            return response()->json(['status' => 'success', 'count' => count($crop_locations), 'data' => $crop_locations], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($crop_locations), 'data' => array() ,'message' => 'Failed! no crop_locations found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['location_id'] = $data['location_id'];
		$saveData['from_day'] = $data['from_day'];
		$saveData['to_day'] = $data['to_day'];
		$saveData['status'] = $data['status'];

        $crop_locations = CropLocation::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $crop_locations], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['location_id'] = $data['location_id'];
		$saveData['from_day'] = $data['from_day'];
		$saveData['to_day'] = $data['to_day'];
		$saveData['status'] = $data['status'];

        $row = CropLocation::where('id', $id)->first();
        if ($row){
            $CropLocation = CropLocation::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $CropLocation], 200);
    }

    public function delete(Request $request)
    {
        $delete = CropLocation::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
