<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\UserMagicCircle;
//add models here

class UserMagicCircleController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $user_magic_circles = DB::table('user_magic_circles')->where('id', '%'.$queryw.'%')
						->orWhere('user_id', 'like', '%'.$queryw.'%')
						->orWhere('mc_id', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $user_magic_circles = DB::table('user_magic_circles')->where('id', $queryw)->first();
            if ($user_magic_circles){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $user_magic_circles], 200);
            }

        }
        else
        $user_magic_circles = UserMagicCircle::get();

        if (count($user_magic_circles) > 0){
            return response()->json(['status' => 'success', 'count' => count($user_magic_circles), 'data' => $user_magic_circles], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($user_magic_circles), 'data' => array() ,'message' => 'Failed! no user_magic_circles found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['mc_id'] = $data['mc_id'];
		$saveData['status'] = $data['status'];

        $user_magic_circles = UserMagicCircle::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $user_magic_circles], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['mc_id'] = $data['mc_id'];
		$saveData['status'] = $data['status'];

        $row = UserMagicCircle::where('id', $id)->first();
        if ($row){
            $UserMagicCircle = UserMagicCircle::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $UserMagicCircle], 200);
    }

    public function delete(Request $request)
    {
        $delete = UserMagicCircle::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
