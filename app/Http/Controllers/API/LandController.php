<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Land;
//add models here

class LandController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $lands = DB::table('lands')->where('id', '%'.$queryw.'%')
						->orWhere('land_name', 'like', '%'.$queryw.'%')
						->orWhere('latitude', 'like', '%'.$queryw.'%')
						->orWhere('longitude', 'like', '%'.$queryw.'%')
						->orWhere('address', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $lands = DB::table('lands')->where('id', $queryw)->first();
            if ($lands){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $lands], 200);
            }

        }
        else
        $lands = Land::get();

        if (count($lands) > 0){
            return response()->json(['status' => 'success', 'count' => count($lands), 'data' => $lands], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($lands), 'data' => array() ,'message' => 'Failed! no lands found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['land_name'] = $data['land_name'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];
		$saveData['address'] = $data['address'];
		$saveData['status'] = $data['status'];

        $lands = Land::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $lands], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['land_name'] = $data['land_name'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];
		$saveData['address'] = $data['address'];
		$saveData['status'] = $data['status'];

        $row = Land::where('id', $id)->first();
        if ($row){
            $Land = Land::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $Land], 200);
    }

    public function delete(Request $request)
    {
        $delete = Land::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
