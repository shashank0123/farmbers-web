<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\UserCropStage;
//add models here

class UserCropStageController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $user_crop_stages = DB::table('user_crop_stages')->where('id', '%'.$queryw.'%')
						->orWhere('user_id', 'like', '%'.$queryw.'%')
						->orWhere('land_crop_id', 'like', '%'.$queryw.'%')
						->orWhere('log_date', 'like', '%'.$queryw.'%')
						->orWhere('stage_flexibility', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $user_crop_stages = DB::table('user_crop_stages')->where('id', $queryw)->first();
            if ($user_crop_stages){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $user_crop_stages], 200);
            }

        }
        else
        $user_crop_stages = UserCropStage::get();

        if (count($user_crop_stages) > 0){
            return response()->json(['status' => 'success', 'count' => count($user_crop_stages), 'data' => $user_crop_stages], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($user_crop_stages), 'data' => array() ,'message' => 'Failed! no user_crop_stages found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['land_crop_id'] = $data['land_crop_id'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['stage_flexibility'] = $data['stage_flexibility'];
		$saveData['status'] = $data['status'];

        $user_crop_stages = UserCropStage::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $user_crop_stages], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['land_crop_id'] = $data['land_crop_id'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['stage_flexibility'] = $data['stage_flexibility'];
		$saveData['status'] = $data['status'];

        $row = UserCropStage::where('id', $id)->first();
        if ($row){
            $UserCropStage = UserCropStage::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $UserCropStage], 200);
    }

    public function delete(Request $request)
    {
        $delete = UserCropStage::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
