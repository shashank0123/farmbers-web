<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\WeatherImpact;
//add models here

class WeatherImpactController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $weather_impacts = DB::table('weather_impacts')->where('id', '%'.$queryw.'%')
						->orWhere('delay_id', 'like', '%'.$queryw.'%')
						->orWhere('log_date', 'like', '%'.$queryw.'%')
						->orWhere('delay_reason', 'like', '%'.$queryw.'%')
						->orWhere('delay', 'like', '%'.$queryw.'%')
						->orWhere('message', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $weather_impacts = DB::table('weather_impacts')->where('id', $queryw)->first();
            if ($weather_impacts){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $weather_impacts], 200);
            }

        }
        else
        $weather_impacts = WeatherImpact::get();

        if (count($weather_impacts) > 0){
            return response()->json(['status' => 'success', 'count' => count($weather_impacts), 'data' => $weather_impacts], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($weather_impacts), 'data' => array() ,'message' => 'Failed! no weather_impacts found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['delay_id'] = $data['delay_id'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['delay_reason'] = $data['delay_reason'];
		$saveData['delay'] = $data['delay'];
		$saveData['message'] = $data['message'];

        $weather_impacts = WeatherImpact::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $weather_impacts], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['delay_id'] = $data['delay_id'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['delay_reason'] = $data['delay_reason'];
		$saveData['delay'] = $data['delay'];
		$saveData['message'] = $data['message'];

        $row = WeatherImpact::where('id', $id)->first();
        if ($row){
            $WeatherImpact = WeatherImpact::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $WeatherImpact], 200);
    }

    public function delete(Request $request)
    {
        $delete = WeatherImpact::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
