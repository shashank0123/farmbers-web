<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\WeatherZone;
//add models here

class WeatherZoneController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $weather_zones = DB::table('weather_zones')->where('id', '%'.$queryw.'%')
						->orWhere('zone_name', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $weather_zones = DB::table('weather_zones')->where('id', $queryw)->first();
            if ($weather_zones){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $weather_zones], 200);
            }

        }
        else
        $weather_zones = WeatherZone::get();

        if (count($weather_zones) > 0){
            return response()->json(['status' => 'success', 'count' => count($weather_zones), 'data' => $weather_zones], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($weather_zones), 'data' => array() ,'message' => 'Failed! no weather_zones found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['zone_name'] = $data['zone_name'];
		$saveData['status'] = $data['status'];

        $weather_zones = WeatherZone::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $weather_zones], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['zone_name'] = $data['zone_name'];
		$saveData['status'] = $data['status'];

        $row = WeatherZone::where('id', $id)->first();
        if ($row){
            $WeatherZone = WeatherZone::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $WeatherZone], 200);
    }

    public function delete(Request $request)
    {
        $delete = WeatherZone::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
