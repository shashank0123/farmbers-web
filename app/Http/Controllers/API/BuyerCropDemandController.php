<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\BuyerCropDemand;
//add models here

class BuyerCropDemandController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $buyer_crop_demands = DB::table('buyer_crop_demands')->where('id', '%'.$queryw.'%')
						->orWhere('buyer_id', 'like', '%'.$queryw.'%')
						->orWhere('crop_id', 'like', '%'.$queryw.'%')
						->orWhere('pincode', 'like', '%'.$queryw.'%')
						->orWhere('demand', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $buyer_crop_demands = DB::table('buyer_crop_demands')->where('id', $queryw)->first();
            if ($buyer_crop_demands){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $buyer_crop_demands], 200);
            }

        }
        else
        $buyer_crop_demands = BuyerCropDemand::get();

        if (count($buyer_crop_demands) > 0){
            return response()->json(['status' => 'success', 'count' => count($buyer_crop_demands), 'data' => $buyer_crop_demands], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($buyer_crop_demands), 'data' => array() ,'message' => 'Failed! no buyer_crop_demands found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['buyer_id'] = $data['buyer_id'];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['demand'] = $data['demand'];
		$saveData['status'] = $data['status'];

        $buyer_crop_demands = BuyerCropDemand::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $buyer_crop_demands], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['buyer_id'] = $data['buyer_id'];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['demand'] = $data['demand'];
		$saveData['status'] = $data['status'];

        $row = BuyerCropDemand::where('id', $id)->first();
        if ($row){
            $BuyerCropDemand = BuyerCropDemand::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $BuyerCropDemand], 200);
    }

    public function delete(Request $request)
    {
        $delete = BuyerCropDemand::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
