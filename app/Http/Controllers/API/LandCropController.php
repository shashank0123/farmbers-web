<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LandCrop;
//add models here

class LandCropController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $land_crops = DB::table('land_crops')->where('id', '%'.$queryw.'%')
						->orWhere('land_id', 'like', '%'.$queryw.'%')
						->orWhere('crop_id', 'like', '%'.$queryw.'%')
						->orWhere('log_date', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $land_crops = DB::table('land_crops')->where('id', $queryw)->first();
            if ($land_crops){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $land_crops], 200);
            }

        }
        else
        $land_crops = LandCrop::get();

        if (count($land_crops) > 0){
            return response()->json(['status' => 'success', 'count' => count($land_crops), 'data' => $land_crops], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($land_crops), 'data' => array() ,'message' => 'Failed! no land_crops found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['land_id'] = $data['land_id'];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['status'] = $data['status'];

        $land_crops = LandCrop::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $land_crops], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['land_id'] = $data['land_id'];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['status'] = $data['status'];

        $row = LandCrop::where('id', $id)->first();
        if ($row){
            $LandCrop = LandCrop::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $LandCrop], 200);
    }

    public function delete(Request $request)
    {
        $delete = LandCrop::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
