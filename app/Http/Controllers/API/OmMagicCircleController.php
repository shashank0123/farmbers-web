<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\OmMagicCircle;
//add models here

class OmMagicCircleController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $om_magic_circles = DB::table('om_magic_circles')->where('id', '%'.$queryw.'%')
						->orWhere('mc_name', 'like', '%'.$queryw.'%')
						->orWhere('pincode', 'like', '%'.$queryw.'%')
						->orWhere('radius', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $om_magic_circles = DB::table('om_magic_circles')->where('id', $queryw)->first();
            if ($om_magic_circles){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $om_magic_circles], 200);
            }

        }
        else
        $om_magic_circles = OmMagicCircle::get();

        if (count($om_magic_circles) > 0){
            return response()->json(['status' => 'success', 'count' => count($om_magic_circles), 'data' => $om_magic_circles], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($om_magic_circles), 'message' => 'Failed! no om_magic_circles found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['mc_name'] = $data['mc_name'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['radius'] = $data['radius'];
		$saveData['status'] = $data['status'];

        $om_magic_circles = OmMagicCircle::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $om_magic_circles], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['mc_name'] = $data['mc_name'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['radius'] = $data['radius'];
		$saveData['status'] = $data['status'];

        $row = OmMagicCircle::where('id', $id)->first();
        if ($row){
            $OmMagicCircle = OmMagicCircle::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $OmMagicCircle], 200);
    }

    public function delete(Request $request)
    {
        $delete = OmMagicCircle::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
