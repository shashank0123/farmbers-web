<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\BuyerCropParam;
//add models here

class BuyerCropParamController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $buyer_crop_params = DB::table('buyer_crop_params')->where('id', '%'.$queryw.'%')
						->orWhere('buyer_id', 'like', '%'.$queryw.'%')
						->orWhere('crop_name', 'like', '%'.$queryw.'%')
						->orWhere('log_date', 'like', '%'.$queryw.'%')
						->orWhere('param_name', 'like', '%'.$queryw.'%')
						->orWhere('param_value', 'like', '%'.$queryw.'%')
						->orWhere('param_unit', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $buyer_crop_params = DB::table('buyer_crop_params')->where('id', $queryw)->first();
            if ($buyer_crop_params){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $buyer_crop_params], 200);
            }

        }
        else
        $buyer_crop_params = BuyerCropParam::get();

        if (count($buyer_crop_params) > 0){
            return response()->json(['status' => 'success', 'count' => count($buyer_crop_params), 'data' => $buyer_crop_params], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($buyer_crop_params), 'data' => array() ,'message' => 'Failed! no buyer_crop_params found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['buyer_id'] = $data['buyer_id'];
		$saveData['crop_name'] = $data['crop_name'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['param_name'] = $data['param_name'];
		$saveData['param_value'] = $data['param_value'];
		$saveData['param_unit'] = $data['param_unit'];

        $buyer_crop_params = BuyerCropParam::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $buyer_crop_params], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['buyer_id'] = $data['buyer_id'];
		$saveData['crop_name'] = $data['crop_name'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['param_name'] = $data['param_name'];
		$saveData['param_value'] = $data['param_value'];
		$saveData['param_unit'] = $data['param_unit'];

        $row = BuyerCropParam::where('id', $id)->first();
        if ($row){
            $BuyerCropParam = BuyerCropParam::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $BuyerCropParam], 200);
    }

    public function delete(Request $request)
    {
        $delete = BuyerCropParam::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
