<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Location;
//add models here

class LocationController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $locations = DB::table('locations')->where('id', '%'.$queryw.'%')
						->orWhere('location_name', 'like', '%'.$queryw.'%')
						->orWhere('location_code', 'like', '%'.$queryw.'%')
						->orWhere('location_pincode', 'like', '%'.$queryw.'%')
						->orWhere('latitude', 'like', '%'.$queryw.'%')
						->orWhere('longitude', 'like', '%'.$queryw.'%')
						->orWhere('weather_zone', 'like', '%'.$queryw.'%')
						->orWhere('soil_type', 'like', '%'.$queryw.'%')
						->orWhere('irrigation_level', 'like', '%'.$queryw.'%')
						->orWhere('cultivation_period', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $locations = DB::table('locations')->where('id', $queryw)->first();
            if ($locations){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $locations], 200);
            }

        }
        else
        $locations = Location::get();

        if (count($locations) > 0){
            return response()->json(['status' => 'success', 'count' => count($locations), 'data' => $locations], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($locations), 'data' => array() ,'message' => 'Failed! no locations found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['location_name'] = $data['location_name'];
		$saveData['location_code'] = $data['location_code'];
		$saveData['location_pincode'] = $data['location_pincode'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];
		$saveData['weather_zone'] = $data['weather_zone'];
		$saveData['soil_type'] = $data['soil_type'];
		$saveData['irrigation_level'] = $data['irrigation_level'];
		$saveData['cultivation_period'] = $data['cultivation_period'];
		$saveData['status'] = $data['status'];

        $locations = Location::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $locations], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['location_name'] = $data['location_name'];
		$saveData['location_code'] = $data['location_code'];
		$saveData['location_pincode'] = $data['location_pincode'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];
		$saveData['weather_zone'] = $data['weather_zone'];
		$saveData['soil_type'] = $data['soil_type'];
		$saveData['irrigation_level'] = $data['irrigation_level'];
		$saveData['cultivation_period'] = $data['cultivation_period'];
		$saveData['status'] = $data['status'];

        $row = Location::where('id', $id)->first();
        if ($row){
            $Location = Location::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $Location], 200);
    }

    public function delete(Request $request)
    {
        $delete = Location::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
