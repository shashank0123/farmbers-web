<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\SoilType;
//add models here

class SoilTypeController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $soil_types = DB::table('soil_types')->where('id', '%'.$queryw.'%')
						->orWhere('soil_type', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $soil_types = DB::table('soil_types')->where('id', $queryw)->first();
            if ($soil_types){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $soil_types], 200);
            }

        }
        else
        $soil_types = SoilType::get();

        if (count($soil_types) > 0){
            return response()->json(['status' => 'success', 'count' => count($soil_types), 'data' => $soil_types], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($soil_types), 'data' => array() ,'message' => 'Failed! no soil_types found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['soil_type'] = $data['soil_type'];
		$saveData['status'] = $data['status'];

        $soil_types = SoilType::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $soil_types], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['soil_type'] = $data['soil_type'];
		$saveData['status'] = $data['status'];

        $row = SoilType::where('id', $id)->first();
        if ($row){
            $SoilType = SoilType::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $SoilType], 200);
    }

    public function delete(Request $request)
    {
        $delete = SoilType::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
