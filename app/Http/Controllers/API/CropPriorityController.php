<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CropPriority;
//add models here

class CropPriorityController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $crop_priorities = DB::table('crop_priorities')->where('id', '%'.$queryw.'%')
						->orWhere('crop_id', 'like', '%'.$queryw.'%')
						->orWhere('latitude', 'like', '%'.$queryw.'%')
						->orWhere('longitude', 'like', '%'.$queryw.'%')
						->orWhere('pincode', 'like', '%'.$queryw.'%')
						->orWhere('priority', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $crop_priorities = DB::table('crop_priorities')->where('id', $queryw)->first();
            if ($crop_priorities){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $crop_priorities], 200);
            }

        }
        else
        $crop_priorities = CropPriority::get();

        if (count($crop_priorities) > 0){
            return response()->json(['status' => 'success', 'count' => count($crop_priorities), 'data' => $crop_priorities], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($crop_priorities), 'data' => array() ,'message' => 'Failed! no crop_priorities found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['priority'] = $data['priority'];
		$saveData['status'] = $data['status'];

        $crop_priorities = CropPriority::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $crop_priorities], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['crop_id'] = $data['crop_id'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['priority'] = $data['priority'];
		$saveData['status'] = $data['status'];

        $row = CropPriority::where('id', $id)->first();
        if ($row){
            $CropPriority = CropPriority::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $CropPriority], 200);
    }

    public function delete(Request $request)
    {
        $delete = CropPriority::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
