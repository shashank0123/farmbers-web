<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Buyer;
//add models here

class BuyerController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $buyers = DB::table('buyers')->where('id', '%'.$queryw.'%')
						->orWhere('buyer_name', 'like', '%'.$queryw.'%')
						->orWhere('buyer_type', 'like', '%'.$queryw.'%')
						->orWhere('address', 'like', '%'.$queryw.'%')
						->orWhere('pincode', 'like', '%'.$queryw.'%')
						->orWhere('latitude', 'like', '%'.$queryw.'%')
						->orWhere('longitude', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $buyers = DB::table('buyers')->where('id', $queryw)->first();
            if ($buyers){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $buyers], 200);
            }

        }
        else
        $buyers = Buyer::get();

        if (count($buyers) > 0){
            return response()->json(['status' => 'success', 'count' => count($buyers), 'data' => $buyers], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($buyers), 'data' => array() ,'message' => 'Failed! no buyers found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['buyer_name'] = $data['buyer_name'];
		$saveData['buyer_type'] = $data['buyer_type'];
		$saveData['address'] = $data['address'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];
		$saveData['status'] = $data['status'];

        $buyers = Buyer::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $buyers], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['buyer_name'] = $data['buyer_name'];
		$saveData['buyer_type'] = $data['buyer_type'];
		$saveData['address'] = $data['address'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];
		$saveData['status'] = $data['status'];

        $row = Buyer::where('id', $id)->first();
        if ($row){
            $Buyer = Buyer::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $Buyer], 200);
    }

    public function delete(Request $request)
    {
        $delete = Buyer::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
