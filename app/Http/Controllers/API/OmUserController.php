<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\OmUser;
//add models here

class OmUserController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $om_users = DB::table('om_users')->where('id', '%'.$queryw.'%')
						->orWhere('name', 'like', '%'.$queryw.'%')
						->orWhere('mobile_no', 'like', '%'.$queryw.'%')
						->orWhere('email', 'like', '%'.$queryw.'%')
						->orWhere('google_id', 'like', '%'.$queryw.'%')
						->orWhere('facebook_id', 'like', '%'.$queryw.'%')
						->orWhere('password', 'like', '%'.$queryw.'%')
						->orWhere('mobile_verified_date', 'like', '%'.$queryw.'%')
						->orWhere('email_verified_date', 'like', '%'.$queryw.'%')
						->orWhere('remember_token', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $om_users = DB::table('om_users')->where('id', $queryw)->first();
            if ($om_users){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $om_users], 200);
            }

        }
        else
        $om_users = OmUser::get();

        if (count($om_users) > 0){
            return response()->json(['status' => 'success', 'count' => count($om_users), 'data' => $om_users], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($om_users), 'message' => 'Failed! no om_users found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['name'] = $data['name'];
		$saveData['mobile_no'] = $data['mobile_no'];
		$saveData['email'] = $data['email'];
		$saveData['google_id'] = $data['google_id'];
		$saveData['facebook_id'] = $data['facebook_id'];
		$saveData['password'] = $data['password'];
		$saveData['mobile_verified_date'] = $data['mobile_verified_date'];
		$saveData['email_verified_date'] = $data['email_verified_date'];
		$saveData['remember_token'] = $data['remember_token'];
		$saveData['status'] = $data['status'];

        $om_users = OmUser::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $om_users], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['name'] = $data['name'];
		$saveData['mobile_no'] = $data['mobile_no'];
		$saveData['email'] = $data['email'];
		$saveData['google_id'] = $data['google_id'];
		$saveData['facebook_id'] = $data['facebook_id'];
		$saveData['password'] = $data['password'];
		$saveData['mobile_verified_date'] = $data['mobile_verified_date'];
		$saveData['email_verified_date'] = $data['email_verified_date'];
		$saveData['remember_token'] = $data['remember_token'];
		$saveData['status'] = $data['status'];

        $row = OmUser::where('id', $id)->first();
        if ($row){
            $OmUser = OmUser::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $OmUser], 200);
    }

    public function delete(Request $request)
    {
        $delete = OmUser::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
