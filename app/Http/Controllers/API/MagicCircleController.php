<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\MagicCircle;
//add models here

class MagicCircleController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $magic_circles = DB::table('magic_circles')->where('id', '%'.$queryw.'%')
						->orWhere('mc_name', 'like', '%'.$queryw.'%')
						->orWhere('pincode', 'like', '%'.$queryw.'%')
						->orWhere('radius', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')
						->orWhere('latitude', 'like', '%'.$queryw.'%')
						->orWhere('longitude', 'like', '%'.$queryw.'%')->orderBy('id', 'DESC')->get();

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $magic_circles = DB::table('magic_circles')->where('id', $queryw)->first();
            if ($magic_circles){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $magic_circles], 200);
            }

        }
        else
        $magic_circles = MagicCircle::get();

        if (count($magic_circles) > 0){
            return response()->json(['status' => 'success', 'count' => count($magic_circles), 'data' => $magic_circles], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($magic_circles), 'data' => array() ,'message' => 'Failed! no magic_circles found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['mc_name'] = $data['mc_name'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['radius'] = $data['radius'];
		$saveData['status'] = $data['status'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];

        $magic_circles = MagicCircle::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $magic_circles], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['mc_name'] = $data['mc_name'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['radius'] = $data['radius'];
		$saveData['status'] = $data['status'];
		$saveData['latitude'] = $data['latitude'];
		$saveData['longitude'] = $data['longitude'];

        $row = MagicCircle::where('id', $id)->first();
        if ($row){
            $MagicCircle = MagicCircle::where('id', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $MagicCircle], 200);
    }

    public function delete(Request $request)
    {
        $delete = MagicCircle::where('id', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
