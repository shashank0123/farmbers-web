<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use stdClass;

class ReactBaseController extends Controller
{
    public function getMenu(Request $request)
    {
        $response = new stdClass;
        $response = array(
            "0" => 'Crop',
            "1" => 'Food',
        );


        return response()->json($response, 200, []);
    }
}
