<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class UserActionController extends Controller
{
    public function logout(Request $request)
    {
    	$logout = Auth::logout();
    	return redirect()->route('login');
    }

    public function getProfile(Request $request)
    {
    	$id = Auth::user()->id;
    	$user = User::find($id);
    	return view('dashboard.profile.index', compact('user'));
    }

    public function editProfileForm(Request $request)
    {
    	$id = Auth::user()->id;
    	$user = User::find($id);
    	return view('dashboard.profile.edit', compact('user'));
    }

    public function updateProfile(Request $request)
    {
    	$id = Auth::user()->id;
    	$user = User::find($id);
    	$user->name = $request->name; 	
    	$user->email = $request->email; 
    	if ($request->file('profile_photo_path')) {
    		$file = $request->file('profile_photo_path');
    		@unlink(public_path('uploads/user_image/'.$user->profile_photo_path));
    		$filename = date('Ymdhi').$user->id.$file->getClientOriginalName();
    		$file->move(public_path('uploads/user_image'), $filename);
    		$user->profile_photo_path = $filename;
		}	
		$user->update();
		$notification = array(
			'message' => 'Profile Updated Successfully',
			'alert-type' => 'success'
		);
    	return redirect()->back()->with($notification);
    }
}
