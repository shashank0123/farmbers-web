<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(Request $request) {
        $credentials = $request->only('mobile');
        $user = User::where('mobile', $request->mobile)->first();
        if ($user){
            $request->email = $request->mobile;
            $mobile = $request->mobile_no;
            $name = 0;
            // return $this->login($request);
        }
        else{

            $rules = [
                'mobile' => 'required|unique:users',
            ];
            $validator = Validator::make($credentials, $rules);
            if($validator->fails()) {
                return response()->json(['success'=> false, 'error'=> $validator->messages()]);
            }
            $password = '123456';
            $name = 'user';
            $mobile = $request->mobile;
            $email_token = base64_encode($request->mobile);
            $randphone = '123456';
            $user = User::create(['mobile' => $mobile, 'name' => $name, 'email' => $mobile, 'password' => Hash::make($password)]);
            $name = 1;
        }

            return response()->json(['success' => true, 'message' => 'Success. OTP Sent', 'name'=> $name], 200);

    }

    // User login
    public function login(Request $request) {
        $request->password = $request->password;
        $request->email = $request->mobile;
        if ($request->otp != 'undefined'){

            $request->password = $request->otp;
        }
        else{
            $request->password = $request->pin;
        }
        $email = $request->mobile;
        $password = $request->password;
        $request->email = $request->mobile;
        $validator = Validator::make($request->all(), [
            "mobile" =>  "required"
        ]);



        if($validator->fails()) {
            return response()->json(["validation_errors" => $validator->errors()]);
        }

        $user = User::where('mobile', $request->mobile)->first();

        
        if(Auth::attempt(['email' => $request->email, 'password' => $password])){
            $user       =       Auth::user();
            $token      =       $user->createToken('token')->plainTextToken;

            if ($request->otp != 'undefined'){
                $user = User::where('mobile', $request->mobile)->first();
                if ($request->otp != 'undefined'){
                    $user->name = $request->name;
                    
                }
                if ($user){
                    $user->password = Hash::make($request->pin);
                    $user->update();
                }
            }


            return response()->json(["status" => "success", "login" => true, "token" => $token, "data" => $user]);
        }
        else {
            return response()->json(["status" => "failed", "success" => false, "message" => "Whoops! invalid password"]);
        }
    }
public function forgotPin(Request $request)
    {
        $validator  =   Validator::make($request->all(), [
            "mobile"  =>  "required",
        ]);

        if($validator->fails()) {
            return response()->json(["status" => "failed", "validation_errors" => $validator->errors()]);
        }

       
        if($request->mobile) {
            return response()->json(["status" => "success", "message" => "Success! OTP SENT", "data" => $request->otp]);
        }
        else {
            return response()->json(["status" => "failed", "message" => "OTP Sending Failed"]);
        }    
    }

    public function updatePin(Request $request)
    {
        $validator  =   Validator::make($request->all(), [
            "mobile"  =>  "required",
            "otp"  =>  "required",
            "pin"  =>  "required",
        ]);

        if($validator->fails()) {
            return response()->json(["status" => "failed", "validation_errors" => $validator->errors()]);
        }

        if ($request->otp == '123456'){
            $user = User::where('mobile', $request->mobile)->first();
            if ($user){
                $user->password = Hash::make($request->pin);
                $user->update();
            }
            $request->otp = 'undefined';
            return $this->login($request);

        }
        else{
            return response()->json(["status" => "failed", "message" => "OTP Wrong"]);
        }

        
       
        if($request->mobile) {
            return response()->json(["status" => "success", "message" => "Success! OTP SENT", "data" => $request->otp]);
        }
        else {
            return response()->json(["status" => "failed", "message" => "OTP Sending Failed"]);
        }    
    }


    // User Detail
    public function user() {
        $user       =       Auth::user();
        if(!is_null($user)) {
            return response()->json(["status" => "success", "data" => $user]);
        }

        else {
            return response()->json(["status" => "failed", "message" => "Whoops! no user found"]);
        }
    }
}
