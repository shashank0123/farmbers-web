<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
    use App\Http\Controllers\admin\AdminBasicController;
            // add routes here

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// add web routes here
Route::view('/{path?}', 'admin');
Route::view('/{path?}', 'admin');
Route::view('/admin/', 'admin');
Route::view('/admin/{path?}', 'admin');
Route::view('/admin/module/{path?}', 'admin');
Route::view('/{usertype}/{module?}/{task?}', 'admin');
Route::view('/{usertype}/', 'admin');
