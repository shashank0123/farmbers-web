<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminRegisterController;
use App\Http\Controllers\AppController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\API\WeatherZoneController;
use App\Http\Controllers\API\WeatherImpactController;
use App\Http\Controllers\API\WeatherDelayController;
use App\Http\Controllers\API\UserMagicCircleController;
use App\Http\Controllers\API\UserLandController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\UserCropStageAdjController;
use App\Http\Controllers\API\UserCropStageController;
use App\Http\Controllers\API\SoilTypeController;
use App\Http\Controllers\API\MagicCircleController;
use App\Http\Controllers\API\LocationController;
use App\Http\Controllers\API\LandCropController;
use App\Http\Controllers\API\LandController;
use App\Http\Controllers\API\CropStageController;
use App\Http\Controllers\API\CropPriorityController;
use App\Http\Controllers\API\CropLocationController;
use App\Http\Controllers\API\CropController;
use App\Http\Controllers\API\BuyerCropParamController;
use App\Http\Controllers\API\BuyerCropDemandController;
use App\Http\Controllers\API\BuyerController;
use App\Http\Controllers\ReactBaseController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);
Route::post('otp', [RegisterController::class, 'login']);
Route::post('admin/login', [AdminRegisterController::class, 'login']);
Route::post('forgot-pin', [RegisterController::class, 'forgotPin']);
Route::post('reset-pin', [RegisterController::class, 'updatePin']);
Route::get('languages', [AppController::class, 'allLanguage']);
Route::get('location_name', [AppController::class, 'locationName']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('profile', [ProfileController::class, 'getProfile'])->middleware('auth:sanctum');
Route::get('dashboard', [AppController::class, 'getDashboard'])->middleware('auth:sanctum');
Route::get('get-magic-circle', [AppController::class, 'getMagicCircle'])->middleware('auth:sanctum');
Route::post('save-farm', [AppController::class, 'saveFarm'])->middleware('auth:sanctum');
Route::post('profile', [ProfileController::class, 'saveProfile'])->middleware('auth:sanctum');
Route::get('notifications', [ProfileController::class, 'allNotification'])->middleware('auth:sanctum');
Route::post('user_language', [AppController::class, 'userLanguage'])->middleware('auth:sanctum');
Route::post('user_farm', [AppController::class, 'userLanguage'])->middleware('auth:sanctum');
Route::get('user_crop', [AppController::class, 'getUserCrop'])->middleware('auth:sanctum');
Route::post('user_crop', [AppController::class, 'userCrop'])->middleware('auth:sanctum');
Route::get('user_crop_stage', [AppController::class, 'userCropStage'])->middleware('auth:sanctum');
Route::get('best_crops', [AppController::class, 'bestCrop'])->middleware('auth:sanctum');
Route::get('buyer_dashboard', [AppController::class, 'buyerDashboard'])->middleware('auth:sanctum');
Route::get('weather_data', [AppController::class, 'weatherData'])->middleware('auth:sanctum');
Route::get('soil_data', [AppController::class, 'soilData'])->middleware('auth:sanctum');
Route::post('help', [AppController::class, 'saveHelp'])->middleware('auth:sanctum');
Route::post('notifications', [AppController::class, 'saveNotification'])->middleware('auth:sanctum');


Route::get('/weather_zones', [WeatherZoneController::class, 'index']);
Route::post('/weather_zones/add', [WeatherZoneController::class, 'store']);
Route::post('/weather_zones/edit/{id}', [WeatherZoneController::class, 'update']);
Route::get('/weather_zones/delete/{id}', [WeatherZoneController::class, 'delete']);
Route::get('/weather_impacts', [WeatherImpactController::class, 'index']);
Route::post('/weather_impacts/add', [WeatherImpactController::class, 'store']);
Route::post('/weather_impacts/edit/{id}', [WeatherImpactController::class, 'update']);
Route::get('/weather_impacts/delete/{id}', [WeatherImpactController::class, 'delete']);
Route::get('/weather_delays', [WeatherDelayController::class, 'index']);
Route::post('/weather_delays/add', [WeatherDelayController::class, 'store']);
Route::post('/weather_delays/edit/{id}', [WeatherDelayController::class, 'update']);
Route::get('/weather_delays/delete/{id}', [WeatherDelayController::class, 'delete']);
Route::get('/user_magic_circles', [UserMagicCircleController::class, 'index']);
Route::post('/user_magic_circles/add', [UserMagicCircleController::class, 'store']);
Route::post('/user_magic_circles/edit/{id}', [UserMagicCircleController::class, 'update']);
Route::get('/user_magic_circles/delete/{id}', [UserMagicCircleController::class, 'delete']);
Route::get('/user_lands', [UserLandController::class, 'index']);
Route::post('/user_lands/add', [UserLandController::class, 'store']);
Route::post('/user_lands/edit/{id}', [UserLandController::class, 'update']);
Route::get('/user_lands/delete/{id}', [UserLandController::class, 'delete']);
Route::get('/user_crop_stage_adjs', [UserCropStageAdjController::class, 'index']);
Route::post('/user_crop_stage_adjs/add', [UserCropStageAdjController::class, 'store']);
Route::post('/user_crop_stage_adjs/edit/{id}', [UserCropStageAdjController::class, 'update']);
Route::get('/user_crop_stage_adjs/delete/{id}', [UserCropStageAdjController::class, 'delete']);
Route::get('/user_crop_stages', [UserCropStageController::class, 'index']);
Route::post('/user_crop_stages/add', [UserCropStageController::class, 'store']);
Route::post('/user_crop_stages/edit/{id}', [UserCropStageController::class, 'update']);
Route::get('/user_crop_stages/delete/{id}', [UserCropStageController::class, 'delete']);
Route::get('/soil_types', [SoilTypeController::class, 'index']);
Route::post('/soil_types/add', [SoilTypeController::class, 'store']);
Route::post('/soil_types/edit/{id}', [SoilTypeController::class, 'update']);
Route::get('/soil_types/delete/{id}', [SoilTypeController::class, 'delete']);
Route::get('/magic_circles', [MagicCircleController::class, 'index']);
Route::post('/magic_circles/add', [MagicCircleController::class, 'store']);
Route::post('/magic_circles/edit/{id}', [MagicCircleController::class, 'update']);
Route::get('/magic_circles/delete/{id}', [MagicCircleController::class, 'delete']);
Route::get('/locations', [LocationController::class, 'index']);
Route::post('/locations/add', [LocationController::class, 'store']);
Route::post('/locations/edit/{id}', [LocationController::class, 'update']);
Route::get('/locations/delete/{id}', [LocationController::class, 'delete']);
Route::get('/land_crops', [LandCropController::class, 'index']);
Route::post('/land_crops/add', [LandCropController::class, 'store']);
Route::post('/land_crops/edit/{id}', [LandCropController::class, 'update']);
Route::get('/land_crops/delete/{id}', [LandCropController::class, 'delete']);
Route::get('/lands', [LandController::class, 'index']);
Route::post('/lands/add', [LandController::class, 'store']);
Route::post('/lands/edit/{id}', [LandController::class, 'update']);
Route::get('/lands/delete/{id}', [LandController::class, 'delete']);
Route::get('/crop_stages', [CropStageController::class, 'index']);
Route::post('/crop_stages/add', [CropStageController::class, 'store']);
Route::post('/crop_stages/edit/{id}', [CropStageController::class, 'update']);
Route::get('/crop_stages/delete/{id}', [CropStageController::class, 'delete']);
Route::get('/crop_priorities', [CropPriorityController::class, 'index']);
Route::post('/crop_priorities/add', [CropPriorityController::class, 'store']);
Route::post('/crop_priorities/edit/{id}', [CropPriorityController::class, 'update']);
Route::get('/crop_priorities/delete/{id}', [CropPriorityController::class, 'delete']);
Route::get('/crop_locations', [CropLocationController::class, 'index']);
Route::post('/crop_locations/add', [CropLocationController::class, 'store']);
Route::post('/crop_locations/edit/{id}', [CropLocationController::class, 'update']);
Route::get('/crop_locations/delete/{id}', [CropLocationController::class, 'delete']);
Route::get('/crops', [CropController::class, 'index']);
Route::post('/crops/add', [CropController::class, 'store']);
Route::post('/crops/edit/{id}', [CropController::class, 'update']);
Route::get('/crops/delete/{id}', [CropController::class, 'delete']);
Route::get('/buyer_crop_params', [BuyerCropParamController::class, 'index']);
Route::post('/buyer_crop_params/add', [BuyerCropParamController::class, 'store']);
Route::post('/buyer_crop_params/edit/{id}', [BuyerCropParamController::class, 'update']);
Route::get('/buyer_crop_params/delete/{id}', [BuyerCropParamController::class, 'delete']);
Route::get('/buyer_crop_demands', [BuyerCropDemandController::class, 'index']);
Route::post('/buyer_crop_demands/add', [BuyerCropDemandController::class, 'store']);
Route::post('/buyer_crop_demands/edit/{id}', [BuyerCropDemandController::class, 'update']);
Route::get('/buyer_crop_demands/delete/{id}', [BuyerCropDemandController::class, 'delete']);
Route::get('/buyers', [BuyerController::class, 'index']);
Route::post('/buyers/add', [BuyerController::class, 'store']);
Route::post('/buyers/edit/{id}', [BuyerController::class, 'update']);
Route::get('/buyers/delete/{id}', [BuyerController::class, 'delete']);
Route::get('/users', [UserController::class, 'index']);
Route::get('menu', [ReactBaseController::class , 'getMenu']);
