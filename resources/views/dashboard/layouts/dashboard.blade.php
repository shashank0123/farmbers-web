<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="{{ asset('dash/images/favicon.png') }}" >
        <!--Page title-->
        <title>Admin easy Learning</title>
        <!--bootstrap-->
        <link rel="stylesheet" href="{{ asset('dash/css/bootstrap.min.css') }}">
        <!--font awesome-->
        <link rel="stylesheet" href="{{ asset('dash/css/all.css') }}">
        <!-- metis menu -->
        <link rel="stylesheet" href="{{ asset('dash/plugins/metismenu-3.0.4/assets/css/metisMenu.min.css') }}">
        <link rel="stylesheet" href="{{ asset('dash/plugins/metismenu-3.0.4/assets/css/mm-vertical-hover.css') }}">
        <!-- chart -->
   
        <!-- <link rel="stylesheet" href="assets/plugins/chartjs-bar-chart/chart.css') }}"> -->
        <!--Custom CSS-->
        <link rel="stylesheet" href="{{ asset('dash/css/style.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    </head>
    <body id="page-top">
        <!-- preloader -->
        <div class="preloader">
            <img src="{{ asset('dash/images/preloader.gif') }}" alt="">
        </div>
        <!-- wrapper -->
        <div class="wrapper">
         
            @include('dashboard.layouts.header')
            @include('dashboard.layouts.sidebar')
            


         


  
<div class="content_wrapper">
    <!--middle content wrapper-->
    <div class="middle_content_wrapper">
        @yield('mainarea')
    </div>
</div><!--/ content wrapper -->










        </div><!--/ wrapper -->


        
        <!-- jquery -->
        <script src="{{ asset('dash/js/jquery.min.js') }}"></script>
        <!-- popper Min Js -->
        <script src="{{ asset('dash/js/popper.min.js') }}"></script>
        <!-- Bootstrap Min Js -->
        <script src="{{ asset('dash/js/bootstrap.min.js') }}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- Fontawesome-->
        <script src="{{ asset('dash/js/all.min.js') }}"></script>
        <!-- metis menu -->
        <script src="{{ asset('dash/plugins/metismenu-3.0.4/assets/js/metismenu.js') }}"></script>
        <script src="{{ asset('dash/plugins/metismenu-3.0.4/assets/js/mm-vertical-hover.js') }}"></script>
        <!-- nice scroll bar -->
        <script src="{{ asset('dash/plugins/scrollbar/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('dash/plugins/scrollbar/scroll.active.js') }}"></script>
        <!-- counter -->
        <script src="{{ asset('dash/plugins/counter/js/counter.js') }}"></script>
        <!-- chart -->
   <script src="{{ asset('dash/plugins/chartjs-bar-chart/Chart.min.js') }}"></script>
        <script src="{{ asset('dash/plugins/chartjs-bar-chart/chart.js') }}"></script>
        <!-- pie chart -->
        <script src="{{ asset('dash/plugins/pie_chart/chart.loader.js') }}"></script>
        <script src="{{ asset('dash/plugins/pie_chart/pie.active.js') }}"></script>
 
 
        <!-- Main js -->
        <script src="{{ asset('dash/js/main.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script>
            @if (Session::has('message'))
                var type = "{{ Session::get('alert-type', 'info') }}"
                switch(type){
                    case 'info':
                    toastr.info("  {{ Session::get('message') }}  ");
                    break;

                    case 'success':
                    toastr.success("  {{ Session::get('message') }}  ");
                    break;

                    case 'warning':
                    toastr.warning("  {{ Session::get('message') }}  ");
                    break;

                    case 'error':
                    toastr.error("  {{ Session::get('message') }}  ");
                    break;
                }
            @endif
        </script>
    
     @yield('customscrips')


    </body>
</html>