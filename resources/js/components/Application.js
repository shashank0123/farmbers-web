import React, { Component, Suspense, lazy } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import ReactDOM from 'react-dom';
import { Provider } from "react-redux"
import * as serviceWorker from "./serviceWorker"
import { store } from "../redux/store"
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import AdminLayout from './layouts/AdminLayout'
import Layout from './layouts/Layout'
import UserLayout from './layouts/UserLayout'
import Login from './Login'

class Application extends Component {



    constructor(props){
        super(props)
        
    }



    render(){
    return (
        <>
        <Provider store={store}>
      <Suspense fallback={<p>Loading...</p>}>

          <Router>
            <Switch>
              <Route exact path="/login" render={(props) => <Login {...props} /> } />
              <Route path='/admin' render={(props) => <AdminLayout {...props} /> } />
              <Route path='/user' render={(props) => <UserLayout {...props} /> } />
              <Route exact path='/' render={(props) => <Layout {...props} /> } />
            </Switch>
          </Router>
      </Suspense>
    </Provider>



        </>
    );
    }
}

export default Application;

if (document.getElementById('app')) {
    ReactDOM.render(<Application />, document.getElementById('app'));
}
