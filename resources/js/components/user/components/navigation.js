import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import UserDashboard from './UserDashboard'

class UserNavigation extends Component{

    constructor(props){
        super(props)
    }

 render() {
     const { match } = this.props
     console.log(this.props)
     console.log(this.props.match)
    return (

        <Switch>
            <Route 
                exact path={'/user'}
                component={UserDashboard}
                    />
            <Route path={'user/dashboard'} component={UserDashboard} />
        </Switch>

    );
}
}

export default UserNavigation;
