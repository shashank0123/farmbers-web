import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_buyer_crop_demands } from '../../../../redux/actions/buyer_crop_demands';
import { save_buyer_crop_demands } from '../../../../redux/actions/buyer_crop_demands';
import { get_buyer_crop_demands } from '../../../../redux/actions/buyer_crop_demands';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_buyers } from '../../../../redux/actions/buyers'; 
import { view_crops } from '../../../../redux/actions/crops'; 

class ModifyBuyerCropDemand extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        buyer_id : '',crop_id : '',pincode : '',demand : '',status : '',buyers : [],crops : [],query: ''
    }

    this.handlebuyer_idChange = this.handlebuyer_idChange.bind(this)
            this.handlecrop_idChange = this.handlecrop_idChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_buyer_crop_demands('token', this.state.id).then(() => {
                this.setState({item : this.props.get_buyer_crop_demandss});
                this.setState({editpage : 1});
                				this.setState({ buyer_id : this.props.get_buyer_crop_demandss.buyer_id }) 
				this.setState({ crop_id : this.props.get_buyer_crop_demandss.crop_id }) 
				this.setState({ pincode : this.props.get_buyer_crop_demandss.pincode }) 
				this.setState({ demand : this.props.get_buyer_crop_demandss.demand }) 
				this.setState({ status : this.props.get_buyer_crop_demandss.status }) 

                })
            console.log(this.props.get_buyer_crop_demandss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_buyers(this.state).then(()=> {
                    if (this.props.view_buyerss != undefined){
                        this.setState({ buyers  : this.props.view_buyerss })    
                        this.renderOptionbuyers()
                    }

                    
                })
        
            

            this.props.view_crops(this.state).then(()=> {
                    if (this.props.view_cropss != undefined){
                        this.setState({ crops  : this.props.view_cropss })    
                        this.renderOptioncrops()
                    }

                    
                })
        
    }


    renderOptionbuyers(){
            console.log(this.props.view_buyerss, 'ye aa rha hai')
            var returnbuyers = <></>;
                    if (typeof this.props.view_buyerss != 'undefined')
                    if ( this.props.view_buyerss.length > 0)
                    returnbuyers =  this.props.view_buyerss.map(function(element){
                        return <option key={element.buyer_id} value={element.buyer_id}>{element.buyer_name}</option>
                        })       

               
            return <>{returnbuyers}</>
        }renderOptioncrops(){
            console.log(this.props.view_cropss, 'ye aa rha hai')
            var returncrops = <></>;
                    if (typeof this.props.view_cropss != 'undefined')
                    if ( this.props.view_cropss.length > 0)
                    returncrops =  this.props.view_cropss.map(function(element){
                        return <option key={element.crop_id} value={element.crop_id}>{element.crop_name}</option>
                        })       

               
            return <>{returncrops}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_buyer_crop_demands(this.state).then(()=> {
            console.log(this.props.save_buyer_crop_demandss)
            history.push('/admin/buyer_crop_demands');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_buyer_crop_demands(this.state).then(()=> {
            console.log(this.props.save_buyer_crop_demandss)
            history.push('/admin/buyer_crop_demands');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlebuyer_idChange(e){
            this.setState({ buyer_id: e.target.value });
        }
        handlecrop_idChange(e){
            this.setState({ crop_id: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        handlebuyer_idChange(e){
            this.setState({ buyer_id: e.target.value });
        }
        handlecrop_idChange(e){
            this.setState({ crop_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
                              <label  className="form-label">Buyer id</label>
                              <select onChange={(e) => this.handlebuyer_idChange(e)} value={this.state.buyer_id} className="form-control" >
                                  <option>Select Buyer id</option>
                                  {this.renderOptionbuyers()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Crop id</label>
                              <select onChange={(e) => this.handlecrop_idChange(e)} value={this.state.crop_id} className="form-control" >
                                  <option>Select Crop id</option>
                                  {this.renderOptioncrops()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Pincode</label>
              <input type="text" value={this.state.pincode} onChange={(e) => {this.setState({pincode: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Demand</label>
              <input type="text" value={this.state.demand} onChange={(e) => {this.setState({demand: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_buyerss: state.buyers.view_buyers,view_cropss: state.crops.view_crops,save_buyer_crop_demandss : state.buyer_crop_demands.save_buyer_crop_demands, 
        update_buyer_crop_demandss : state.buyer_crop_demands.update_buyer_crop_demands,
        get_buyer_crop_demandss : state.buyer_crop_demands.get_buyer_crop_demands,
    };
  };

  export default connect(mapStateToProps, {
    view_buyers,view_crops, save_buyer_crop_demands, update_buyer_crop_demands, get_buyer_crop_demands
  })(ModifyBuyerCropDemand);


