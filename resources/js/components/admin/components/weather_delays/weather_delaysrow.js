import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_weather_delays } from '../../../../redux/actions/weather_delays';

class WeatherDelayRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_weather_delays(id, 'token').then(()=>{console.log(this.props.delete_weather_delayss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/weather_delays/edit/'+this.state.row.id
                var url2 = '/admin/weather_delays/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.pincode}</td>
<td>{this.state.row.latitude}</td>
<td>{this.state.row.longitude}</td>
<td>{this.state.row.crop_id}</td>
<td>{this.state.row.stage_id}</td>
<td>{this.state.row.delay_reason}</td>
<td>{this.state.row.delay}</td>
<td>{this.state.row.message}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_weather_delayss : state.weather_delays.delete_weather_delays, 
    };
  };

  export default connect(mapStateToProps, {
     delete_weather_delays
  })(WeatherDelayRow);
