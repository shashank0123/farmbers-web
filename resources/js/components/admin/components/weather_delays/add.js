import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_weather_delays } from '../../../../redux/actions/weather_delays';
import { save_weather_delays } from '../../../../redux/actions/weather_delays';
import { get_weather_delays } from '../../../../redux/actions/weather_delays';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_crops } from '../../../../redux/actions/crops'; 

class ModifyWeatherDelay extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        pincode : '',latitude : '',longitude : '',crop_id : '',stage_id : '',delay_reason : '',delay : '',message : '',status : '',crops : [],query: ''
    }

    this.handlecrop_idChange = this.handlecrop_idChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_weather_delays('token', this.state.id).then(() => {
                this.setState({item : this.props.get_weather_delayss});
                this.setState({editpage : 1});
                				this.setState({ pincode : this.props.get_weather_delayss.pincode }) 
				this.setState({ latitude : this.props.get_weather_delayss.latitude }) 
				this.setState({ longitude : this.props.get_weather_delayss.longitude }) 
				this.setState({ crop_id : this.props.get_weather_delayss.crop_id }) 
				this.setState({ stage_id : this.props.get_weather_delayss.stage_id }) 
				this.setState({ delay_reason : this.props.get_weather_delayss.delay_reason }) 
				this.setState({ delay : this.props.get_weather_delayss.delay }) 
				this.setState({ message : this.props.get_weather_delayss.message }) 
				this.setState({ status : this.props.get_weather_delayss.status }) 

                })
            console.log(this.props.get_weather_delayss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_crops(this.state).then(()=> {
                    if (this.props.view_cropss != undefined){
                        this.setState({ crops  : this.props.view_cropss })    
                        this.renderOptioncrops()
                    }

                    
                })
        
    }


    renderOptioncrops(){
            console.log(this.props.view_cropss, 'ye aa rha hai')
            var returncrops = <></>;
                    if (typeof this.props.view_cropss != 'undefined')
                    if ( this.props.view_cropss.length > 0)
                    returncrops =  this.props.view_cropss.map(function(element){
                        return <option key={element.crop_id} value={element.crop_id}>{element.crop_name}</option>
                        })       

               
            return <>{returncrops}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_weather_delays(this.state).then(()=> {
            console.log(this.props.save_weather_delayss)
            history.push('/admin/weather_delays');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_weather_delays(this.state).then(()=> {
            console.log(this.props.save_weather_delayss)
            history.push('/admin/weather_delays');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlecrop_idChange(e){
            this.setState({ crop_id: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        handlecrop_idChange(e){
            this.setState({ crop_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">Pincode</label>
              <input type="text" value={this.state.pincode} onChange={(e) => {this.setState({pincode: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Latitude</label>
              <input type="text" value={this.state.latitude} onChange={(e) => {this.setState({latitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Longitude</label>
              <input type="text" value={this.state.longitude} onChange={(e) => {this.setState({longitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Crop id</label>
                              <select onChange={(e) => this.handlecrop_idChange(e)} value={this.state.crop_id} className="form-control" >
                                  <option>Select Crop id</option>
                                  {this.renderOptioncrops()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Stage id</label>
              <input type="text" value={this.state.stage_id} onChange={(e) => {this.setState({stage_id: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Delay reason</label>
              <input type="text" value={this.state.delay_reason} onChange={(e) => {this.setState({delay_reason: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Delay</label>
              <input type="text" value={this.state.delay} onChange={(e) => {this.setState({delay: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Message</label>
              <input type="text" value={this.state.message} onChange={(e) => {this.setState({message: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_cropss: state.crops.view_crops,save_weather_delayss : state.weather_delays.save_weather_delays, 
        update_weather_delayss : state.weather_delays.update_weather_delays,
        get_weather_delayss : state.weather_delays.get_weather_delays,
    };
  };

  export default connect(mapStateToProps, {
    view_crops, save_weather_delays, update_weather_delays, get_weather_delays
  })(ModifyWeatherDelay);


