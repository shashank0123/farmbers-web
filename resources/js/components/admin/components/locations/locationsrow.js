import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_locations } from '../../../../redux/actions/locations';

class LocationRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_locations(id, 'token').then(()=>{console.log(this.props.delete_locationss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/locations/edit/'+this.state.row.id
                var url2 = '/admin/locations/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.location_name}</td>
<td>{this.state.row.location_code}</td>
<td>{this.state.row.location_pincode}</td>
<td>{this.state.row.latitude}</td>
<td>{this.state.row.longitude}</td>
<td>{this.state.row.weather_zone}</td>
<td>{this.state.row.soil_type}</td>
<td>{this.state.row.irrigation_level}</td>
<td>{this.state.row.cultivation_period}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_locationss : state.locations.delete_locations, 
    };
  };

  export default connect(mapStateToProps, {
     delete_locations
  })(LocationRow);
