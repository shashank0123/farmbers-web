import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_locations } from '../../../../redux/actions/locations';
import { save_locations } from '../../../../redux/actions/locations';
import { get_locations } from '../../../../redux/actions/locations';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_weather_zones } from '../../../../redux/actions/weather_zones'; 
import { view_soil_types } from '../../../../redux/actions/soil_types'; 

class ModifyLocation extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        location_name : '',location_code : '',location_pincode : '',latitude : '',longitude : '',weather_zone : '',soil_type : '',irrigation_level : '',cultivation_period : '',status : '',weather_zones : [],soil_types : [],query: ''
    }

    this.handleweather_zoneChange = this.handleweather_zoneChange.bind(this)
            this.handlesoil_typeChange = this.handlesoil_typeChange.bind(this)
            this.handleirrigation_levelChange = this.handleirrigation_levelChange.bind(this)
            this.handlecultivation_periodChange = this.handlecultivation_periodChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_locations('token', this.state.id).then(() => {
                this.setState({item : this.props.get_locationss});
                this.setState({editpage : 1});
                				this.setState({ location_name : this.props.get_locationss.location_name }) 
				this.setState({ location_code : this.props.get_locationss.location_code }) 
				this.setState({ location_pincode : this.props.get_locationss.location_pincode }) 
				this.setState({ latitude : this.props.get_locationss.latitude }) 
				this.setState({ longitude : this.props.get_locationss.longitude }) 
				this.setState({ weather_zone : this.props.get_locationss.weather_zone }) 
				this.setState({ soil_type : this.props.get_locationss.soil_type }) 
				this.setState({ irrigation_level : this.props.get_locationss.irrigation_level }) 
				this.setState({ cultivation_period : this.props.get_locationss.cultivation_period }) 
				this.setState({ status : this.props.get_locationss.status }) 

                })
            console.log(this.props.get_locationss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_weather_zones(this.state).then(()=> {
                    if (this.props.view_weather_zoness != undefined){
                        this.setState({ weather_zones  : this.props.view_weather_zoness })    
                        this.renderOptionweather_zones()
                    }

                    
                })
        
            

            this.props.view_soil_types(this.state).then(()=> {
                    if (this.props.view_soil_typess != undefined){
                        this.setState({ soil_types  : this.props.view_soil_typess })    
                        this.renderOptionsoil_types()
                    }

                    
                })
        
    }


    renderOptionweather_zones(){
            console.log(this.props.view_weather_zoness, 'ye aa rha hai')
            var returnweather_zones = <></>;
                    if (typeof this.props.view_weather_zoness != 'undefined')
                    if ( this.props.view_weather_zoness.length > 0)
                    returnweather_zones =  this.props.view_weather_zoness.map(function(element){
                        return <option key={element.weather_zone} value={element.weather_zone}>{element.zone_name}</option>
                        })       

               
            return <>{returnweather_zones}</>
        }renderOptionsoil_types(){
            console.log(this.props.view_soil_typess, 'ye aa rha hai')
            var returnsoil_types = <></>;
                    if (typeof this.props.view_soil_typess != 'undefined')
                    if ( this.props.view_soil_typess.length > 0)
                    returnsoil_types =  this.props.view_soil_typess.map(function(element){
                        return <option key={element.soil_type} value={element.soil_type}>{element.soil_type}</option>
                        })       

               
            return <>{returnsoil_types}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_locations(this.state).then(()=> {
            console.log(this.props.save_locationss)
            history.push('/admin/locations');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_locations(this.state).then(()=> {
            console.log(this.props.save_locationss)
            history.push('/admin/locations');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handleweather_zoneChange(e){
            this.setState({ weather_zone: e.target.value });
        }
        handlesoil_typeChange(e){
            this.setState({ soil_type: e.target.value });
        }
        handleirrigation_levelChange(e){
            this.setState({ irrigation_level: e.target.value });
        }
        handlecultivation_periodChange(e){
            this.setState({ cultivation_period: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        handleweather_zoneChange(e){
            this.setState({ weather_zone: e.target.value });
        }
        handlesoil_typeChange(e){
            this.setState({ soil_type: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">Location name</label>
              <input type="text" value={this.state.location_name} onChange={(e) => {this.setState({location_name: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Location code</label>
              <input type="text" value={this.state.location_code} onChange={(e) => {this.setState({location_code: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Location pincode</label>
              <input type="text" value={this.state.location_pincode} onChange={(e) => {this.setState({location_pincode: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Latitude</label>
              <input type="text" value={this.state.latitude} onChange={(e) => {this.setState({latitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Longitude</label>
              <input type="text" value={this.state.longitude} onChange={(e) => {this.setState({longitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Weather zone</label>
                              <select onChange={(e) => this.handleweather_zoneChange(e)} value={this.state.weather_zone} className="form-control" >
                                  <option>Select Weather zone</option>
                                  {this.renderOptionweather_zones()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Soil type</label>
                              <select onChange={(e) => this.handlesoil_typeChange(e)} value={this.state.soil_type} className="form-control" >
                                  <option>Select Soil type</option>
                                  {this.renderOptionsoil_types()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Irrigation level</label>
                              <select onChange={(e) => this.handleirrigation_levelChange(e)} value={this.state.irrigation_level} className="form-control">
                              
                                  <option>Select Irrigation level</option><option value='Good'>Good</option>
                                  <option value='Bad'>Bad</option>
                                  </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Cultivation period</label>
                              <select onChange={(e) => this.handlecultivation_periodChange(e)} value={this.state.cultivation_period} className="form-control">
                              
                                  <option>Select Cultivation period</option><option value='Good'>Good</option>
                                  <option value='Bad'>Bad</option>
                                  </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_weather_zoness: state.weather_zones.view_weather_zones,view_soil_typess: state.soil_types.view_soil_types,save_locationss : state.locations.save_locations, 
        update_locationss : state.locations.update_locations,
        get_locationss : state.locations.get_locations,
    };
  };

  export default connect(mapStateToProps, {
    view_weather_zones,view_soil_types, save_locations, update_locations, get_locations
  })(ModifyLocation);


