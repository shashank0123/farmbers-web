import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { menu } from '../../../../redux/actions/menu';
import { NavLink } from 'react-router-dom'

class AdminSideBar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            login: false
        }

    }


    createMenu() {


        return <></>
    }

    render() {
        if (localStorage.getItem('token'))

            return (
                <>
                    <div className="sl-logo">
                        <a href="">
                            <i className="icon ion-android-star-outline"></i> FarmbersAdmin</a>
                    </div>
                    <div className="sl-sideleft">
                        <div className="input-group input-group-search">
                            <input type="search" name="search" className="form-control" placeholder="Search" />
                            <span className="input-group-btn">
                                <button className="btn"><i className="fa fa-search"></i></button>
                            </span>
                        </div>

                        <label className="sidebar-label">Navigation</label>
                        <div className="sl-sideleft-menu">
                            <a href="/admin/dashboard" className="sl-menu-link active">
                                <div className="sl-menu-item">
                                    <i className="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                                    <span className="menu-item-label">Dashboard</span>
                                </div>
                            </a>

                            {this.createMenu()}

                            <NavLink to='/admin/buyers' className='sl-menu-link' activeClassName='active'>
                                <div className='sl-menu-item'>
                                    <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
                                    <span className='menu-item-label'>Buyer</span>
                                    <i className='menu-item-arrow fa fa-angle-down'></i>
                                </div>
                            </NavLink>
                            <ul className='sl-menu-sub nav flex-column'>
                                <li className='nav-item'><NavLink to={'/admin/buyers' + '/add'} className='nav-link'>Add Buyer</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/buyers' className='nav-link'>View Buyer</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/buyer_crop_demands' + '/add'} className='nav-link'>Add Buyer Crop Demand</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/buyer_crop_demands' className='nav-link'>View Buyer Crop Demand</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/buyer_crop_params' + '/add'} className='nav-link'>Add Buyer Crop Param</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/buyer_crop_params' className='nav-link'>View Buyer Crop Param</NavLink></li>

                            </ul>

                            <NavLink to='/admin/magic_circles' className='sl-menu-link' activeClassName='active'>
                                <div className='sl-menu-item'>
                                    <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
                                    <span className='menu-item-label'>Magic Circle</span>
                                    <i className='menu-item-arrow fa fa-angle-down'></i>
                                </div>
                            </NavLink>
                            <ul className='sl-menu-sub nav flex-column'>
                                <li className='nav-item'><NavLink to={'/admin/magic_circles' + '/add'} className='nav-link'>Add Magic Circle</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/magic_circles' className='nav-link'>View Magic Circle</NavLink>
                                </li>
                            </ul>
                            <NavLink to='/admin/users' className='sl-menu-link' activeClassName='active'>
                                <div className='sl-menu-item'>
                                    <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
                                    <span className='menu-item-label'>User</span>
                                    <i className='menu-item-arrow fa fa-angle-down'></i>
                                </div>
                            </NavLink>
                            <ul className='sl-menu-sub nav flex-column'>
                                <li className='nav-item'><NavLink to={'/admin/users' + '/add'} className='nav-link'>Add User</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/users' className='nav-link'>View User</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/user_crop_stages' + '/add'} className='nav-link'>Add User Crop Stage</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/user_crop_stages' className='nav-link'>View User Crop Stage</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/user_crop_stage_adjs' + '/add'} className='nav-link'>Add User Crop Stage Adj</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/user_crop_stage_adjs' className='nav-link'>View User Crop Stage Adj</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/user_lands' + '/add'} className='nav-link'>Add User Land</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/user_lands' className='nav-link'>View User Land</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/user_magic_circles' + '/add'} className='nav-link'>Add User Magic Circle</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/user_magic_circles' className='nav-link'>View User Magic Circle</NavLink></li>

                            </ul>
                            <NavLink to='/admin/crops' className='sl-menu-link' activeClassName='active'>
                                <div className='sl-menu-item'>
                                    <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
                                    <span className='menu-item-label'>Crops</span>
                                    <i className='menu-item-arrow fa fa-angle-down'></i>
                                </div>
                            </NavLink>
                            <ul className='sl-menu-sub nav flex-column'>
                                <li className='nav-item'><NavLink to={'/admin/crops' + '/add'} className='nav-link'>Add Crop</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/crops' className='nav-link'>View Crop</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/crop_locations' + '/add'} className='nav-link'>Add Crop Location</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/crop_locations' className='nav-link'>View Crop Location</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/crop_priorities' + '/add'} className='nav-link'>Add Crop Priority</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/crop_priorities' className='nav-link'>View Crop Priority</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/crop_stages' + '/add'} className='nav-link'>Add Crop Stage</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/crop_stages' className='nav-link'>View Crop Stage</NavLink></li>
                            </ul>
                            <NavLink to='/admin/lands' className='sl-menu-link' activeClassName='active'>
                                <div className='sl-menu-item'>
                                    <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
                                    <span className='menu-item-label'>Land</span>
                                    <i className='menu-item-arrow fa fa-angle-down'></i>
                                </div>
                            </NavLink>
                            <ul className='sl-menu-sub nav flex-column'>
                                <li className='nav-item'><NavLink to={'/admin/lands' + '/add'} className='nav-link'>Add Land</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/lands' className='nav-link'>View Land</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/land_crops' + '/add'} className='nav-link'>Add Land Crop</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/land_crops' className='nav-link'>View Land Crop</NavLink></li>
                            </ul>
                            <NavLink to='/admin/soil_types' className='sl-menu-link' activeClassName='active'>
                                <div className='sl-menu-item'>
                                    <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
                                    <span className='menu-item-label'>Extras</span>
                                    <i className='menu-item-arrow fa fa-angle-down'></i>
                                </div>
                            </NavLink>
                            <ul className='sl-menu-sub nav flex-column'>
                                <li className='nav-item'><NavLink to={'/admin/soil_types' + '/add'} className='nav-link'>Add Soil Type</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/soil_types' className='nav-link'>View Soil Type</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/weather_zones' + '/add'} className='nav-link'>Add Weather Zone</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/weather_zones' className='nav-link'>View Weather Zone</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/locations' + '/add'} className='nav-link'>Add Location</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/locations' className='nav-link'>View Location</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/weather_delays' + '/add'} className='nav-link'>Add Weather Delay</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/weather_delays' className='nav-link'>View Weather Delay</NavLink></li>
                                <li className='nav-item'><NavLink to={'/admin/weather_impacts' + '/add'} className='nav-link'>Add Weather Impact</NavLink></li>
                                <li className='nav-item'><NavLink to='/admin/weather_impacts' className='nav-link'>View Weather Impact</NavLink></li>
                            </ul>

                        </div>

                        <br />
                    </div>
                </>
            );

        else
            return (<></>)

    }
}

const mapStateToProps = (state) => {
    return {
        menus: state.menu.menu,
    };
};

export default connect(mapStateToProps, {
    menu
})(AdminSideBar);
