import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_buyers } from '../../../../redux/actions/buyers';

class BuyerRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_buyers(id, 'token').then(()=>{console.log(this.props.delete_buyerss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/buyers/edit/'+this.state.row.id
                var url2 = '/admin/buyers/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.buyer_name}</td>
<td>{this.state.row.buyer_type}</td>
<td>{this.state.row.address}</td>
<td>{this.state.row.pincode}</td>
<td>{this.state.row.latitude}</td>
<td>{this.state.row.longitude}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span><Link className='btn btn-primary' style={{fontSize:'smaller'}} to={'buyer_crop_demands?buyer_id='+this.state.row.id}>Buyer Crop Demand</Link><Link className='btn btn-primary' style={{fontSize:'smaller'}} to={'buyer_crop_params?buyer_id='+this.state.row.id}>Buyer Crop Param</Link></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_buyerss : state.buyers.delete_buyers, 
    };
  };

  export default connect(mapStateToProps, {
     delete_buyers
  })(BuyerRow);
