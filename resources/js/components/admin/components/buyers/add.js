import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_buyers } from '../../../../redux/actions/buyers';
import { save_buyers } from '../../../../redux/actions/buyers';
import { get_buyers } from '../../../../redux/actions/buyers';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;

class ModifyBuyer extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        buyer_name : '',buyer_type : '',address : '',pincode : '',latitude : '',longitude : '',status : '',query: ''
    }

    this.handlebuyer_typeChange = this.handlebuyer_typeChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_buyers('token', this.state.id).then(() => {
                this.setState({item : this.props.get_buyerss});
                this.setState({editpage : 1});
                				this.setState({ buyer_name : this.props.get_buyerss.buyer_name }) 
				this.setState({ buyer_type : this.props.get_buyerss.buyer_type }) 
				this.setState({ address : this.props.get_buyerss.address }) 
				this.setState({ pincode : this.props.get_buyerss.pincode }) 
				this.setState({ latitude : this.props.get_buyerss.latitude }) 
				this.setState({ longitude : this.props.get_buyerss.longitude }) 
				this.setState({ status : this.props.get_buyerss.status }) 

                })
            console.log(this.props.get_buyerss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
    }


    

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_buyers(this.state).then(()=> {
            console.log(this.props.save_buyerss)
            history.push('/admin/buyers');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_buyers(this.state).then(()=> {
            console.log(this.props.save_buyerss)
            history.push('/admin/buyers');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlebuyer_typeChange(e){
            this.setState({ buyer_type: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">Buyer name</label>
              <input type="text" value={this.state.buyer_name} onChange={(e) => {this.setState({buyer_name: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Buyer type</label>
                              <select onChange={(e) => this.handlebuyer_typeChange(e)} value={this.state.buyer_type} className="form-control">
                              
                                  <option>Select Buyer type</option><option value='MONDI'>MONDI</option>
                                  <option value='RESTAURANT'>RESTAURANT</option>
                                  <option value='SHOP'>SHOP</option>
                                  </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Address</label>
              <input type="text" value={this.state.address} onChange={(e) => {this.setState({address: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Pincode</label>
              <input type="text" value={this.state.pincode} onChange={(e) => {this.setState({pincode: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Latitude</label>
              <input type="text" value={this.state.latitude} onChange={(e) => {this.setState({latitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Longitude</label>
              <input type="text" value={this.state.longitude} onChange={(e) => {this.setState({longitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {save_buyerss : state.buyers.save_buyers, 
        update_buyerss : state.buyers.update_buyers,
        get_buyerss : state.buyers.get_buyers,
    };
  };

  export default connect(mapStateToProps, {
     save_buyers, update_buyers, get_buyers
  })(ModifyBuyer);


