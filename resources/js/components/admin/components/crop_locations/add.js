import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_crop_locations } from '../../../../redux/actions/crop_locations';
import { save_crop_locations } from '../../../../redux/actions/crop_locations';
import { get_crop_locations } from '../../../../redux/actions/crop_locations';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_crops } from '../../../../redux/actions/crops'; 
import { view_locations } from '../../../../redux/actions/locations'; 

class ModifyCropLocation extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        crop_id : '',location_id : '',from_day : '',to_day : '',status : '',crops : [],locations : [],query: ''
    }

    this.handlecrop_idChange = this.handlecrop_idChange.bind(this)
            this.handlelocation_idChange = this.handlelocation_idChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_crop_locations('token', this.state.id).then(() => {
                this.setState({item : this.props.get_crop_locationss});
                this.setState({editpage : 1});
                				this.setState({ crop_id : this.props.get_crop_locationss.crop_id }) 
				this.setState({ location_id : this.props.get_crop_locationss.location_id }) 
				this.setState({ from_day : this.props.get_crop_locationss.from_day }) 
				this.setState({ to_day : this.props.get_crop_locationss.to_day }) 
				this.setState({ status : this.props.get_crop_locationss.status }) 

                })
            console.log(this.props.get_crop_locationss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_crops(this.state).then(()=> {
                    if (this.props.view_cropss != undefined){
                        this.setState({ crops  : this.props.view_cropss })    
                        this.renderOptioncrops()
                    }

                    
                })
        
            

            this.props.view_locations(this.state).then(()=> {
                    if (this.props.view_locationss != undefined){
                        this.setState({ locations  : this.props.view_locationss })    
                        this.renderOptionlocations()
                    }

                    
                })
        
    }


    renderOptioncrops(){
            console.log(this.props.view_cropss, 'ye aa rha hai')
            var returncrops = <></>;
                    if (typeof this.props.view_cropss != 'undefined')
                    if ( this.props.view_cropss.length > 0)
                    returncrops =  this.props.view_cropss.map(function(element){
                        return <option key={element.crop_id} value={element.crop_id}>{element.crop_name}</option>
                        })       

               
            return <>{returncrops}</>
        }renderOptionlocations(){
            console.log(this.props.view_locationss, 'ye aa rha hai')
            var returnlocations = <></>;
                    if (typeof this.props.view_locationss != 'undefined')
                    if ( this.props.view_locationss.length > 0)
                    returnlocations =  this.props.view_locationss.map(function(element){
                        return <option key={element.location_id} value={element.location_id}>{element.location_name}</option>
                        })       

               
            return <>{returnlocations}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_crop_locations(this.state).then(()=> {
            console.log(this.props.save_crop_locationss)
            history.push('/admin/crop_locations');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_crop_locations(this.state).then(()=> {
            console.log(this.props.save_crop_locationss)
            history.push('/admin/crop_locations');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlecrop_idChange(e){
            this.setState({ crop_id: e.target.value });
        }
        handlelocation_idChange(e){
            this.setState({ location_id: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        handlecrop_idChange(e){
            this.setState({ crop_id: e.target.value });
        }
        handlelocation_idChange(e){
            this.setState({ location_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
                              <label  className="form-label">Crop id</label>
                              <select onChange={(e) => this.handlecrop_idChange(e)} value={this.state.crop_id} className="form-control" >
                                  <option>Select Crop id</option>
                                  {this.renderOptioncrops()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Location id</label>
                              <select onChange={(e) => this.handlelocation_idChange(e)} value={this.state.location_id} className="form-control" >
                                  <option>Select Location id</option>
                                  {this.renderOptionlocations()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">From day</label>
              <input type="text" value={this.state.from_day} onChange={(e) => {this.setState({from_day: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">To day</label>
              <input type="text" value={this.state.to_day} onChange={(e) => {this.setState({to_day: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_cropss: state.crops.view_crops,view_locationss: state.locations.view_locations,save_crop_locationss : state.crop_locations.save_crop_locations, 
        update_crop_locationss : state.crop_locations.update_crop_locations,
        get_crop_locationss : state.crop_locations.get_crop_locations,
    };
  };

  export default connect(mapStateToProps, {
    view_crops,view_locations, save_crop_locations, update_crop_locations, get_crop_locations
  })(ModifyCropLocation);


