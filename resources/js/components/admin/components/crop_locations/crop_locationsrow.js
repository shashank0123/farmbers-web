import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_crop_locations } from '../../../../redux/actions/crop_locations';

class CropLocationRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_crop_locations(id, 'token').then(()=>{console.log(this.props.delete_crop_locationss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/crop_locations/edit/'+this.state.row.id
                var url2 = '/admin/crop_locations/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.crop_id}</td>
<td>{this.state.row.location_id}</td>
<td>{this.state.row.from_day}</td>
<td>{this.state.row.to_day}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span><Link className='btn btn-primary' style={{fontSize:'smaller'}} to={'user_crop_stage_adjs?croplocation_id='+this.state.row.id}>User Crop Stage Adj</Link></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_crop_locationss : state.crop_locations.delete_crop_locations, 
    };
  };

  export default connect(mapStateToProps, {
     delete_crop_locations
  })(CropLocationRow);
