import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import AdminDashboard from './AdminDashboard'
import Login from './../../Login'
import ViewWeatherZone from './weather_zones'
import ModifyWeatherZone from './weather_zones/add'
import ViewWeatherImpact from './weather_impacts'
import ModifyWeatherImpact from './weather_impacts/add'
import ViewWeatherDelay from './weather_delays'
import ModifyWeatherDelay from './weather_delays/add'
import ViewUserMagicCircle from './user_magic_circles'
import ModifyUserMagicCircle from './user_magic_circles/add'
import ViewUserLand from './user_lands'
import ModifyUserLand from './user_lands/add'
import ViewUserCropStageAdj from './user_crop_stage_adjs'
import ModifyUserCropStageAdj from './user_crop_stage_adjs/add'
import ViewUserCropStage from './user_crop_stages'
import ViewUser from './users'
import ModifyUserCropStage from './user_crop_stages/add'
import ViewSoilType from './soil_types'
import ModifySoilType from './soil_types/add'
import ViewMagicCircle from './magic_circles'
import ModifyMagicCircle from './magic_circles/add'
import ViewLocation from './locations'
import ModifyLocation from './locations/add'
import ViewLandCrop from './land_crops'
import ModifyLandCrop from './land_crops/add'
import ViewLand from './lands'
import ModifyLand from './lands/add'
import ViewCropStage from './crop_stages'
import ModifyCropStage from './crop_stages/add'
import ViewCropPriority from './crop_priorities'
import ModifyCropPriority from './crop_priorities/add'
import ViewCropLocation from './crop_locations'
import ModifyCropLocation from './crop_locations/add'
import ViewCrop from './crops'
import ModifyCrop from './crops/add'
import ViewBuyerCropParam from './buyer_crop_params'
import ModifyBuyerCropParam from './buyer_crop_params/add'
import ViewBuyerCropDemand from './buyer_crop_demands'
import ModifyBuyerCropDemand from './buyer_crop_demands/add'
import ViewBuyer from './buyers'
import ModifyBuyer from './buyers/add'
import Profile from './Profile'

class AdminNavigation extends Component {

    constructor(props) {
        super(props)
        this.match = this.props.match
    }


    render() {




        return (
            <Switch>
                <Route exact path={'/'}
                    component={AdminDashboard}
                />
                <Route path={'admin/dashboard'} component={AdminDashboard} />
                <Route path={'/admin/dashboard'} exact component={AdminDashboard} />
                <Route path={'/admin/login'} exact component={Login} />
                <Route path={'/admin/profile'} exact component={Profile} />
                <Route path={'/admin/buyers'} exact component={ViewBuyer} />
                <Route path={'/admin/buyers/add'} exact component={ModifyBuyer} />
                <Route path={'/admin/buyers/edit/:id'} exact component={ModifyBuyer} />

                <Route path={'/admin/buyer_crop_demands'} exact component={ViewBuyerCropDemand} />
                <Route path={'/admin/buyer_crop_demands/add'} exact component={ModifyBuyerCropDemand} />
                <Route path={'/admin/buyer_crop_demands/edit/:id'} exact component={ModifyBuyerCropDemand} />

                <Route path={'/admin/buyer_crop_params'} exact component={ViewBuyerCropParam} />
                <Route path={'/admin/buyer_crop_params/add'} exact component={ModifyBuyerCropParam} />
                <Route path={'/admin/buyer_crop_params/edit/:id'} exact component={ModifyBuyerCropParam} />

                <Route path={'/admin/crops'} exact component={ViewCrop} />
                <Route path={'/admin/crops/add'} exact component={ModifyCrop} />
                <Route path={'/admin/crops/edit/:id'} exact component={ModifyCrop} />

                <Route path={'/admin/crop_locations'} exact component={ViewCropLocation} />
                <Route path={'/admin/crop_locations/add'} exact component={ModifyCropLocation} />
                <Route path={'/admin/crop_locations/edit/:id'} exact component={ModifyCropLocation} />

                <Route path={'/admin/crop_priorities'} exact component={ViewCropPriority} />
                <Route path={'/admin/crop_priorities/add'} exact component={ModifyCropPriority} />
                <Route path={'/admin/crop_priorities/edit/:id'} exact component={ModifyCropPriority} />

                <Route path={'/admin/crop_stages'} exact component={ViewCropStage} />
                <Route path={'/admin/crop_stages/add'} exact component={ModifyCropStage} />
                <Route path={'/admin/crop_stages/edit/:id'} exact component={ModifyCropStage} />

                <Route path={'/admin/lands'} exact component={ViewLand} />
                <Route path={'/admin/lands/add'} exact component={ModifyLand} />
                <Route path={'/admin/lands/edit/:id'} exact component={ModifyLand} />

                <Route path={'/admin/land_crops'} exact component={ViewLandCrop} />
                <Route path={'/admin/land_crops/add'} exact component={ModifyLandCrop} />
                <Route path={'/admin/land_crops/edit/:id'} exact component={ModifyLandCrop} />

                <Route path={'/admin/locations'} exact component={ViewLocation} />
                <Route path={'/admin/locations/add'} exact component={ModifyLocation} />
                <Route path={'/admin/locations/edit/:id'} exact component={ModifyLocation} />

                <Route path={'/admin/magic_circles'} exact component={ViewMagicCircle} />
                <Route path={'/admin/magic_circles/add'} exact component={ModifyMagicCircle} />
                <Route path={'/admin/magic_circles/edit/:id'} exact component={ModifyMagicCircle} />

                <Route path={'/admin/soil_types'} exact component={ViewSoilType} />
                <Route path={'/admin/soil_types/add'} exact component={ModifySoilType} />
                <Route path={'/admin/soil_types/edit/:id'} exact component={ModifySoilType} />

                <Route path={'/admin/user_crop_stages'} exact component={ViewUserCropStage} />
                <Route path={'/admin/user_crop_stages/add'} exact component={ModifyUserCropStage} />
                <Route path={'/admin/user_crop_stages/edit/:id'} exact component={ModifyUserCropStage} />

                <Route path={'/admin/user_crop_stage_adjs'} exact component={ViewUserCropStageAdj} />
                <Route path={'/admin/users'} exact component={ViewUser} />
                <Route path={'/admin/user_crop_stage_adjs/add'} exact component={ModifyUserCropStageAdj} />
                <Route path={'/admin/user_crop_stage_adjs/edit/:id'} exact component={ModifyUserCropStageAdj} />

                <Route path={'/admin/user_lands'} exact component={ViewUserLand} />
                <Route path={'/admin/user_lands/add'} exact component={ModifyUserLand} />
                <Route path={'/admin/user_lands/edit/:id'} exact component={ModifyUserLand} />

                <Route path={'/admin/user_magic_circles'} exact component={ViewUserMagicCircle} />
                <Route path={'/admin/user_magic_circles/add'} exact component={ModifyUserMagicCircle} />
                <Route path={'/admin/user_magic_circles/edit/:id'} exact component={ModifyUserMagicCircle} />

                <Route path={'/admin/weather_delays'} exact component={ViewWeatherDelay} />
                <Route path={'/admin/weather_delays/add'} exact component={ModifyWeatherDelay} />
                <Route path={'/admin/weather_delays/edit/:id'} exact component={ModifyWeatherDelay} />

                <Route path={'/admin/weather_impacts'} exact component={ViewWeatherImpact} />
                <Route path={'/admin/weather_impacts/add'} exact component={ModifyWeatherImpact} />
                <Route path={'/admin/weather_impacts/edit/:id'} exact component={ModifyWeatherImpact} />

                <Route path={'/admin/weather_zones'} exact component={ViewWeatherZone} />
                <Route path={'/admin/weather_zones/add'} exact component={ModifyWeatherZone} />
                <Route path={'/admin/weather_zones/edit/:id'} exact component={ModifyWeatherZone} />




            </Switch>



        );
    }
}

export default AdminNavigation;
