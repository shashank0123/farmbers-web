import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_crop_priorities } from '../../../../redux/actions/crop_priorities';
import { save_crop_priorities } from '../../../../redux/actions/crop_priorities';
import { get_crop_priorities } from '../../../../redux/actions/crop_priorities';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_crops } from '../../../../redux/actions/crops'; 

class ModifyCropPriority extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        crop_id : '',latitude : '',longitude : '',pincode : '',priority : '',status : '',crops : [],query: ''
    }

    this.handlecrop_idChange = this.handlecrop_idChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_crop_priorities('token', this.state.id).then(() => {
                this.setState({item : this.props.get_crop_prioritiess});
                this.setState({editpage : 1});
                				this.setState({ crop_id : this.props.get_crop_prioritiess.crop_id }) 
				this.setState({ latitude : this.props.get_crop_prioritiess.latitude }) 
				this.setState({ longitude : this.props.get_crop_prioritiess.longitude }) 
				this.setState({ pincode : this.props.get_crop_prioritiess.pincode }) 
				this.setState({ priority : this.props.get_crop_prioritiess.priority }) 
				this.setState({ status : this.props.get_crop_prioritiess.status }) 

                })
            console.log(this.props.get_crop_prioritiess, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_crops(this.state).then(()=> {
                    if (this.props.view_cropss != undefined){
                        this.setState({ crops  : this.props.view_cropss })    
                        this.renderOptioncrops()
                    }

                    
                })
        
    }


    renderOptioncrops(){
            console.log(this.props.view_cropss, 'ye aa rha hai')
            var returncrops = <></>;
                    if (typeof this.props.view_cropss != 'undefined')
                    if ( this.props.view_cropss.length > 0)
                    returncrops =  this.props.view_cropss.map(function(element){
                        return <option key={element.crop_id} value={element.crop_id}>{element.crop_name}</option>
                        })       

               
            return <>{returncrops}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_crop_priorities(this.state).then(()=> {
            console.log(this.props.save_crop_prioritiess)
            history.push('/admin/crop_priorities');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_crop_priorities(this.state).then(()=> {
            console.log(this.props.save_crop_prioritiess)
            history.push('/admin/crop_priorities');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlecrop_idChange(e){
            this.setState({ crop_id: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        handlecrop_idChange(e){
            this.setState({ crop_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
                              <label  className="form-label">Crop id</label>
                              <select onChange={(e) => this.handlecrop_idChange(e)} value={this.state.crop_id} className="form-control" >
                                  <option>Select Crop id</option>
                                  {this.renderOptioncrops()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Latitude</label>
              <input type="text" value={this.state.latitude} onChange={(e) => {this.setState({latitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Longitude</label>
              <input type="text" value={this.state.longitude} onChange={(e) => {this.setState({longitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Pincode</label>
              <input type="text" value={this.state.pincode} onChange={(e) => {this.setState({pincode: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Priority</label>
              <input type="text" value={this.state.priority} onChange={(e) => {this.setState({priority: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_cropss: state.crops.view_crops,save_crop_prioritiess : state.crop_priorities.save_crop_priorities, 
        update_crop_prioritiess : state.crop_priorities.update_crop_priorities,
        get_crop_prioritiess : state.crop_priorities.get_crop_priorities,
    };
  };

  export default connect(mapStateToProps, {
    view_crops, save_crop_priorities, update_crop_priorities, get_crop_priorities
  })(ModifyCropPriority);


