import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { view_crop_priorities } from '../../../../redux/actions/crop_priorities';
import { Link } from 'react-router-dom';
const createHistory = require('history').createBrowserHistory;
import CropPriorityRow from './crop_prioritiesrow';

class ViewCropPriority extends Component {

    state = {
        query : '',
        start_date : null,
        end_date : null
    }
    constructor(props){
        super(props)
        this.checkLogin()
        this.getData()
    }

    checkLogin(){
        var token = localStorage.getItem('token')
        if (!token || token == undefined || token == 'undefined'){
            let history = createHistory();
            console.log('yaha aaya to hai')
            history.push('/admin/login');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;

        }
        console.log(token)
    }

    getData(){
        if (this.state.query == undefined){
            this.state.query = '';
        }
        this.props.view_crop_priorities(this.state).then(()=>{

            })
    }

    searchText(e){
        this.setState({query : e.target.value})
        this.getData();
        this.renderTableData()
    }

    searchDataNow(){
        this.getData();
        this.renderTableData()
    }

    clearSelection(){
        this.setState({start_date : null, end_date : null, query : ''})
        this.getData();
    }

    renderTableData = () => {
        var data = this.props.view_crop_prioritiess;

        if (data != undefined){
            var tabledata = data.map((element) => {
                return <CropPriorityRow datavalue={element}/>
                
            })
            return  <>{ tabledata }

            </>

        }
        else{
            return <tr><td>No Data Found</td></tr>
        }
    }



    renderpageSystem() {

        var count = this.props.count_crop_prioritiess;
        var rows = '';
        if (count == 0 || count == 'undefined'){
            return <></>
        }
        else
        {
            count = Math.round(count)
            var i = 1;
            for (i = 1; i <= count; i++) {
                rows += <li class='page-item'><a class='page-link' href='?page={i}'>{i}</a></li>
            }
        }

        return (<><div>
            <nav>
                <ul class='pagination'>
                    {rows}
                </ul>
            </nav>

        </div></>)
    }

    render(){
        return (
        <>
        <div className='row'>
                        <div className=' col-md-2 mb-3'>
                            <input type='date' className='form-control' value={this.state.start_date} onChange={(e) => {this.setState({start_date : e.target.value})} }/>
                          </div>
                          <div className='mb-3 col-md-2 '>
                            <input type='date' className='form-control' value={this.state.end_date} onChange={(e) => {this.setState({end_date : e.target.value})} }/>
                          </div>
                          <div className='mb-3 col-md-3'>
                            <input type='text' placeholder={'Search'} className='form-control' value={this.state.query} onChange={(e) => {this.searchText(e)} }/>
                            </div>
                            <div className='mb-3 col-md-5'>
                            <a className='btn btn-primary' onClick={() => {this.searchDataNow()}}>Search</a>
                          
                        
                          
                            <a className='btn btn-primary' onClick={() => {this.clearSelection()}}>Reset</a>
                          
                    
                        
                            <a className='btn btn-primary' href='/admin/crop_priorities/add'>Add New</a>
                          </div>
                        </div>
                   
        <div className='container'>
            <table className='table bordered'>
            <thead>
                <tr>
                <td>Crop Id</td>
<td>Latitude</td>
<td>Longitude</td>
<td>Pincode</td>
<td>Priority</td>
<td>Status</td>
<td>Actions</td></tr>
                </thead>
                <tbody>
                
                {this.renderTableData()}
                </tbody>
                        
            </table>
                
            
        </div>
        </>
            )


    }
}

const mapStateToProps = (state) => {
    return {
      view_crop_prioritiess: state.crop_priorities.view_crop_priorities,
      count_crop_prioritiess: state.crop_priorities.count_crop_priorities,
    };
  };

  export default connect(mapStateToProps, {
    view_crop_priorities
  })(ViewCropPriority);
