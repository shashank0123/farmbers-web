import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_crop_priorities } from '../../../../redux/actions/crop_priorities';

class CropPriorityRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_crop_priorities(id, 'token').then(()=>{console.log(this.props.delete_crop_prioritiess)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/crop_priorities/edit/'+this.state.row.id
                var url2 = '/admin/crop_priorities/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.crop_id}</td>
<td>{this.state.row.latitude}</td>
<td>{this.state.row.longitude}</td>
<td>{this.state.row.pincode}</td>
<td>{this.state.row.priority}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_crop_prioritiess : state.crop_priorities.delete_crop_priorities, 
    };
  };

  export default connect(mapStateToProps, {
     delete_crop_priorities
  })(CropPriorityRow);
