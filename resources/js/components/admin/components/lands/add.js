import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_lands } from '../../../../redux/actions/lands';
import { save_lands } from '../../../../redux/actions/lands';
import { get_lands } from '../../../../redux/actions/lands';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;

class ModifyLand extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        land_name : '',latitude : '',longitude : '',address : '',status : '',query: ''
    }

    this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_lands('token', this.state.id).then(() => {
                this.setState({item : this.props.get_landss});
                this.setState({editpage : 1});
                				this.setState({ land_name : this.props.get_landss.land_name }) 
				this.setState({ latitude : this.props.get_landss.latitude }) 
				this.setState({ longitude : this.props.get_landss.longitude }) 
				this.setState({ address : this.props.get_landss.address }) 
				this.setState({ status : this.props.get_landss.status }) 

                })
            console.log(this.props.get_landss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
    }


    

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_lands(this.state).then(()=> {
            console.log(this.props.save_landss)
            history.push('/admin/lands');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_lands(this.state).then(()=> {
            console.log(this.props.save_landss)
            history.push('/admin/lands');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">Land name</label>
              <input type="text" value={this.state.land_name} onChange={(e) => {this.setState({land_name: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Latitude</label>
              <input type="text" value={this.state.latitude} onChange={(e) => {this.setState({latitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Longitude</label>
              <input type="text" value={this.state.longitude} onChange={(e) => {this.setState({longitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Address</label>
              <input type="text" value={this.state.address} onChange={(e) => {this.setState({address: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {save_landss : state.lands.save_lands, 
        update_landss : state.lands.update_lands,
        get_landss : state.lands.get_lands,
    };
  };

  export default connect(mapStateToProps, {
     save_lands, update_lands, get_lands
  })(ModifyLand);


