import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_lands } from '../../../../redux/actions/lands';

class LandRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_lands(id, 'token').then(()=>{console.log(this.props.delete_landss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/lands/edit/'+this.state.row.id
                var url2 = '/admin/lands/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.land_name}</td>
<td>{this.state.row.latitude}</td>
<td>{this.state.row.longitude}</td>
<td>{this.state.row.address}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span><Link className='btn btn-primary' style={{fontSize:'smaller'}} to={'land_crops?land_id='+this.state.row.id}>Land Crop</Link></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_landss : state.lands.delete_lands, 
    };
  };

  export default connect(mapStateToProps, {
     delete_lands
  })(LandRow);
