import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_users } from '../../../../redux/actions/users';

class UserRow extends Component {


    constructor(props) {
        super(props)
        this.state = { row: this.props.datavalue, query: '' }
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_users(id, 'token').then(() => { console.log(this.props.delete_userss) })
        window.location.reload();
    }


    render() {

        var url1 = '/admin/users/edit/' + this.state.row.id
        var url2 = '/admin/users/delete/' + this.state.row.id
        return (<tr>
            <td>{this.state.row.name}</td>
            <td>{this.state.row.mobile_no}</td>
            <td>{this.state.row.email}</td>
            <td>{this.state.row.google_id}</td>
            <td>{this.state.row.facebook_id}</td>
            <td>{this.state.row.password}</td>
            <td>{this.state.row.mobile_verified_date}</td>
            <td>{this.state.row.email_verified_date}</td>
            <td>{this.state.row.remember_token}</td>
            <td>{this.state.row.status}</td>
            <td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={() => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



const mapStateToProps = (state) => {
    return {
        delete_userss: state.users.delete_users,
    };
};

export default connect(mapStateToProps, {
    delete_users
})(UserRow);
