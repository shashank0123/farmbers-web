import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_buyer_crop_params } from '../../../../redux/actions/buyer_crop_params';

class BuyerCropParamRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_buyer_crop_params(id, 'token').then(()=>{console.log(this.props.delete_buyer_crop_paramss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/buyer_crop_params/edit/'+this.state.row.id
                var url2 = '/admin/buyer_crop_params/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.buyer_id}</td>
<td>{this.state.row.crop_name}</td>
<td>{this.state.row.log_date}</td>
<td>{this.state.row.param_name}</td>
<td>{this.state.row.param_value}</td>
<td>{this.state.row.param_unit}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_buyer_crop_paramss : state.buyer_crop_params.delete_buyer_crop_params, 
    };
  };

  export default connect(mapStateToProps, {
     delete_buyer_crop_params
  })(BuyerCropParamRow);
