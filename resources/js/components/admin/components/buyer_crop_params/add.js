import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_buyer_crop_params } from '../../../../redux/actions/buyer_crop_params';
import { save_buyer_crop_params } from '../../../../redux/actions/buyer_crop_params';
import { get_buyer_crop_params } from '../../../../redux/actions/buyer_crop_params';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_buyers } from '../../../../redux/actions/buyers'; 

class ModifyBuyerCropParam extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        buyer_id : '',crop_name : '',log_date : '',param_name : '',param_value : '',param_unit : '',buyers : [],query: ''
    }

    this.handlebuyer_idChange = this.handlebuyer_idChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_buyer_crop_params('token', this.state.id).then(() => {
                this.setState({item : this.props.get_buyer_crop_paramss});
                this.setState({editpage : 1});
                				this.setState({ buyer_id : this.props.get_buyer_crop_paramss.buyer_id }) 
				this.setState({ crop_name : this.props.get_buyer_crop_paramss.crop_name }) 
				this.setState({ log_date : this.props.get_buyer_crop_paramss.log_date }) 
				this.setState({ param_name : this.props.get_buyer_crop_paramss.param_name }) 
				this.setState({ param_value : this.props.get_buyer_crop_paramss.param_value }) 
				this.setState({ param_unit : this.props.get_buyer_crop_paramss.param_unit }) 

                })
            console.log(this.props.get_buyer_crop_paramss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_buyers(this.state).then(()=> {
                    if (this.props.view_buyerss != undefined){
                        this.setState({ buyers  : this.props.view_buyerss })    
                        this.renderOptionbuyers()
                    }

                    
                })
        
    }


    renderOptionbuyers(){
            console.log(this.props.view_buyerss, 'ye aa rha hai')
            var returnbuyers = <></>;
                    if (typeof this.props.view_buyerss != 'undefined')
                    if ( this.props.view_buyerss.length > 0)
                    returnbuyers =  this.props.view_buyerss.map(function(element){
                        return <option key={element.buyer_id} value={element.buyer_id}>{element.buyer_name}</option>
                        })       

               
            return <>{returnbuyers}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_buyer_crop_params(this.state).then(()=> {
            console.log(this.props.save_buyer_crop_paramss)
            history.push('/admin/buyer_crop_params');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_buyer_crop_params(this.state).then(()=> {
            console.log(this.props.save_buyer_crop_paramss)
            history.push('/admin/buyer_crop_params');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlebuyer_idChange(e){
            this.setState({ buyer_id: e.target.value });
        }
        handlebuyer_idChange(e){
            this.setState({ buyer_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.param_unit)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
                              <label  className="form-label">Buyer id</label>
                              <select onChange={(e) => this.handlebuyer_idChange(e)} value={this.state.buyer_id} className="form-control" >
                                  <option>Select Buyer id</option>
                                  {this.renderOptionbuyers()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Crop name</label>
              <input type="text" value={this.state.crop_name} onChange={(e) => {this.setState({crop_name: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Param name</label>
              <input type="text" value={this.state.param_name} onChange={(e) => {this.setState({param_name: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Param value</label>
              <input type="text" value={this.state.param_value} onChange={(e) => {this.setState({param_value: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Param unit</label>
              <input type="text" value={this.state.param_unit} onChange={(e) => {this.setState({param_unit: e.target.value})}} className="form-control"/>
            </div>
            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_buyerss: state.buyers.view_buyers,save_buyer_crop_paramss : state.buyer_crop_params.save_buyer_crop_params, 
        update_buyer_crop_paramss : state.buyer_crop_params.update_buyer_crop_params,
        get_buyer_crop_paramss : state.buyer_crop_params.get_buyer_crop_params,
    };
  };

  export default connect(mapStateToProps, {
    view_buyers, save_buyer_crop_params, update_buyer_crop_params, get_buyer_crop_params
  })(ModifyBuyerCropParam);


