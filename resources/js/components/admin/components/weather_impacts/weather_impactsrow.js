import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_weather_impacts } from '../../../../redux/actions/weather_impacts';

class WeatherImpactRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_weather_impacts(id, 'token').then(()=>{console.log(this.props.delete_weather_impactss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/weather_impacts/edit/'+this.state.row.id
                var url2 = '/admin/weather_impacts/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.delay_id}</td>
<td>{this.state.row.log_date}</td>
<td>{this.state.row.delay_reason}</td>
<td>{this.state.row.delay}</td>
<td>{this.state.row.message}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_weather_impactss : state.weather_impacts.delete_weather_impacts, 
    };
  };

  export default connect(mapStateToProps, {
     delete_weather_impacts
  })(WeatherImpactRow);
