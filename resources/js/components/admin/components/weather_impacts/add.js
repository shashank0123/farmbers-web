import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_weather_impacts } from '../../../../redux/actions/weather_impacts';
import { save_weather_impacts } from '../../../../redux/actions/weather_impacts';
import { get_weather_impacts } from '../../../../redux/actions/weather_impacts';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_weather_delays } from '../../../../redux/actions/weather_delays'; 

class ModifyWeatherImpact extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        delay_id : '',log_date : '',delay_reason : '',delay : '',message : '',weather_delays : [],query: ''
    }

    this.handledelay_idChange = this.handledelay_idChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_weather_impacts('token', this.state.id).then(() => {
                this.setState({item : this.props.get_weather_impactss});
                this.setState({editpage : 1});
                				this.setState({ delay_id : this.props.get_weather_impactss.delay_id }) 
				this.setState({ log_date : this.props.get_weather_impactss.log_date }) 
				this.setState({ delay_reason : this.props.get_weather_impactss.delay_reason }) 
				this.setState({ delay : this.props.get_weather_impactss.delay }) 
				this.setState({ message : this.props.get_weather_impactss.message }) 

                })
            console.log(this.props.get_weather_impactss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_weather_delays(this.state).then(()=> {
                    if (this.props.view_weather_delayss != undefined){
                        this.setState({ weather_delays  : this.props.view_weather_delayss })    
                        this.renderOptionweather_delays()
                    }

                    
                })
        
    }


    renderOptionweather_delays(){
            console.log(this.props.view_weather_delayss, 'ye aa rha hai')
            var returnweather_delays = <></>;
                    if (typeof this.props.view_weather_delayss != 'undefined')
                    if ( this.props.view_weather_delayss.length > 0)
                    returnweather_delays =  this.props.view_weather_delayss.map(function(element){
                        return <option key={element.delay_id} value={element.delay_id}>{element.delay_reason}</option>
                        })       

               
            return <>{returnweather_delays}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_weather_impacts(this.state).then(()=> {
            console.log(this.props.save_weather_impactss)
            history.push('/admin/weather_impacts');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_weather_impacts(this.state).then(()=> {
            console.log(this.props.save_weather_impactss)
            history.push('/admin/weather_impacts');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handledelay_idChange(e){
            this.setState({ delay_id: e.target.value });
        }
        handledelay_idChange(e){
            this.setState({ delay_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.message)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
                              <label  className="form-label">Delay id</label>
                              <select onChange={(e) => this.handledelay_idChange(e)} value={this.state.delay_id} className="form-control" >
                                  <option>Select Delay id</option>
                                  {this.renderOptionweather_delays()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Delay reason</label>
              <input type="text" value={this.state.delay_reason} onChange={(e) => {this.setState({delay_reason: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Delay</label>
              <input type="text" value={this.state.delay} onChange={(e) => {this.setState({delay: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Message</label>
              <input type="text" value={this.state.message} onChange={(e) => {this.setState({message: e.target.value})}} className="form-control"/>
            </div>
            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_weather_delayss: state.weather_delays.view_weather_delays,save_weather_impactss : state.weather_impacts.save_weather_impacts, 
        update_weather_impactss : state.weather_impacts.update_weather_impacts,
        get_weather_impactss : state.weather_impacts.get_weather_impacts,
    };
  };

  export default connect(mapStateToProps, {
    view_weather_delays, save_weather_impacts, update_weather_impacts, get_weather_impacts
  })(ModifyWeatherImpact);


