import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_crop_stages } from '../../../../redux/actions/crop_stages';

class CropStageRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_crop_stages(id, 'token').then(()=>{console.log(this.props.delete_crop_stagess)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/crop_stages/edit/'+this.state.row.id
                var url2 = '/admin/crop_stages/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.crop_id}</td>
<td>{this.state.row.stage_name}</td>
<td>{this.state.row.stage_flexibility}</td>
<td>{this.state.row.stage_duration}</td>
<td>{this.state.row.position}</td>
<td>{this.state.row.status}</td>
<td>{this.state.row.stage_desc}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_crop_stagess : state.crop_stages.delete_crop_stages, 
    };
  };

  export default connect(mapStateToProps, {
     delete_crop_stages
  })(CropStageRow);
