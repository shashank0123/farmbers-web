import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_crop_stages } from '../../../../redux/actions/crop_stages';
import { save_crop_stages } from '../../../../redux/actions/crop_stages';
import { get_crop_stages } from '../../../../redux/actions/crop_stages';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_crops } from '../../../../redux/actions/crops';

class ModifyCropStage extends Component {



    constructor(props) {
        super(props)
        this.state = {
            item: {},
            id: '',
            editpage: 0,
            crop_id: '', stage_name: '', stage_flexibility: '', stage_duration: '', position: '', status: '', stage_desc: EditorState.createEmpty(), crops: [], query: ''
        }

        this.handlecrop_idChange = this.handlecrop_idChange.bind(this)
        this.handlestatusChange = this.handlestatusChange.bind(this)

        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem() {
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')) {
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_crop_stages('token', this.state.id).then(() => {
                this.setState({ item: this.props.get_crop_stagess });
                this.setState({ editpage: 1 });
                this.setState({ crop_id: this.props.get_crop_stagess.crop_id })
                this.setState({ stage_name: this.props.get_crop_stagess.stage_name })
                this.setState({ stage_flexibility: this.props.get_crop_stagess.stage_flexibility })
                this.setState({ stage_duration: this.props.get_crop_stagess.stage_duration })
                this.setState({ position: this.props.get_crop_stagess.position })
                this.setState({ status: this.props.get_crop_stagess.status })
                this.setState({ stage_desc: this.props.get_crop_stagess.stage_desc })

            })
            console.log(this.props.get_crop_stagess, 'state');
        }
    }

    async getToken() {
        this.setState({ token: localStorage.getItem('token') })
    }

    callDependencies() {



        this.props.view_crops(this.state).then(() => {
            if (this.props.view_cropss != undefined) {
                this.setState({ crops: this.props.view_cropss })
                this.renderOptioncrops()
            }


        })

    }


    renderOptioncrops() {
        console.log(this.props.view_cropss, 'ye aa rha hai')
        var returncrops = <></>;
        if (typeof this.props.view_cropss != 'undefined')
            if (this.props.view_cropss.length > 0)
                returncrops = this.props.view_cropss.map(function (element) {
                    return <option key={element.id} value={element.id}>{element.crop_name}</option>
                })


        return <>{returncrops}</>
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log('yaha aaya tha')
        let history = createHistory();
        // this is in case of save
        if (this.state.editpage) {
            this.props.update_crop_stages(this.state).then(() => {
                console.log(this.props.save_crop_stagess)
                history.push('/admin/crop_stages');
                let pathUrl = window.location.href;
                window.location.href = pathUrl;
            })
        }
        else
            this.props.save_crop_stages(this.state).then(() => {
                console.log(this.props.save_crop_stagess)
                history.push('/admin/crop_stages');
                let pathUrl = window.location.href;
                window.location.href = pathUrl;
            })
    }

    handlecrop_idChange(e) {
        this.setState({ crop_id: e.target.value });
    }
    handlestatusChange(e) {
        this.setState({ status: e.target.value });
    }
    handlecrop_idChange(e) {
        this.setState({ crop_id: e.target.value });
    }


    render() {
        // console.log(this.state.stage_desc)
        return (
            <>
                <form onSubmit={this.handleSubmit}>

                    <div className="mb-3">
                        <label className="form-label">Crop id</label>
                        <select onChange={(e) => this.handlecrop_idChange(e)} value={this.state.crop_id} className="form-control" >
                            <option>Select Crop id</option>
                            {this.renderOptioncrops()}

                        </select>
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Stage name</label>
                        <input type="text" value={this.state.stage_name} onChange={(e) => { this.setState({ stage_name: e.target.value }) }} className="form-control" />
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Stage flexibility</label>
                        <input type="text" value={this.state.stage_flexibility} onChange={(e) => { this.setState({ stage_flexibility: e.target.value }) }} className="form-control" />
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Stage duration</label>
                        <input type="text" value={this.state.stage_duration} onChange={(e) => { this.setState({ stage_duration: e.target.value }) }} className="form-control" />
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Position</label>
                        <input type="text" value={this.state.position} onChange={(e) => { this.setState({ position: e.target.value }) }} className="form-control" />
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Status</label>
                        <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">

                            <option>Select Status</option><option value='Active'>Active</option>
                            <option value='Deactive'>Deactive</option>
                        </select>
                    </div>
                    <div className="mb-3">
                        <label >Stage desc</label>
                        <Editor
                            editorState={this.state.stage_desc}
                            toolbarClassName="toolbarClassName"
                            wrapperClassName="wrapperClassName"
                            editorClassName="editorClassName"
                            onEditorStateChange={(editorState) => this.setState({ stage_desc: editorState })}
                        />
                    </div>

                    <button type='submit' className='btn btn-primary'>Submit</button>
                </form>
            </>
        )


    }
}
const mapStateToProps = (state) => {
    return {
        view_cropss: state.crops.view_crops, save_crop_stagess: state.crop_stages.save_crop_stages,
        update_crop_stagess: state.crop_stages.update_crop_stages,
        get_crop_stagess: state.crop_stages.get_crop_stages,
    };
};

export default connect(mapStateToProps, {
    view_crops, save_crop_stages, update_crop_stages, get_crop_stages
})(ModifyCropStage);


