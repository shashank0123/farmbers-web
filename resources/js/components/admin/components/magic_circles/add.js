import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_magic_circles } from '../../../../redux/actions/magic_circles';
import { save_magic_circles } from '../../../../redux/actions/magic_circles';
import { get_magic_circles } from '../../../../redux/actions/magic_circles';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;

class ModifyMagicCircle extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        mc_name : '',pincode : '',radius : '',status : '',latitude : '',longitude : '',query: ''
    }

    this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_magic_circles('token', this.state.id).then(() => {
                this.setState({item : this.props.get_magic_circless});
                this.setState({editpage : 1});
                				this.setState({ mc_name : this.props.get_magic_circless.mc_name }) 
				this.setState({ pincode : this.props.get_magic_circless.pincode }) 
				this.setState({ radius : this.props.get_magic_circless.radius }) 
				this.setState({ status : this.props.get_magic_circless.status }) 
				this.setState({ latitude : this.props.get_magic_circless.latitude }) 
				this.setState({ longitude : this.props.get_magic_circless.longitude }) 

                })
            console.log(this.props.get_magic_circless, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
    }


    

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_magic_circles(this.state).then(()=> {
            console.log(this.props.save_magic_circless)
            history.push('/admin/magic_circles');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_magic_circles(this.state).then(()=> {
            console.log(this.props.save_magic_circless)
            history.push('/admin/magic_circles');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        

    render(){
        // console.log(this.state.longitude)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">Mc name</label>
              <input type="text" value={this.state.mc_name} onChange={(e) => {this.setState({mc_name: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Pincode</label>
              <input type="text" value={this.state.pincode} onChange={(e) => {this.setState({pincode: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Radius</label>
              <input type="text" value={this.state.radius} onChange={(e) => {this.setState({radius: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Latitude</label>
              <input type="text" value={this.state.latitude} onChange={(e) => {this.setState({latitude: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Longitude</label>
              <input type="text" value={this.state.longitude} onChange={(e) => {this.setState({longitude: e.target.value})}} className="form-control"/>
            </div>
            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {save_magic_circless : state.magic_circles.save_magic_circles, 
        update_magic_circless : state.magic_circles.update_magic_circles,
        get_magic_circless : state.magic_circles.get_magic_circles,
    };
  };

  export default connect(mapStateToProps, {
     save_magic_circles, update_magic_circles, get_magic_circles
  })(ModifyMagicCircle);


