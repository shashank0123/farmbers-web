import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { checkToken } from '../../../../redux/actions/checkToken';
import { connect } from 'react-redux';
const createHistory = require("history").createBrowserHistory;

class AdminHeader extends Component {

    constructor(props){
        super(props)
        let history = createHistory();
        var currenturl = window.location.href
        this.state = {
            login : false
        }
        if (!currenturl.includes('/admin/login'))
        this.props.checkToken().then((result) => {
           if (this.props.checkTokens == '' || this.props.checkTokens == 'undefined'){
               console.log(this.props.checkTokens)
            //    go to dashboard
            history.push("/admin/login");
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
           }
           else{
               this.setState({login: true})
            //    stay on this page

           }
        })
        .catch((err) => {
            console.log(err)
        })
    }

    logout(){
        localStorage.removeItem('token')
        localStorage.removeItem('email')
        localStorage.removeItem('userType')
        localStorage.removeItem('name')
        let history = createHistory();
        history.push("/admin/login");
        var npathUrl = window.location.href;
        window.location.href = npathUrl;
    }

    renderHeader(){
        return (<>
            <div className="sl-header">
              <div className="sl-header-left">
                <div className="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i className="icon ion-navicon-round"></i></a></div>
                <div className="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i className="icon ion-navicon-round"></i></a></div>
              </div>
              <div className="sl-header-right">
                <nav className="nav">
                  <div className="dropdown">
                    <a href="" className="nav-link nav-link-profile" data-toggle="dropdown">
                      <span className="logged-name">{localStorage.getItem('name')}</span>
                      <img src="../img/img3.jpg" className="wd-32 rounded-circle" alt=""/>
                    </a>
                    <div className="dropdown-menu dropdown-menu-header wd-200">
                      <ul className="list-unstyled user-profile-nav">
                        <li><a href=""><i className="icon ion-ios-person-outline"></i> Edit Profile</a></li>

                        <li><span onClick={() => {
                            this.logout()
                        }}><i className="icon ion-power"></i> Sign Out</span></li>
                      </ul>
                    </div>
                  </div>
                </nav>
                <div className="navicon-right">
                  <a id="btnRightMenu" href="" className="pos-relative">
                    <i className="icon ion-ios-bell-outline"></i>

                    <span className="square-8 bg-danger"></span>

                  </a>
                </div>
              </div>
            </div>

                        </>)
    }

    render(){

            return (
                <>{ this.state.login ?
                this.renderHeader() : <></>}</>
            );

    }
}
const mapStateToProps = (state) => {
    return {
      checkTokens: state.checkToken.checkToken,
    };
  };

  export default connect(mapStateToProps, {
    checkToken
  })(AdminHeader);
