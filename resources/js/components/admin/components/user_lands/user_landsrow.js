import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_user_lands } from '../../../../redux/actions/user_lands';

class UserLandRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_user_lands(id, 'token').then(()=>{console.log(this.props.delete_user_landss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/user_lands/edit/'+this.state.row.id
                var url2 = '/admin/user_lands/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.user_id}</td>
<td>{this.state.row.land_id}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_user_landss : state.user_lands.delete_user_lands, 
    };
  };

  export default connect(mapStateToProps, {
     delete_user_lands
  })(UserLandRow);
