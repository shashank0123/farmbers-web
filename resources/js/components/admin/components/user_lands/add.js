import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_user_lands } from '../../../../redux/actions/user_lands';
import { save_user_lands } from '../../../../redux/actions/user_lands';
import { get_user_lands } from '../../../../redux/actions/user_lands';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_users } from '../../../../redux/actions/users'; 
import { view_lands } from '../../../../redux/actions/lands'; 

class ModifyUserLand extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        user_id : '',land_id : '',status : '',users : [],lands : [],query: ''
    }

    this.handleuser_idChange = this.handleuser_idChange.bind(this)
            this.handleland_idChange = this.handleland_idChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_user_lands('token', this.state.id).then(() => {
                this.setState({item : this.props.get_user_landss});
                this.setState({editpage : 1});
                				this.setState({ user_id : this.props.get_user_landss.user_id }) 
				this.setState({ land_id : this.props.get_user_landss.land_id }) 
				this.setState({ status : this.props.get_user_landss.status }) 

                })
            console.log(this.props.get_user_landss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_users(this.state).then(()=> {
                    if (this.props.view_userss != undefined){
                        this.setState({ users  : this.props.view_userss })    
                        this.renderOptionusers()
                    }

                    
                })
        
            

            this.props.view_lands(this.state).then(()=> {
                    if (this.props.view_landss != undefined){
                        this.setState({ lands  : this.props.view_landss })    
                        this.renderOptionlands()
                    }

                    
                })
        
    }


    renderOptionusers(){
            console.log(this.props.view_userss, 'ye aa rha hai')
            var returnusers = <></>;
                    if (typeof this.props.view_userss != 'undefined')
                    if ( this.props.view_userss.length > 0)
                    returnusers =  this.props.view_userss.map(function(element){
                        return <option key={element.user_id} value={element.user_id}>{element.name}</option>
                        })       

               
            return <>{returnusers}</>
        }renderOptionlands(){
            console.log(this.props.view_landss, 'ye aa rha hai')
            var returnlands = <></>;
                    if (typeof this.props.view_landss != 'undefined')
                    if ( this.props.view_landss.length > 0)
                    returnlands =  this.props.view_landss.map(function(element){
                        return <option key={element.land_id} value={element.land_id}>{element.land_name}</option>
                        })       

               
            return <>{returnlands}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_user_lands(this.state).then(()=> {
            console.log(this.props.save_user_landss)
            history.push('/admin/user_lands');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_user_lands(this.state).then(()=> {
            console.log(this.props.save_user_landss)
            history.push('/admin/user_lands');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handleuser_idChange(e){
            this.setState({ user_id: e.target.value });
        }
        handleland_idChange(e){
            this.setState({ land_id: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        handleuser_idChange(e){
            this.setState({ user_id: e.target.value });
        }
        handleland_idChange(e){
            this.setState({ land_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
                              <label  className="form-label">User id</label>
                              <select onChange={(e) => this.handleuser_idChange(e)} value={this.state.user_id} className="form-control" >
                                  <option>Select User id</option>
                                  {this.renderOptionusers()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Land id</label>
                              <select onChange={(e) => this.handleland_idChange(e)} value={this.state.land_id} className="form-control" >
                                  <option>Select Land id</option>
                                  {this.renderOptionlands()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_userss: state.users.view_users,view_landss: state.lands.view_lands,save_user_landss : state.user_lands.save_user_lands, 
        update_user_landss : state.user_lands.update_user_lands,
        get_user_landss : state.user_lands.get_user_lands,
    };
  };

  export default connect(mapStateToProps, {
    view_users,view_lands, save_user_lands, update_user_lands, get_user_lands
  })(ModifyUserLand);


