import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_user_magic_circles } from '../../../../redux/actions/user_magic_circles';
import { save_user_magic_circles } from '../../../../redux/actions/user_magic_circles';
import { get_user_magic_circles } from '../../../../redux/actions/user_magic_circles';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_users } from '../../../../redux/actions/users'; 
import { view_magic_circles } from '../../../../redux/actions/magic_circles'; 

class ModifyUserMagicCircle extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        user_id : '', mc_id : '',status : '',users : [],magic_circles : [],query: ''
    }

    this.handleuser_idChange = this.handleuser_idChange.bind(this)
            this.handlemc_idChange = this.handlemc_idChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_user_magic_circles('token', this.state.id).then(() => {
                this.setState({item : this.props.get_user_magic_circless});
                this.setState({editpage : 1});
                				this.setState({ user_id : this.props.get_user_magic_circless.user_id }) 
				this.setState({  mc_id : this.props.get_user_magic_circless. mc_id }) 
				this.setState({ status : this.props.get_user_magic_circless.status }) 

                })
            console.log(this.props.get_user_magic_circless, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_users(this.state).then(()=> {
                    if (this.props.view_userss != undefined){
                        this.setState({ users  : this.props.view_userss })    
                        this.renderOptionusers()
                    }

                    
                })
        
            

            this.props.view_magic_circles(this.state).then(()=> {
                    if (this.props.view_magic_circless != undefined){
                        this.setState({ magic_circles  : this.props.view_magic_circless })    
                        this.renderOptionmagic_circles()
                    }

                    
                })
        
    }


    renderOptionusers(){
            console.log(this.props.view_userss, 'ye aa rha hai')
            var returnusers = <></>;
                    if (typeof this.props.view_userss != 'undefined')
                    if ( this.props.view_userss.length > 0)
                    returnusers =  this.props.view_userss.map(function(element){
                        return <option key={element.user_id} value={element.user_id}>{element.user_name}</option>
                        })       

               
            return <>{returnusers}</>
        }renderOptionmagic_circles(){
            console.log(this.props.view_magic_circless, 'ye aa rha hai')
            var returnmagic_circles = <></>;
                    if (typeof this.props.view_magic_circless != 'undefined')
                    if ( this.props.view_magic_circless.length > 0)
                    returnmagic_circles =  this.props.view_magic_circless.map(function(element){
                        return <option key={element. mc_id} value={element. mc_id}>{element.mc_name}</option>
                        })       

               
            return <>{returnmagic_circles}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_user_magic_circles(this.state).then(()=> {
            console.log(this.props.save_user_magic_circless)
            history.push('/admin/user_magic_circles');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_user_magic_circles(this.state).then(()=> {
            console.log(this.props.save_user_magic_circless)
            history.push('/admin/user_magic_circles');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handleuser_idChange(e){
            this.setState({ user_id: e.target.value });
        }
        handlemc_idChange(e){
            this.setState({  mc_id: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        handleuser_idChange(e){
            this.setState({ user_id: e.target.value });
        }
        handlemc_idChange(e){
            this.setState({  mc_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
                              <label  className="form-label">User id</label>
                              <select onChange={(e) => this.handleuser_idChange(e)} value={this.state.user_id} className="form-control" >
                                  <option>Select User id</option>
                                  {this.renderOptionusers()}
                                  
                                  
                                </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label"> mc id</label>
                              <select onChange={(e) => this.handlemc_idChange(e)} value={this.state. mc_id} className="form-control" >
                                  <option>Select  mc id</option>
                                  {this.renderOptionmagic_circles()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_userss: state.users.view_users,view_magic_circless: state.magic_circles.view_magic_circles,save_user_magic_circless : state.user_magic_circles.save_user_magic_circles, 
        update_user_magic_circless : state.user_magic_circles.update_user_magic_circles,
        get_user_magic_circless : state.user_magic_circles.get_user_magic_circles,
    };
  };

  export default connect(mapStateToProps, {
    view_users,view_magic_circles, save_user_magic_circles, update_user_magic_circles, get_user_magic_circles
  })(ModifyUserMagicCircle);


