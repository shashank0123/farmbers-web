import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_land_crops } from '../../../../redux/actions/land_crops';

class LandCropRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_land_crops(id, 'token').then(()=>{console.log(this.props.delete_land_cropss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/land_crops/edit/'+this.state.row.id
                var url2 = '/admin/land_crops/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.land_id}</td>
<td>{this.state.row.crop_id}</td>
<td>{this.state.row.log_date}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_land_cropss : state.land_crops.delete_land_crops, 
    };
  };

  export default connect(mapStateToProps, {
     delete_land_crops
  })(LandCropRow);
