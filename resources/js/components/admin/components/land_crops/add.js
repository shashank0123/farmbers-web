import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_land_crops } from '../../../../redux/actions/land_crops';
import { save_land_crops } from '../../../../redux/actions/land_crops';
import { get_land_crops } from '../../../../redux/actions/land_crops';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_lands } from '../../../../redux/actions/lands'; 
import { view_crops } from '../../../../redux/actions/crops'; 

class ModifyLandCrop extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        land_id : '',crop_id : '',log_date : '',status : '',lands : [],crops : [],query: ''
    }

    this.handleland_idChange = this.handleland_idChange.bind(this)
            this.handlecrop_idChange = this.handlecrop_idChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_land_crops('token', this.state.id).then(() => {
                this.setState({item : this.props.get_land_cropss});
                this.setState({editpage : 1});
                				this.setState({ land_id : this.props.get_land_cropss.land_id }) 
				this.setState({ crop_id : this.props.get_land_cropss.crop_id }) 
				this.setState({ log_date : this.props.get_land_cropss.log_date }) 
				this.setState({ status : this.props.get_land_cropss.status }) 

                })
            console.log(this.props.get_land_cropss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_lands(this.state).then(()=> {
                    if (this.props.view_landss != undefined){
                        this.setState({ lands  : this.props.view_landss })    
                        this.renderOptionlands()
                    }

                    
                })
        
            

            this.props.view_crops(this.state).then(()=> {
                    if (this.props.view_cropss != undefined){
                        this.setState({ crops  : this.props.view_cropss })    
                        this.renderOptioncrops()
                    }

                    
                })
        
    }


    renderOptionlands(){
            console.log(this.props.view_landss, 'ye aa rha hai')
            var returnlands = <></>;
                    if (typeof this.props.view_landss != 'undefined')
                    if ( this.props.view_landss.length > 0)
                    returnlands =  this.props.view_landss.map(function(element){
                        return <option key={element.land_id} value={element.land_id}>{element.land_name}</option>
                        })       

               
            return <>{returnlands}</>
        }renderOptioncrops(){
            console.log(this.props.view_cropss, 'ye aa rha hai')
            var returncrops = <></>;
                    if (typeof this.props.view_cropss != 'undefined')
                    if ( this.props.view_cropss.length > 0)
                    returncrops =  this.props.view_cropss.map(function(element){
                        return <option key={element.crop_id} value={element.crop_id}>{element.crop_name}</option>
                        })       

               
            return <>{returncrops}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_land_crops(this.state).then(()=> {
            console.log(this.props.save_land_cropss)
            history.push('/admin/land_crops');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_land_crops(this.state).then(()=> {
            console.log(this.props.save_land_cropss)
            history.push('/admin/land_crops');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handleland_idChange(e){
            this.setState({ land_id: e.target.value });
        }
        handlecrop_idChange(e){
            this.setState({ crop_id: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        handleland_idChange(e){
            this.setState({ land_id: e.target.value });
        }
        handlecrop_idChange(e){
            this.setState({ crop_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
                              <label  className="form-label">Land id</label>
                              <select onChange={(e) => this.handleland_idChange(e)} value={this.state.land_id} className="form-control" >
                                  <option>Select Land id</option>
                                  {this.renderOptionlands()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Crop id</label>
                              <select onChange={(e) => this.handlecrop_idChange(e)} value={this.state.crop_id} className="form-control" >
                                  <option>Select Crop id</option>
                                  {this.renderOptioncrops()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_landss: state.lands.view_lands,view_cropss: state.crops.view_crops,save_land_cropss : state.land_crops.save_land_crops, 
        update_land_cropss : state.land_crops.update_land_crops,
        get_land_cropss : state.land_crops.get_land_crops,
    };
  };

  export default connect(mapStateToProps, {
    view_lands,view_crops, save_land_crops, update_land_crops, get_land_crops
  })(ModifyLandCrop);


