import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_weather_zones } from '../../../../redux/actions/weather_zones';

class WeatherZoneRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_weather_zones(id, 'token').then(()=>{console.log(this.props.delete_weather_zoness)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/weather_zones/edit/'+this.state.row.id
                var url2 = '/admin/weather_zones/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.zone_name}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_weather_zoness : state.weather_zones.delete_weather_zones, 
    };
  };

  export default connect(mapStateToProps, {
     delete_weather_zones
  })(WeatherZoneRow);
