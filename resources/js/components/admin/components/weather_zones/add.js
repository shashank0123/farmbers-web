import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_weather_zones } from '../../../../redux/actions/weather_zones';
import { save_weather_zones } from '../../../../redux/actions/weather_zones';
import { get_weather_zones } from '../../../../redux/actions/weather_zones';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;

class ModifyWeatherZone extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        zone_name : '',status : '',query: ''
    }

    this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_weather_zones('token', this.state.id).then(() => {
                this.setState({item : this.props.get_weather_zoness});
                this.setState({editpage : 1});
                				this.setState({ zone_name : this.props.get_weather_zoness.zone_name }) 
				this.setState({ status : this.props.get_weather_zoness.status }) 

                })
            console.log(this.props.get_weather_zoness, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
    }


    

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_weather_zones(this.state).then(()=> {
            console.log(this.props.save_weather_zoness)
            history.push('/admin/weather_zones');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_weather_zones(this.state).then(()=> {
            console.log(this.props.save_weather_zoness)
            history.push('/admin/weather_zones');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">Zone name</label>
              <input type="text" value={this.state.zone_name} onChange={(e) => {this.setState({zone_name: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {save_weather_zoness : state.weather_zones.save_weather_zones, 
        update_weather_zoness : state.weather_zones.update_weather_zones,
        get_weather_zoness : state.weather_zones.get_weather_zones,
    };
  };

  export default connect(mapStateToProps, {
     save_weather_zones, update_weather_zones, get_weather_zones
  })(ModifyWeatherZone);


