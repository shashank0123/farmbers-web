import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_crops } from '../../../../redux/actions/crops';
import { save_crops } from '../../../../redux/actions/crops';
import { get_crops } from '../../../../redux/actions/crops';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;

class ModifyCrop extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        crop_name : '',crop_duration : '',crop_image : '',featured : '',position : '',status : '',description : EditorState.createEmpty(),query: ''
    }

    this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_crops('token', this.state.id).then(() => {
                this.setState({item : this.props.get_cropss});
                this.setState({editpage : 1});
                				this.setState({ crop_name : this.props.get_cropss.crop_name }) 
				this.setState({ crop_duration : this.props.get_cropss.crop_duration }) 
				this.setState({ crop_image : this.props.get_cropss.crop_image }) 
				this.setState({ featured : this.props.get_cropss.featured }) 
				this.setState({ position : this.props.get_cropss.position }) 
				this.setState({ status : this.props.get_cropss.status }) 
				this.setState({ description : this.props.get_cropss.description }) 

                })
            console.log(this.props.get_cropss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
    }


    

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_crops(this.state).then(()=> {
            console.log(this.props.save_cropss)
            history.push('/admin/crops');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_crops(this.state).then(()=> {
            console.log(this.props.save_cropss)
            history.push('/admin/crops');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        

    render(){
        // console.log(this.state.description)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">Crop name</label>
              <input type="text" value={this.state.crop_name} onChange={(e) => {this.setState({crop_name: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Crop duration</label>
              <input type="text" value={this.state.crop_duration} onChange={(e) => {this.setState({crop_duration: e.target.value})}} className="form-control"/>
            </div>
            <div className="form-group">
                <label >Crop image</label>
                <input onChange={(e) => { this.setState({crop_image: e.target.files[0]}) }} type="file" className="form-control-file" />
              </div>
              <div className="mb-3">
              <label  className="form-label">Featured</label>
              <input type="text" value={this.state.featured} onChange={(e) => {this.setState({featured: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Position</label>
              <input type="text" value={this.state.position} onChange={(e) => {this.setState({position: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            <div className="mb-3">
                                <label >Description</label>
                                <Editor
                                  editorState={this.state.description}
                                  toolbarClassName="toolbarClassName"
                                  wrapperClassName="wrapperClassName"
                                  editorClassName="editorClassName"
                                  onEditorStateChange={(editorState) => this.setState({description: editorState})}
                                />
                              </div>
                              
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {save_cropss : state.crops.save_crops, 
        update_cropss : state.crops.update_crops,
        get_cropss : state.crops.get_crops,
    };
  };

  export default connect(mapStateToProps, {
     save_crops, update_crops, get_crops
  })(ModifyCrop);


