import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_crops } from '../../../../redux/actions/crops';

class CropRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_crops(id, 'token').then(()=>{console.log(this.props.delete_cropss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/crops/edit/'+this.state.row.id
                var url2 = '/admin/crops/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.crop_name}</td>
<td>{this.state.row.crop_duration}</td>
<td><img width='100px' src={this.state.row.crop_image}/></td>
<td>{this.state.row.featured}</td>
<td>{this.state.row.position}</td>
<td>{this.state.row.status}</td>
<td>{this.state.row.description}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span><Link className='btn btn-primary' style={{fontSize:'smaller'}} to={'crop_stages?crop_id='+this.state.row.id}>Crop Stage</Link><Link className='btn btn-primary' style={{fontSize:'smaller'}} to={'crop_priorities?crop_id='+this.state.row.id}>Crop Priority</Link><Link className='btn btn-primary' style={{fontSize:'smaller'}} to={'crop_locations?crop_id='+this.state.row.id}>Crop Location</Link><Link className='btn btn-primary' style={{fontSize:'smaller'}} to={'weather_delays?crop_id='+this.state.row.id}>Weather Delay</Link><Link className='btn btn-primary' style={{fontSize:'smaller'}} to={'land_crops?crop_id='+this.state.row.id}>Land Crop</Link></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_cropss : state.crops.delete_crops, 
    };
  };

  export default connect(mapStateToProps, {
     delete_crops
  })(CropRow);
