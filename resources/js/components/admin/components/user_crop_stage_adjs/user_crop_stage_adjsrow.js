import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_user_crop_stage_adjs } from '../../../../redux/actions/user_crop_stage_adjs';

class UserCropStageAdjRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_user_crop_stage_adjs(id, 'token').then(()=>{console.log(this.props.delete_user_crop_stage_adjss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/user_crop_stage_adjs/edit/'+this.state.row.id
                var url2 = '/admin/user_crop_stage_adjs/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.user_crop_stage_id}</td>
<td>{this.state.row.adj_day}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_user_crop_stage_adjss : state.user_crop_stage_adjs.delete_user_crop_stage_adjs, 
    };
  };

  export default connect(mapStateToProps, {
     delete_user_crop_stage_adjs
  })(UserCropStageAdjRow);
