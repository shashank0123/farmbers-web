import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_user_crop_stage_adjs } from '../../../../redux/actions/user_crop_stage_adjs';
import { save_user_crop_stage_adjs } from '../../../../redux/actions/user_crop_stage_adjs';
import { get_user_crop_stage_adjs } from '../../../../redux/actions/user_crop_stage_adjs';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;

class ModifyUserCropStageAdj extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        user_crop_stage_id : '',adj_day : '',status : '',query: ''
    }

    this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_user_crop_stage_adjs('token', this.state.id).then(() => {
                this.setState({item : this.props.get_user_crop_stage_adjss});
                this.setState({editpage : 1});
                				this.setState({ user_crop_stage_id : this.props.get_user_crop_stage_adjss.user_crop_stage_id }) 
				this.setState({ adj_day : this.props.get_user_crop_stage_adjss.adj_day }) 
				this.setState({ status : this.props.get_user_crop_stage_adjss.status }) 

                })
            console.log(this.props.get_user_crop_stage_adjss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
    }


    

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_user_crop_stage_adjs(this.state).then(()=> {
            console.log(this.props.save_user_crop_stage_adjss)
            history.push('/admin/user_crop_stage_adjs');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_user_crop_stage_adjs(this.state).then(()=> {
            console.log(this.props.save_user_crop_stage_adjss)
            history.push('/admin/user_crop_stage_adjs');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">User crop stage id</label>
              <input type="text" value={this.state.user_crop_stage_id} onChange={(e) => {this.setState({user_crop_stage_id: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Adj day</label>
              <input type="text" value={this.state.adj_day} onChange={(e) => {this.setState({adj_day: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {save_user_crop_stage_adjss : state.user_crop_stage_adjs.save_user_crop_stage_adjs, 
        update_user_crop_stage_adjss : state.user_crop_stage_adjs.update_user_crop_stage_adjs,
        get_user_crop_stage_adjss : state.user_crop_stage_adjs.get_user_crop_stage_adjs,
    };
  };

  export default connect(mapStateToProps, {
     save_user_crop_stage_adjs, update_user_crop_stage_adjs, get_user_crop_stage_adjs
  })(ModifyUserCropStageAdj);


