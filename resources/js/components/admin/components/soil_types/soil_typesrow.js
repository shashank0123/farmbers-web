import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_soil_types } from '../../../../redux/actions/soil_types';

class SoilTypeRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_soil_types(id, 'token').then(()=>{console.log(this.props.delete_soil_typess)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/soil_types/edit/'+this.state.row.id
                var url2 = '/admin/soil_types/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.soil_type}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_soil_typess : state.soil_types.delete_soil_types, 
    };
  };

  export default connect(mapStateToProps, {
     delete_soil_types
  })(SoilTypeRow);
