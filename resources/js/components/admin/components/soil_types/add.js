import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_soil_types } from '../../../../redux/actions/soil_types';
import { save_soil_types } from '../../../../redux/actions/soil_types';
import { get_soil_types } from '../../../../redux/actions/soil_types';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;

class ModifySoilType extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        soil_type : '',status : '',query: ''
    }

    this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_soil_types('token', this.state.id).then(() => {
                this.setState({item : this.props.get_soil_typess});
                this.setState({editpage : 1});
                				this.setState({ soil_type : this.props.get_soil_typess.soil_type }) 
				this.setState({ status : this.props.get_soil_typess.status }) 

                })
            console.log(this.props.get_soil_typess, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
    }


    

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_soil_types(this.state).then(()=> {
            console.log(this.props.save_soil_typess)
            history.push('/admin/soil_types');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_soil_types(this.state).then(()=> {
            console.log(this.props.save_soil_typess)
            history.push('/admin/soil_types');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">Soil type</label>
              <input type="text" value={this.state.soil_type} onChange={(e) => {this.setState({soil_type: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {save_soil_typess : state.soil_types.save_soil_types, 
        update_soil_typess : state.soil_types.update_soil_types,
        get_soil_typess : state.soil_types.get_soil_types,
    };
  };

  export default connect(mapStateToProps, {
     save_soil_types, update_soil_types, get_soil_types
  })(ModifySoilType);


