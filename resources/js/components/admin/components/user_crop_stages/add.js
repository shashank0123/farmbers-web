import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_user_crop_stages } from '../../../../redux/actions/user_crop_stages';
import { save_user_crop_stages } from '../../../../redux/actions/user_crop_stages';
import { get_user_crop_stages } from '../../../../redux/actions/user_crop_stages';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const createHistory = require('history').createBrowserHistory;
import { view_users } from '../../../../redux/actions/users'; 

class ModifyUserCropStage extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        user_id : '',land_crop_id : '',log_date : '',stage_flexibility : '',status : '',users : [],query: ''
    }

    this.handleuser_idChange = this.handleuser_idChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_user_crop_stages('token', this.state.id).then(() => {
                this.setState({item : this.props.get_user_crop_stagess});
                this.setState({editpage : 1});
                				this.setState({ user_id : this.props.get_user_crop_stagess.user_id }) 
				this.setState({ land_crop_id : this.props.get_user_crop_stagess.land_crop_id }) 
				this.setState({ log_date : this.props.get_user_crop_stagess.log_date }) 
				this.setState({ stage_flexibility : this.props.get_user_crop_stagess.stage_flexibility }) 
				this.setState({ status : this.props.get_user_crop_stagess.status }) 

                })
            console.log(this.props.get_user_crop_stagess, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_users(this.state).then(()=> {
                    if (this.props.view_userss != undefined){
                        this.setState({ users  : this.props.view_userss })    
                        this.renderOptionusers()
                    }

                    
                })
        
    }


    renderOptionusers(){
            console.log(this.props.view_userss, 'ye aa rha hai')
            var returnusers = <></>;
                    if (typeof this.props.view_userss != 'undefined')
                    if ( this.props.view_userss.length > 0)
                    returnusers =  this.props.view_userss.map(function(element){
                        return <option key={element.user_id} value={element.user_id}>{element.name}</option>
                        })       

               
            return <>{returnusers}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_user_crop_stages(this.state).then(()=> {
            console.log(this.props.save_user_crop_stagess)
            history.push('/admin/user_crop_stages');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_user_crop_stages(this.state).then(()=> {
            console.log(this.props.save_user_crop_stagess)
            history.push('/admin/user_crop_stages');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handleuser_idChange(e){
            this.setState({ user_id: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        handleuser_idChange(e){
            this.setState({ user_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.status)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
                              <label  className="form-label">User id</label>
                              <select onChange={(e) => this.handleuser_idChange(e)} value={this.state.user_id} className="form-control" >
                                  <option>Select User id</option>
                                  {this.renderOptionusers()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Land crop id</label>
              <input type="text" value={this.state.land_crop_id} onChange={(e) => {this.setState({land_crop_id: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Stage flexibility</label>
              <input type="text" value={this.state.stage_flexibility} onChange={(e) => {this.setState({stage_flexibility: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={(e) => this.handlestatusChange(e)} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_userss: state.users.view_users,save_user_crop_stagess : state.user_crop_stages.save_user_crop_stages, 
        update_user_crop_stagess : state.user_crop_stages.update_user_crop_stages,
        get_user_crop_stagess : state.user_crop_stages.get_user_crop_stages,
    };
  };

  export default connect(mapStateToProps, {
    view_users, save_user_crop_stages, update_user_crop_stages, get_user_crop_stages
  })(ModifyUserCropStage);


