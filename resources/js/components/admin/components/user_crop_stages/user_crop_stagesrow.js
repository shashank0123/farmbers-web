import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_user_crop_stages } from '../../../../redux/actions/user_crop_stages';

class UserCropStageRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_user_crop_stages(id, 'token').then(()=>{console.log(this.props.delete_user_crop_stagess)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/user_crop_stages/edit/'+this.state.row.id
                var url2 = '/admin/user_crop_stages/delete/'+this.state.row.id
                return (<tr>
                <td>{this.state.row.user_id}</td>
<td>{this.state.row.land_crop_id}</td>
<td>{this.state.row.log_date}</td>
<td>{this.state.row.stage_flexibility}</td>
<td>{this.state.row.status}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.id)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_user_crop_stagess : state.user_crop_stages.delete_user_crop_stages, 
    };
  };

  export default connect(mapStateToProps, {
     delete_user_crop_stages
  })(UserCropStageRow);
