import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom'
import { connect } from 'react-redux';
import { login } from '../redux/actions/login';



class ForgotPassword extends Component {

    constructor(props){
        super(props)
        this.state={
            username : '',
            password : ''
        }
    }

    submitLogin(){

    }

    render(){
    return (
        <div className="d-flex align-items-center justify-content-center bg-sl-primary ht-100v">

      <div className="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white">
        <div className="signin-logo tx-center tx-24 tx-bold tx-inverse">Backstage <span className="tx-info tx-normal">admin</span></div>
        <div className="tx-center mg-b-60">High Functionality Admin Panel</div>

        <div className="form-group">
          <input type="text" className="form-control" placeholder="Enter your username"/>
        </div>

        <div className="form-group">
          <input type="password" className="form-control" placeholder="Enter your password"/>
          <Link to={'forgot-password'} className="tx-info tx-12 d-block mg-t-10">Forgot password?</Link>
        </div>
        <button type="submit" className="btn btn-info btn-block">Sign In</button>

        <div className="mg-t-60 tx-center">Not yet a member? <a href="register" className="tx-info">Sign Up</a></div>
      </div>
    </div>
    );
    }
}

const mapStateToProps = (state) => {
    return {
      logins: state.login.login,
    };
  };

  export default connect(mapStateToProps, {
    login,
  })(ForgotPassword);

