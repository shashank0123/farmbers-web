import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import Homepage from './Homepage'

class Navigation extends Component{

    constructor(props){
        super(props)
    }

 render() {
    const { match } = this.props
    return (

        <Switch>
        <Route 
                        exact path={'/'}
                        component={Homepage}
                    />
                    <Route path={'/home'} component={Homepage} />
        </Switch>

    );
}
}

export default Navigation;
