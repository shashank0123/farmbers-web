import React, { Component, Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Provider } from "react-redux"
import { store } from "../../redux/store"
import AdminNavigation from './../admin/components/navigation'
import AdminHeader from './../admin/components/AdminHeader';
import AdminFooter from './../admin/components/AdminFooter';
import AdminSideBar from './../admin/components/AdminSideBar';
import AdminRightSideBar from './../admin/components/AdminRightSideBar';

class AdminLayout extends Component {


    render(){
    	
        return (
        <>
        <Provider store={store}>
      <Suspense fallback={<p>Loading...</p>}>
          <>
          <Router>
            <AdminHeader/>
            <AdminSideBar/>
            <AdminRightSideBar/>
            <div className="sl-mainpanel">
                <div className="sl-pagebody">

                <AdminNavigation/>
                </div>
                <AdminFooter/>
            </div>
            </Router>
        </>
      </Suspense>
    </Provider>



        </>
            )
        }
}

export default AdminLayout;