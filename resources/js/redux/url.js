var base_url = 'http://farmbers.smartstalk.com/api'
const urlObj = {
    admin_login: base_url + '/admin/login',
    register: base_url + '/register',
    users_url: base_url + "/users",
    menu: base_url + '/menu',
    weather_zones_url: base_url + "/weather_zones",

    weather_impacts_url: base_url + "/weather_impacts",

    weather_delays_url: base_url + "/weather_delays",

    user_magic_circles_url: base_url + "/user_magic_circles",

    user_lands_url: base_url + "/user_lands",

    user_crop_stage_adjs_url: base_url + "/user_crop_stage_adjs",

    user_crop_stages_url: base_url + "/user_crop_stages",

    soil_types_url: base_url + "/soil_types",

    magic_circles_url: base_url + "/magic_circles",

    locations_url: base_url + "/locations",

    land_crops_url: base_url + "/land_crops",

    lands_url: base_url + "/lands",

    crop_stages_url: base_url + "/crop_stages",

    crop_priorities_url: base_url + "/crop_priorities",

    crop_locations_url: base_url + "/crop_locations",

    crops_url: base_url + "/crops",

    buyer_crop_params_url: base_url + "/buyer_crop_params",

    buyer_crop_demands_url: base_url + "/buyer_crop_demands",

    buyers_url: base_url + "/buyers",

weather_zones_url: base_url+"/weather_zones",

weather_impacts_url: base_url+"/weather_impacts",

weather_delays_url: base_url+"/weather_delays",

user_magic_circles_url: base_url+"/user_magic_circles",

user_lands_url: base_url+"/user_lands",

user_crop_stage_adjs_url: base_url+"/user_crop_stage_adjs",

user_crop_stages_url: base_url+"/user_crop_stages",

soil_types_url: base_url+"/soil_types",

magic_circles_url: base_url+"/magic_circles",

locations_url: base_url+"/locations",

land_crops_url: base_url+"/land_crops",

lands_url: base_url+"/lands",

crop_stages_url: base_url+"/crop_stages",

crop_priorities_url: base_url+"/crop_priorities",

crop_locations_url: base_url+"/crop_locations",

crops_url: base_url+"/crops",

buyer_crop_params_url: base_url+"/buyer_crop_params",

buyer_crop_demands_url: base_url+"/buyer_crop_demands",

buyers_url: base_url+"/buyers",

};

module.exports = urlObj;
