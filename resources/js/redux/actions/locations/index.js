import axios from 'axios';
import urlObj from '../../url';
export const view_locations = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.locations_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'LOCATIONSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_locations = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.locations_url+'/add?token='+state.token;
var data = new FormData()
data.append('location_name', state.location_name)
data.append('location_code', state.location_code)
data.append('location_pincode', state.location_pincode)
data.append('latitude', state.latitude)
data.append('longitude', state.longitude)
data.append('weather_zone', state.weather_zone)
data.append('soil_type', state.soil_type)
data.append('irrigation_level', state.irrigation_level)
data.append('cultivation_period', state.cultivation_period)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'LOCATIONSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_locations = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.locations_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('location_name', state.location_name)
data.append('location_code', state.location_code)
data.append('location_pincode', state.location_pincode)
data.append('latitude', state.latitude)
data.append('longitude', state.longitude)
data.append('weather_zone', state.weather_zone)
data.append('soil_type', state.soil_type)
data.append('irrigation_level', state.irrigation_level)
data.append('cultivation_period', state.cultivation_period)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'LOCATIONSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_locations = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.locations_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'LOCATIONSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_locations = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.locations_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'LOCATIONSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};