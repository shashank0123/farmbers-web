import axios from 'axios';
import urlObj from '../../url';
export const view_user_lands = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.user_lands_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'USER_LANDSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_user_lands = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_lands_url+'/add?token='+state.token;
var data = new FormData()
data.append('user_id', state.user_id)
data.append('land_id', state.land_id)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'USER_LANDSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_user_lands = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_lands_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('user_id', state.user_id)
data.append('land_id', state.land_id)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'USER_LANDSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_user_lands = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_lands_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'USER_LANDSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_user_lands = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_lands_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'USER_LANDSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};