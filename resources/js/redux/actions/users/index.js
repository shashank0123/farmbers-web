import axios from 'axios';
import urlObj from '../../url';
export const view_users = (state) => {
    return async (dispatch, getState) => {
        if (state.queryw == undefined) {
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null) {
            query += '&start_date=' + state.start_date
        }
        if (state.end_date != null) {
            query += '&end_date=' + state.end_date
        }
        var url = urlObj.users_url + '?q=' + state.query + query

        await axios
            .get(url)
            .then((response) => {
                if (response.data.data != undefined)
                    dispatch({
                        type: 'USERSVIEW',
                        payload: response.data

                    });

            })
            .catch((error) => {
                console.log(error);
            });
    };
};

export const get_crops = (token, id) => {
    return async (dispatch, getState) => {

        var url = urlObj.crops_url + '?token=' + token + '&item_id=' + id

        await axios
            .get(url)
            .then((response) => {
                dispatch({
                    type: 'CROPSGET',
                    payload: response.data

                });

            })
            .catch((error) => {
                console.log(error);
            });
    };
};

export const delete_users = (id, token) => {
    return async (dispatch, getState) => {

        var url = urlObj.users_url + '/delete/' + id + '?token=' + token + '&id=' + id;


        await axios
            .get(url)
            .then((response) => {
                dispatch({
                    type: 'DELETE',
                    payload: response.data

                });
            })
            .catch((error) => {
                console.log(error);
            });
    };
}
