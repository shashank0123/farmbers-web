import axios from 'axios';
import urlObj from '../../url';
export const view_user_crop_stages = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.user_crop_stages_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'USER_CROP_STAGESVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_user_crop_stages = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_crop_stages_url+'/add?token='+state.token;
var data = new FormData()
data.append('user_id', state.user_id)
data.append('land_crop_id', state.land_crop_id)
data.append('log_date', state.log_date)
data.append('stage_flexibility', state.stage_flexibility)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'USER_CROP_STAGESSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_user_crop_stages = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_crop_stages_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('user_id', state.user_id)
data.append('land_crop_id', state.land_crop_id)
data.append('log_date', state.log_date)
data.append('stage_flexibility', state.stage_flexibility)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'USER_CROP_STAGESEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_user_crop_stages = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_crop_stages_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'USER_CROP_STAGESDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_user_crop_stages = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_crop_stages_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'USER_CROP_STAGESGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};