import axios from 'axios';
import urlObj from '../../url';
export const view_weather_impacts = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.weather_impacts_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'WEATHER_IMPACTSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_weather_impacts = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.weather_impacts_url+'/add?token='+state.token;
var data = new FormData()
data.append('delay_id', state.delay_id)
data.append('log_date', state.log_date)
data.append('delay_reason', state.delay_reason)
data.append('delay', state.delay)
data.append('message', state.message)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'WEATHER_IMPACTSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_weather_impacts = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.weather_impacts_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('delay_id', state.delay_id)
data.append('log_date', state.log_date)
data.append('delay_reason', state.delay_reason)
data.append('delay', state.delay)
data.append('message', state.message)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'WEATHER_IMPACTSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_weather_impacts = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.weather_impacts_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'WEATHER_IMPACTSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_weather_impacts = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.weather_impacts_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'WEATHER_IMPACTSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};