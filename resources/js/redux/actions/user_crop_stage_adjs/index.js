import axios from 'axios';
import urlObj from '../../url';
export const view_user_crop_stage_adjs = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.user_crop_stage_adjs_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'USER_CROP_STAGE_ADJSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_user_crop_stage_adjs = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_crop_stage_adjs_url+'/add?token='+state.token;
var data = new FormData()
data.append('user_crop_stage_id', state.user_crop_stage_id)
data.append('adj_day', state.adj_day)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'USER_CROP_STAGE_ADJSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_user_crop_stage_adjs = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_crop_stage_adjs_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('user_crop_stage_id', state.user_crop_stage_id)
data.append('adj_day', state.adj_day)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'USER_CROP_STAGE_ADJSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_user_crop_stage_adjs = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_crop_stage_adjs_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'USER_CROP_STAGE_ADJSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_user_crop_stage_adjs = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.user_crop_stage_adjs_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'USER_CROP_STAGE_ADJSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};