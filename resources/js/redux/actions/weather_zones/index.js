import axios from 'axios';
import urlObj from '../../url';
export const view_weather_zones = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.weather_zones_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'WEATHER_ZONESVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_weather_zones = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.weather_zones_url+'/add?token='+state.token;
var data = new FormData()
data.append('zone_name', state.zone_name)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'WEATHER_ZONESSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_weather_zones = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.weather_zones_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('zone_name', state.zone_name)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'WEATHER_ZONESEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_weather_zones = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.weather_zones_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'WEATHER_ZONESDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_weather_zones = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.weather_zones_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'WEATHER_ZONESGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};