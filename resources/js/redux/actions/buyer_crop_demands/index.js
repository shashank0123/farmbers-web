import axios from 'axios';
import urlObj from '../../url';
export const view_buyer_crop_demands = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.buyer_crop_demands_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'BUYER_CROP_DEMANDSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_buyer_crop_demands = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyer_crop_demands_url+'/add?token='+state.token;
var data = new FormData()
data.append('buyer_id', state.buyer_id)
data.append('crop_id', state.crop_id)
data.append('pincode', state.pincode)
data.append('demand', state.demand)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'BUYER_CROP_DEMANDSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_buyer_crop_demands = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyer_crop_demands_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('buyer_id', state.buyer_id)
data.append('crop_id', state.crop_id)
data.append('pincode', state.pincode)
data.append('demand', state.demand)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'BUYER_CROP_DEMANDSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_buyer_crop_demands = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyer_crop_demands_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'BUYER_CROP_DEMANDSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_buyer_crop_demands = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyer_crop_demands_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'BUYER_CROP_DEMANDSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};