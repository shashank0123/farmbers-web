import axios from 'axios';
import urlObj from '../../url';
export const view_lands = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.lands_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'LANDSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_lands = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.lands_url+'/add?token='+state.token;
var data = new FormData()
data.append('land_name', state.land_name)
data.append('latitude', state.latitude)
data.append('longitude', state.longitude)
data.append('address', state.address)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'LANDSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_lands = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.lands_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('land_name', state.land_name)
data.append('latitude', state.latitude)
data.append('longitude', state.longitude)
data.append('address', state.address)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'LANDSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_lands = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.lands_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'LANDSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_lands = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.lands_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'LANDSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};