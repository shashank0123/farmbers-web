import axios from 'axios';
import urlObj from '../../url';
export const view_crop_stages = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.crop_stages_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'CROP_STAGESVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_crop_stages = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_stages_url+'/add?token='+state.token;
var data = new FormData()
data.append('crop_id', state.crop_id)
data.append('stage_name', state.stage_name)
data.append('stage_flexibility', state.stage_flexibility)
data.append('stage_duration', state.stage_duration)
data.append('position', state.position)
data.append('status', state.status)
data.append('stage_desc', state.stage_desc)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'CROP_STAGESSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_crop_stages = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_stages_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('crop_id', state.crop_id)
data.append('stage_name', state.stage_name)
data.append('stage_flexibility', state.stage_flexibility)
data.append('stage_duration', state.stage_duration)
data.append('position', state.position)
data.append('status', state.status)
data.append('stage_desc', state.stage_desc)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'CROP_STAGESEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_crop_stages = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_stages_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'CROP_STAGESDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_crop_stages = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_stages_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'CROP_STAGESGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};