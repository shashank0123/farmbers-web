import axios from 'axios';
import urlObj from '../../url';
export const view_crops = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.crops_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'CROPSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_crops = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crops_url+'/add?token='+state.token;
var data = new FormData()
data.append('crop_name', state.crop_name)
data.append('crop_duration', state.crop_duration)

        if (state.crop_image != '')
        data.append('crop_image', state.crop_image, 'crops' )
        else
            data.append('crop_image', null )
        data.append('featured', state.featured)
data.append('position', state.position)
data.append('status', state.status)
data.append('description', state.description)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'CROPSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_crops = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crops_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('crop_name', state.crop_name)
data.append('crop_duration', state.crop_duration)
data.append('crop_image', state.crop_image, state.crop_image.name )
data.append('featured', state.featured)
data.append('position', state.position)
data.append('status', state.status)
data.append('description', state.description)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'CROPSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_crops = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crops_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'CROPSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_crops = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crops_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'CROPSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};