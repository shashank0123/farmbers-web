import axios from 'axios';
import urlObj from '../../url';
export const view_buyers = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.buyers_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'BUYERSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_buyers = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyers_url+'/add?token='+state.token;
var data = new FormData()
data.append('buyer_name', state.buyer_name)
data.append('buyer_type', state.buyer_type)
data.append('address', state.address)
data.append('pincode', state.pincode)
data.append('latitude', state.latitude)
data.append('longitude', state.longitude)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'BUYERSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_buyers = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyers_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('buyer_name', state.buyer_name)
data.append('buyer_type', state.buyer_type)
data.append('address', state.address)
data.append('pincode', state.pincode)
data.append('latitude', state.latitude)
data.append('longitude', state.longitude)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'BUYERSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_buyers = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyers_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'BUYERSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_buyers = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyers_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'BUYERSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};