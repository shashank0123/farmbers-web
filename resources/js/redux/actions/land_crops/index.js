import axios from 'axios';
import urlObj from '../../url';
export const view_land_crops = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.land_crops_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'LAND_CROPSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_land_crops = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.land_crops_url+'/add?token='+state.token;
var data = new FormData()
data.append('land_id', state.land_id)
data.append('crop_id', state.crop_id)
data.append('log_date', state.log_date)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'LAND_CROPSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_land_crops = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.land_crops_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('land_id', state.land_id)
data.append('crop_id', state.crop_id)
data.append('log_date', state.log_date)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'LAND_CROPSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_land_crops = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.land_crops_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'LAND_CROPSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_land_crops = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.land_crops_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'LAND_CROPSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};