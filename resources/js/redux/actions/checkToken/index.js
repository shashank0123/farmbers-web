import axios from 'axios';
import url from './../../url';
export const checkToken = (state) => {
  return async (dispatch, getState) => {

    var token = localStorage.getItem('token')
    if (token) {
        dispatch({
            type: 'Success',
            payload: token,

        });
    }
    else{
        dispatch({
            type: 'Failure',
            payload: "",

        });

    }

}

};
