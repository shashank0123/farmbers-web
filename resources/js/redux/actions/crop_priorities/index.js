import axios from 'axios';
import urlObj from '../../url';
export const view_crop_priorities = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.crop_priorities_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'CROP_PRIORITIESVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_crop_priorities = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_priorities_url+'/add?token='+state.token;
var data = new FormData()
data.append('crop_id', state.crop_id)
data.append('latitude', state.latitude)
data.append('longitude', state.longitude)
data.append('pincode', state.pincode)
data.append('priority', state.priority)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'CROP_PRIORITIESSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_crop_priorities = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_priorities_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('crop_id', state.crop_id)
data.append('latitude', state.latitude)
data.append('longitude', state.longitude)
data.append('pincode', state.pincode)
data.append('priority', state.priority)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'CROP_PRIORITIESEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_crop_priorities = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_priorities_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'CROP_PRIORITIESDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_crop_priorities = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_priorities_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'CROP_PRIORITIESGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};