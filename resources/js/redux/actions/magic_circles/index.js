import axios from 'axios';
import urlObj from '../../url';
export const view_magic_circles = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.magic_circles_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'MAGIC_CIRCLESVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_magic_circles = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.magic_circles_url+'/add?token='+state.token;
var data = new FormData()
data.append('mc_name', state.mc_name)
data.append('pincode', state.pincode)
data.append('radius', state.radius)
data.append('status', state.status)
data.append('latitude', state.latitude)
data.append('longitude', state.longitude)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'MAGIC_CIRCLESSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_magic_circles = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.magic_circles_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('mc_name', state.mc_name)
data.append('pincode', state.pincode)
data.append('radius', state.radius)
data.append('status', state.status)
data.append('latitude', state.latitude)
data.append('longitude', state.longitude)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'MAGIC_CIRCLESEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_magic_circles = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.magic_circles_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'MAGIC_CIRCLESDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_magic_circles = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.magic_circles_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'MAGIC_CIRCLESGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};