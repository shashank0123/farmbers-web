import axios from 'axios';
import urlObj from '../../url';
export const view_crop_locations = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.crop_locations_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'CROP_LOCATIONSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_crop_locations = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_locations_url+'/add?token='+state.token;
var data = new FormData()
data.append('crop_id', state.crop_id)
data.append('location_id', state.location_id)
data.append('from_day', state.from_day)
data.append('to_day', state.to_day)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'CROP_LOCATIONSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_crop_locations = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_locations_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('crop_id', state.crop_id)
data.append('location_id', state.location_id)
data.append('from_day', state.from_day)
data.append('to_day', state.to_day)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'CROP_LOCATIONSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_crop_locations = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_locations_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'CROP_LOCATIONSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_crop_locations = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.crop_locations_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'CROP_LOCATIONSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};