import axios from 'axios';
import urlObj from '../../url';
export const view_buyer_crop_params = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.buyer_crop_params_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'BUYER_CROP_PARAMSVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_buyer_crop_params = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyer_crop_params_url+'/add?token='+state.token;
var data = new FormData()
data.append('buyer_id', state.buyer_id)
data.append('crop_name', state.crop_name)
data.append('log_date', state.log_date)
data.append('param_name', state.param_name)
data.append('param_value', state.param_value)
data.append('param_unit', state.param_unit)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'BUYER_CROP_PARAMSSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_buyer_crop_params = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyer_crop_params_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('buyer_id', state.buyer_id)
data.append('crop_name', state.crop_name)
data.append('log_date', state.log_date)
data.append('param_name', state.param_name)
data.append('param_value', state.param_value)
data.append('param_unit', state.param_unit)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'BUYER_CROP_PARAMSEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_buyer_crop_params = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyer_crop_params_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'BUYER_CROP_PARAMSDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_buyer_crop_params = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.buyer_crop_params_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'BUYER_CROP_PARAMSGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};