import axios from 'axios';
import urlObj from '../../url';
export const view_soil_types = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
        var query = '';
        if (state.start_date != null){
            query += '&start_date='+state.start_date
        }
        if (state.end_date != null){
            query += '&end_date='+state.end_date
        }
    var url = urlObj.soil_types_url+'?q='+state.query+query
		
    await axios
      .get(url)
      .then((response) => {
        if (response.data.data != undefined)
        dispatch({
          type: 'SOIL_TYPESVIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_soil_types = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.soil_types_url+'/add?token='+state.token;
var data = new FormData()
data.append('soil_type', state.soil_type)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'SOIL_TYPESSAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_soil_types = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.soil_types_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('soil_type', state.soil_type)
data.append('status', state.status)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'SOIL_TYPESEDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_soil_types = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.soil_types_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'SOIL_TYPESDELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_soil_types = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.soil_types_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'SOIL_TYPESGET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};