import axios from 'axios';
import url from './../../url';
export const menu = (state) => {
  return async (dispatch, getState) => {
    await axios

      .get(url.menu, state)
      .then((response) => {
        dispatch({
          type: 'REGISTER',
          payload: response.data,

        });

      })
      .catch((error) => {
        console.log(error);
      });
  };
};
