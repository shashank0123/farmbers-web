const initialState = {
    view_lands: [],
    save_lands: {},
    update_lands: {},
    delete_lands: {},
    get_lands: {},
    count_lands: 0
  };
  
  export const lands = (state = initialState, action) => {
    switch (action.type) {
      case 'LANDSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_lands:action.payload.data, count_lands: action.payload.count }
        }
      }
      case 'LANDSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_lands:action.payload.data, count_lands: 0 }
        }
      }
      case 'LANDSSAVE': {
        return { ...state,save_lands:action.payload };
      }
      case 'LANDSEDIT': {
        return { ...state,update_lands:action.payload };
      }
      case 'LANDSDELETE': {
        return { ...state,delete_lands:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default lands;