const initialState = {
    view_crop_stages: [],
    save_crop_stages: {},
    update_crop_stages: {},
    delete_crop_stages: {},
    get_crop_stages: {},
    count_crop_stages: 0
  };
  
  export const crop_stages = (state = initialState, action) => {
    switch (action.type) {
      case 'CROP_STAGESVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_crop_stages:action.payload.data, count_crop_stages: action.payload.count }
        }
      }
      case 'CROP_STAGESGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_crop_stages:action.payload.data, count_crop_stages: 0 }
        }
      }
      case 'CROP_STAGESSAVE': {
        return { ...state,save_crop_stages:action.payload };
      }
      case 'CROP_STAGESEDIT': {
        return { ...state,update_crop_stages:action.payload };
      }
      case 'CROP_STAGESDELETE': {
        return { ...state,delete_crop_stages:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default crop_stages;