const initialState = {
    checkToken: {}
  };

  export const checkToken = (state = initialState, action) => {
    switch (action.type) {
      case 'Success': {
        return { ...state, checkToken: action.payload,getValue:'yes' };
      }
      case 'Failure': {
        return { ...state, checkToken: action.payload,getValue:'no' };
      }
      default: {
        return state;
      }
    }
  };
  export default checkToken;
