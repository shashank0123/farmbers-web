const initialState = {
    view_crop_locations: [],
    save_crop_locations: {},
    update_crop_locations: {},
    delete_crop_locations: {},
    get_crop_locations: {},
    count_crop_locations: 0
  };
  
  export const crop_locations = (state = initialState, action) => {
    switch (action.type) {
      case 'CROP_LOCATIONSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_crop_locations:action.payload.data, count_crop_locations: action.payload.count }
        }
      }
      case 'CROP_LOCATIONSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_crop_locations:action.payload.data, count_crop_locations: 0 }
        }
      }
      case 'CROP_LOCATIONSSAVE': {
        return { ...state,save_crop_locations:action.payload };
      }
      case 'CROP_LOCATIONSEDIT': {
        return { ...state,update_crop_locations:action.payload };
      }
      case 'CROP_LOCATIONSDELETE': {
        return { ...state,delete_crop_locations:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default crop_locations;