import crop_stages from "./crop_stages"
import lands from "./lands"
import magic_circles from "./magic_circles"
import buyers from "./buyers"
import buyer_crop_demands from "./buyer_crop_demands"
import buyer_crop_params from "./buyer_crop_params"
import crop_locations from "./crop_locations"
import crop_priorities from "./crop_priorities"
import land_crops from "./land_crops"
import crops from "./crops"
import locations from "./locations"
import soil_types from "./soil_types"
import user_crop_stages from "./user_crop_stages"
import users from "./users"
import user_crop_stage_adjs from "./user_crop_stage_adjs"
import user_lands from "./user_lands"
import user_magic_circles from "./user_magic_circles"
import weather_delays from "./weather_delays"
import weather_impacts from "./weather_impacts"
import weather_zones from "./weather_zones"


import { combineReducers } from "redux"
// import customizer from "./customizer/"
import login from "./login"
import register from "./register"
import checkToken from "./checkToken"
import menu from "./menu"

const rootReducer = combineReducers({
    login: login,
    register: register,
    checkToken: checkToken,
    menu: menu


    ,
    weather_zones: weather_zones
    ,
    weather_impacts: weather_impacts
    ,
    weather_delays: weather_delays
    ,
    user_magic_circles: user_magic_circles
    ,
    user_lands: user_lands
    ,
    user_crop_stage_adjs: user_crop_stage_adjs
    ,
    user_crop_stages: user_crop_stages
    ,
    soil_types: soil_types
    ,
    locations: locations
    ,
    land_crops: land_crops,
    users: users,
    crops: crops
    ,
    crop_priorities: crop_priorities
    ,
    crop_locations: crop_locations
    ,
    buyer_crop_params: buyer_crop_params
    ,
    buyer_crop_demands: buyer_crop_demands
    ,
    buyers: buyers
    ,
    magic_circles: magic_circles
    ,
    lands: lands
    ,
    crop_stages: crop_stages
})

export default rootReducer
