const initialState = {
    view_locations: [],
    save_locations: {},
    update_locations: {},
    delete_locations: {},
    get_locations: {},
    count_locations: 0
  };
  
  export const locations = (state = initialState, action) => {
    switch (action.type) {
      case 'LOCATIONSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_locations:action.payload.data, count_locations: action.payload.count }
        }
      }
      case 'LOCATIONSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_locations:action.payload.data, count_locations: 0 }
        }
      }
      case 'LOCATIONSSAVE': {
        return { ...state,save_locations:action.payload };
      }
      case 'LOCATIONSEDIT': {
        return { ...state,update_locations:action.payload };
      }
      case 'LOCATIONSDELETE': {
        return { ...state,delete_locations:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default locations;