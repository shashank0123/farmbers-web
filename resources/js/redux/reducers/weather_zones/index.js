const initialState = {
    view_weather_zones: [],
    save_weather_zones: {},
    update_weather_zones: {},
    delete_weather_zones: {},
    get_weather_zones: {},
    count_weather_zones: 0
  };
  
  export const weather_zones = (state = initialState, action) => {
    switch (action.type) {
      case 'WEATHER_ZONESVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_weather_zones:action.payload.data, count_weather_zones: action.payload.count }
        }
      }
      case 'WEATHER_ZONESGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_weather_zones:action.payload.data, count_weather_zones: 0 }
        }
      }
      case 'WEATHER_ZONESSAVE': {
        return { ...state,save_weather_zones:action.payload };
      }
      case 'WEATHER_ZONESEDIT': {
        return { ...state,update_weather_zones:action.payload };
      }
      case 'WEATHER_ZONESDELETE': {
        return { ...state,delete_weather_zones:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default weather_zones;