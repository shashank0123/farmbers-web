const initialState = {
    view_magic_circles: [],
    save_magic_circles: {},
    update_magic_circles: {},
    delete_magic_circles: {},
    get_magic_circles: {},
    count_magic_circles: 0
  };
  
  export const magic_circles = (state = initialState, action) => {
    switch (action.type) {
      case 'MAGIC_CIRCLESVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_magic_circles:action.payload.data, count_magic_circles: action.payload.count }
        }
      }
      case 'MAGIC_CIRCLESGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_magic_circles:action.payload.data, count_magic_circles: 0 }
        }
      }
      case 'MAGIC_CIRCLESSAVE': {
        return { ...state,save_magic_circles:action.payload };
      }
      case 'MAGIC_CIRCLESEDIT': {
        return { ...state,update_magic_circles:action.payload };
      }
      case 'MAGIC_CIRCLESDELETE': {
        return { ...state,delete_magic_circles:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default magic_circles;