const initialState = {
    view_crop_priorities: [],
    save_crop_priorities: {},
    update_crop_priorities: {},
    delete_crop_priorities: {},
    get_crop_priorities: {},
    count_crop_priorities: 0
  };
  
  export const crop_priorities = (state = initialState, action) => {
    switch (action.type) {
      case 'CROP_PRIORITIESVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_crop_priorities:action.payload.data, count_crop_priorities: action.payload.count }
        }
      }
      case 'CROP_PRIORITIESGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_crop_priorities:action.payload.data, count_crop_priorities: 0 }
        }
      }
      case 'CROP_PRIORITIESSAVE': {
        return { ...state,save_crop_priorities:action.payload };
      }
      case 'CROP_PRIORITIESEDIT': {
        return { ...state,update_crop_priorities:action.payload };
      }
      case 'CROP_PRIORITIESDELETE': {
        return { ...state,delete_crop_priorities:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default crop_priorities;