const initialState = {
    view_buyer_crop_demands: [],
    save_buyer_crop_demands: {},
    update_buyer_crop_demands: {},
    delete_buyer_crop_demands: {},
    get_buyer_crop_demands: {},
    count_buyer_crop_demands: 0
  };
  
  export const buyer_crop_demands = (state = initialState, action) => {
    switch (action.type) {
      case 'BUYER_CROP_DEMANDSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_buyer_crop_demands:action.payload.data, count_buyer_crop_demands: action.payload.count }
        }
      }
      case 'BUYER_CROP_DEMANDSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_buyer_crop_demands:action.payload.data, count_buyer_crop_demands: 0 }
        }
      }
      case 'BUYER_CROP_DEMANDSSAVE': {
        return { ...state,save_buyer_crop_demands:action.payload };
      }
      case 'BUYER_CROP_DEMANDSEDIT': {
        return { ...state,update_buyer_crop_demands:action.payload };
      }
      case 'BUYER_CROP_DEMANDSDELETE': {
        return { ...state,delete_buyer_crop_demands:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default buyer_crop_demands;