const initialState = {
    view_users: [],
    save_users: {},
    update_users: {},
    delete_users: {},
    get_users: {},
    count_users: 0
};

export const users = (state = initialState, action) => {
    switch (action.type) {
        case 'USERSVIEW': {
            if (action.payload.status == 'failed')
                return { state };
            else {
                return { ...state, view_users: action.payload.data, count_users: action.payload.count }
            }
        }
        case 'USERSGET': {
            if (action.payload.status == 'failed')
                return { state };
            else {
                return { ...state, get_users: action.payload.data, count_users: 0 }
            }
        }
        case 'USERSSAVE': {
            return { ...state, save_users: action.payload };
        }
        case 'USERSEDIT': {
            return { ...state, update_users: action.payload };
        }
        case 'USERSDELETE': {
            return { ...state, delete_users: action.payload };
        }
        default: {
            return state;
        }
    }
};
export default users;
