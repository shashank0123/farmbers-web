const initialState = {
    view_user_crop_stages: [],
    save_user_crop_stages: {},
    update_user_crop_stages: {},
    delete_user_crop_stages: {},
    get_user_crop_stages: {},
    count_user_crop_stages: 0
  };
  
  export const user_crop_stages = (state = initialState, action) => {
    switch (action.type) {
      case 'USER_CROP_STAGESVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_user_crop_stages:action.payload.data, count_user_crop_stages: action.payload.count }
        }
      }
      case 'USER_CROP_STAGESGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_user_crop_stages:action.payload.data, count_user_crop_stages: 0 }
        }
      }
      case 'USER_CROP_STAGESSAVE': {
        return { ...state,save_user_crop_stages:action.payload };
      }
      case 'USER_CROP_STAGESEDIT': {
        return { ...state,update_user_crop_stages:action.payload };
      }
      case 'USER_CROP_STAGESDELETE': {
        return { ...state,delete_user_crop_stages:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default user_crop_stages;