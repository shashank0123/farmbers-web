const initialState = {
    view_buyers: [],
    save_buyers: {},
    update_buyers: {},
    delete_buyers: {},
    get_buyers: {},
    count_buyers: 0
  };
  
  export const buyers = (state = initialState, action) => {
    switch (action.type) {
      case 'BUYERSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_buyers:action.payload.data, count_buyers: action.payload.count }
        }
      }
      case 'BUYERSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_buyers:action.payload.data, count_buyers: 0 }
        }
      }
      case 'BUYERSSAVE': {
        return { ...state,save_buyers:action.payload };
      }
      case 'BUYERSEDIT': {
        return { ...state,update_buyers:action.payload };
      }
      case 'BUYERSDELETE': {
        return { ...state,delete_buyers:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default buyers;