const initialState = {
    view_soil_types: [],
    save_soil_types: {},
    update_soil_types: {},
    delete_soil_types: {},
    get_soil_types: {},
    count_soil_types: 0
  };
  
  export const soil_types = (state = initialState, action) => {
    switch (action.type) {
      case 'SOIL_TYPESVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_soil_types:action.payload.data, count_soil_types: action.payload.count }
        }
      }
      case 'SOIL_TYPESGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_soil_types:action.payload.data, count_soil_types: 0 }
        }
      }
      case 'SOIL_TYPESSAVE': {
        return { ...state,save_soil_types:action.payload };
      }
      case 'SOIL_TYPESEDIT': {
        return { ...state,update_soil_types:action.payload };
      }
      case 'SOIL_TYPESDELETE': {
        return { ...state,delete_soil_types:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default soil_types;