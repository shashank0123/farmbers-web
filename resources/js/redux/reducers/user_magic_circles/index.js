const initialState = {
    view_user_magic_circles: [],
    save_user_magic_circles: {},
    update_user_magic_circles: {},
    delete_user_magic_circles: {},
    get_user_magic_circles: {},
    count_user_magic_circles: 0
  };
  
  export const user_magic_circles = (state = initialState, action) => {
    switch (action.type) {
      case 'USER_MAGIC_CIRCLESVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_user_magic_circles:action.payload.data, count_user_magic_circles: action.payload.count }
        }
      }
      case 'USER_MAGIC_CIRCLESGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_user_magic_circles:action.payload.data, count_user_magic_circles: 0 }
        }
      }
      case 'USER_MAGIC_CIRCLESSAVE': {
        return { ...state,save_user_magic_circles:action.payload };
      }
      case 'USER_MAGIC_CIRCLESEDIT': {
        return { ...state,update_user_magic_circles:action.payload };
      }
      case 'USER_MAGIC_CIRCLESDELETE': {
        return { ...state,delete_user_magic_circles:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default user_magic_circles;