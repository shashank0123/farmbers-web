const initialState = {
    view_weather_impacts: [],
    save_weather_impacts: {},
    update_weather_impacts: {},
    delete_weather_impacts: {},
    get_weather_impacts: {},
    count_weather_impacts: 0
  };
  
  export const weather_impacts = (state = initialState, action) => {
    switch (action.type) {
      case 'WEATHER_IMPACTSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_weather_impacts:action.payload.data, count_weather_impacts: action.payload.count }
        }
      }
      case 'WEATHER_IMPACTSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_weather_impacts:action.payload.data, count_weather_impacts: 0 }
        }
      }
      case 'WEATHER_IMPACTSSAVE': {
        return { ...state,save_weather_impacts:action.payload };
      }
      case 'WEATHER_IMPACTSEDIT': {
        return { ...state,update_weather_impacts:action.payload };
      }
      case 'WEATHER_IMPACTSDELETE': {
        return { ...state,delete_weather_impacts:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default weather_impacts;