const initialState = {
    view_land_crops: [],
    save_land_crops: {},
    update_land_crops: {},
    delete_land_crops: {},
    get_land_crops: {},
    count_land_crops: 0
  };
  
  export const land_crops = (state = initialState, action) => {
    switch (action.type) {
      case 'LAND_CROPSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_land_crops:action.payload.data, count_land_crops: action.payload.count }
        }
      }
      case 'LAND_CROPSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_land_crops:action.payload.data, count_land_crops: 0 }
        }
      }
      case 'LAND_CROPSSAVE': {
        return { ...state,save_land_crops:action.payload };
      }
      case 'LAND_CROPSEDIT': {
        return { ...state,update_land_crops:action.payload };
      }
      case 'LAND_CROPSDELETE': {
        return { ...state,delete_land_crops:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default land_crops;