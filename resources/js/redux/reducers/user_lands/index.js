const initialState = {
    view_user_lands: [],
    save_user_lands: {},
    update_user_lands: {},
    delete_user_lands: {},
    get_user_lands: {},
    count_user_lands: 0
  };
  
  export const user_lands = (state = initialState, action) => {
    switch (action.type) {
      case 'USER_LANDSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_user_lands:action.payload.data, count_user_lands: action.payload.count }
        }
      }
      case 'USER_LANDSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_user_lands:action.payload.data, count_user_lands: 0 }
        }
      }
      case 'USER_LANDSSAVE': {
        return { ...state,save_user_lands:action.payload };
      }
      case 'USER_LANDSEDIT': {
        return { ...state,update_user_lands:action.payload };
      }
      case 'USER_LANDSDELETE': {
        return { ...state,delete_user_lands:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default user_lands;