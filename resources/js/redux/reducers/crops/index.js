const initialState = {
    view_crops: [],
    save_crops: {},
    update_crops: {},
    delete_crops: {},
    get_crops: {},
    count_crops: 0
  };
  
  export const crops = (state = initialState, action) => {
    switch (action.type) {
      case 'CROPSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_crops:action.payload.data, count_crops: action.payload.count }
        }
      }
      case 'CROPSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_crops:action.payload.data, count_crops: 0 }
        }
      }
      case 'CROPSSAVE': {
        return { ...state,save_crops:action.payload };
      }
      case 'CROPSEDIT': {
        return { ...state,update_crops:action.payload };
      }
      case 'CROPSDELETE': {
        return { ...state,delete_crops:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default crops;