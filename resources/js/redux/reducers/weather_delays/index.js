const initialState = {
    view_weather_delays: [],
    save_weather_delays: {},
    update_weather_delays: {},
    delete_weather_delays: {},
    get_weather_delays: {},
    count_weather_delays: 0
  };
  
  export const weather_delays = (state = initialState, action) => {
    switch (action.type) {
      case 'WEATHER_DELAYSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_weather_delays:action.payload.data, count_weather_delays: action.payload.count }
        }
      }
      case 'WEATHER_DELAYSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_weather_delays:action.payload.data, count_weather_delays: 0 }
        }
      }
      case 'WEATHER_DELAYSSAVE': {
        return { ...state,save_weather_delays:action.payload };
      }
      case 'WEATHER_DELAYSEDIT': {
        return { ...state,update_weather_delays:action.payload };
      }
      case 'WEATHER_DELAYSDELETE': {
        return { ...state,delete_weather_delays:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default weather_delays;