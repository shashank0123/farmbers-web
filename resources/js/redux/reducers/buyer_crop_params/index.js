const initialState = {
    view_buyer_crop_params: [],
    save_buyer_crop_params: {},
    update_buyer_crop_params: {},
    delete_buyer_crop_params: {},
    get_buyer_crop_params: {},
    count_buyer_crop_params: 0
  };
  
  export const buyer_crop_params = (state = initialState, action) => {
    switch (action.type) {
      case 'BUYER_CROP_PARAMSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_buyer_crop_params:action.payload.data, count_buyer_crop_params: action.payload.count }
        }
      }
      case 'BUYER_CROP_PARAMSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_buyer_crop_params:action.payload.data, count_buyer_crop_params: 0 }
        }
      }
      case 'BUYER_CROP_PARAMSSAVE': {
        return { ...state,save_buyer_crop_params:action.payload };
      }
      case 'BUYER_CROP_PARAMSEDIT': {
        return { ...state,update_buyer_crop_params:action.payload };
      }
      case 'BUYER_CROP_PARAMSDELETE': {
        return { ...state,delete_buyer_crop_params:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default buyer_crop_params;