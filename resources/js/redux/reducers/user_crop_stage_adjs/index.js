const initialState = {
    view_user_crop_stage_adjs: [],
    save_user_crop_stage_adjs: {},
    update_user_crop_stage_adjs: {},
    delete_user_crop_stage_adjs: {},
    get_user_crop_stage_adjs: {},
    count_user_crop_stage_adjs: 0
  };
  
  export const user_crop_stage_adjs = (state = initialState, action) => {
    switch (action.type) {
      case 'USER_CROP_STAGE_ADJSVIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_user_crop_stage_adjs:action.payload.data, count_user_crop_stage_adjs: action.payload.count }
        }
      }
      case 'USER_CROP_STAGE_ADJSGET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_user_crop_stage_adjs:action.payload.data, count_user_crop_stage_adjs: 0 }
        }
      }
      case 'USER_CROP_STAGE_ADJSSAVE': {
        return { ...state,save_user_crop_stage_adjs:action.payload };
      }
      case 'USER_CROP_STAGE_ADJSEDIT': {
        return { ...state,update_user_crop_stage_adjs:action.payload };
      }
      case 'USER_CROP_STAGE_ADJSDELETE': {
        return { ...state,delete_user_crop_stage_adjs:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default user_crop_stage_adjs;