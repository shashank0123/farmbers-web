var token = localStorage.getItem('token')
const requestHeader = { headers: { 'Authorization': 'Bearer '+token } };

module.exports = requestHeader;
