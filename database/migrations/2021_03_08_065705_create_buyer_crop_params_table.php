<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyerCropParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyer_crop_params', function (Blueprint $table) {
            $table->id('id');
			$table->string('buyer_id')->nullable();
			$table->string('crop_name')->nullable();
			$table->string('log_date')->nullable();
			$table->string('param_name')->nullable();
			$table->string('param_value')->nullable();
			$table->string('param_unit')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyer_crop_params');
    }
}
