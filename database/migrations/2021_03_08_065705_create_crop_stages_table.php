<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCropStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crop_stages', function (Blueprint $table) {
            $table->id('id');
			$table->string('crop_id')->nullable();
			$table->string('stage_name')->nullable();
			$table->string('stage_flexibility')->nullable();
			$table->string('stage_duration')->nullable();
			$table->string('position')->nullable();
			$table->string('status')->nullable();
			$table->string('stage_desc')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crop_stages');
    }
}
