<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyerCropDemandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyer_crop_demands', function (Blueprint $table) {
            $table->id('id');
			$table->string('buyer_id')->nullable();
			$table->string('crop_id')->nullable();
			$table->string('pincode')->nullable();
			$table->string('demand')->nullable();
			$table->string('status')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyer_crop_demands');
    }
}
