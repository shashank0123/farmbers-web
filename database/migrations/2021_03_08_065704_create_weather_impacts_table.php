<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherImpactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_impacts', function (Blueprint $table) {
            $table->id('id');
			$table->string('delay_id')->nullable();
			$table->string('log_date')->nullable();
			$table->string('delay_reason')->nullable();
			$table->string('delay')->nullable();
			$table->string('message')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_impacts');
    }
}
