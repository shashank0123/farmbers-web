<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crops', function (Blueprint $table) {
            $table->id('id');
			$table->string('crop_name')->nullable();
			$table->string('crop_duration')->nullable();
			$table->string('crop_image')->nullable();
			$table->string('featured')->nullable();
			$table->string('position')->nullable();
			$table->string('status')->nullable();
			$table->string('description')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crops');
    }
}
