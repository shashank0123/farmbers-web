<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCropLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crop_locations', function (Blueprint $table) {
            $table->id('id');
			$table->string('crop_id')->nullable();
			$table->string('location_id')->nullable();
			$table->string('from_day')->nullable();
			$table->string('to_day')->nullable();
			$table->string('status')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crop_locations');
    }
}
