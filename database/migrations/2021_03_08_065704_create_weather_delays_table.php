<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherDelaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_delays', function (Blueprint $table) {
            $table->id('id');
			$table->string('pincode')->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->string('crop_id')->nullable();
			$table->string('stage_id')->nullable();
			$table->string('delay_reason')->nullable();
			$table->string('delay')->nullable();
			$table->string('message')->nullable();
			$table->string('status')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_delays');
    }
}
