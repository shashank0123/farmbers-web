<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCropStageAdjsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_crop_stage_adjs', function (Blueprint $table) {
            $table->id('id');
			$table->string('user_crop_stage_id')->nullable();
			$table->string('adj_day')->nullable();
			$table->string('status')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_crop_stage_adjs');
    }
}
